/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_MAIN_GAMETYPE
#define TOB_MAIN_GAMETYPE

// Tower of Babylon !
#include "GameTob/Gui/Resources.hpp"


namespace Main
{

// The constants that define which and how the game is built
// They should be the only game-type specific code that the generic code references (albeit generically):

// Tower of Babylon !
using GameGuiResources = GameTob::Gui::Resources;

// The "application" name used in constructing the preference directory
static const char kPrefDirApplicationName[] = "towerofbabylon";

}   // namespace Main

#endif   // TOB_MAIN_GAMETYPE

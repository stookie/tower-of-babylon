/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_SDL_TEXT
#define TOB_SDL_TEXT

#include "Sdl/SdlFont.hpp"
#include "Sdl/SdlTexture.hpp"
#include "Sdl/SdlTypes.hpp"

#include <memory>
#include <string>


// See SdlContext.hpp for an introduction into the classes in this directory.

// The SdlText class is an opaque wrapper around a SdlTexture specifically created from text rendered by a SdlFont;
//  as well as some caching details to minimise the amount the text needs to be re-rendered. It is accessed and
//  modified through the SdlContext.

// The following are read-only convenience methods to view properties without referring to the SdlContext:
// - getDimensions() : Return the text dimensions


class SdlText
{
public:
    // Constructor is private: SdlText()
    ~SdlText();

    SdlText(const SdlText&) = delete;
    SdlText& operator=(const SdlText&) = delete;
    SdlText(SdlText&&) = delete;
    SdlText& operator=(SdlText&&) = delete;

    // Return the text dimensions
    SdlDims getDimensions() const;

    // Friend declaration
    friend class SdlContext;

private:
    // Constructor, only available for SdlContext
    SdlText(const std::shared_ptr<SdlFont>& iFont, const std::string& iText = "");

    // Check if the texture representing the text requires regenerating, only available for SdlContext
    bool isGenerateTextureNeeded(SDL_Colour iColour);

    // Update the text, only available for SdlContext
    // Returns true if the text actually changes, and the texture needs to be regenerated; false otherwise
    bool updateText(const std::string& iText);

    // Replace the texture representing the text, only available for SdlContext
    void replaceTexture(unsigned int iFontVersion, SDL_Colour iColour, const std::shared_ptr<SdlTexture>& iTexture);

    // Retrieve the texture representing the text, only available for SdlContext
    std::shared_ptr<SdlTexture> getTexture() const;

    // Retrieve the font, only available for SdlContext
    std::shared_ptr<SdlFont> getFont() const;

    // Retrieve the text, only available for SdlContext
    std::string getText() const;

    // Cached inputs
    std::shared_ptr<SdlFont> _font;
    unsigned int _fontVersion;
    std::string _text;
    SDL_Colour _colour;

    // Cached outputs
    std::shared_ptr<SdlTexture> _texture;

    // Caching details
    bool _regenerateNeeded;
};

#endif   // TOB_SDL_TEXT

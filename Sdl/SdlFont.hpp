/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_SDL_FONT
#define TOB_SDL_FONT

#include "Sdl/SdlTypes.hpp"

#include <SDL2/SDL_ttf.h>

#include <string>


// See SdlContext.hpp for an introduction into the classes in this directory.

// The SdlFont class is an opaque wrapper around a TTF_Font. It is accessed and modified through the SdlContext.

// The following are read-only convenience methods to view properties without referring to the SdlContext:
// - getVersion() : Return the current version of the loaded font


class SdlFont
{
public:
    // Constructor is private: SdlFont(TTF_Font* iFont)
    ~SdlFont();

    SdlFont(const SdlFont&) = delete;
    SdlFont& operator=(const SdlFont&) = delete;
    SdlFont(SdlFont&&) = delete;
    SdlFont& operator=(SdlFont&&) = delete;

    // Return the current version of the loaded font
    unsigned int getVersion() const;

    // Friend declaration
    friend class SdlContext;

private:
    // Constructor, only available for SdlContext
    SdlFont(TTF_Font* iFont);

    // Replace the old with the new font pointer, only available for SdlContext
    void replaceOwnedFont(TTF_Font* iFont);

    // Retrieve the owned font pointer, only available for SdlContext
    TTF_Font* getOwnedFont() const;

    TTF_Font* _font;

    // An ID that is incremented every time the font is changed
    unsigned int _version;
};

#endif   // TOB_SDL_FONT

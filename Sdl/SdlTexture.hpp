/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_SDL_TEXTURE
#define TOB_SDL_TEXTURE

#include "Sdl/SdlTypes.hpp"

#include <SDL2/SDL.h>


// See SdlContext.hpp for an introduction into the classes in this directory.

// The SdlTexture class is an opaque wrapper around a SDL_Texture. It is accessed and modified through the SdlContext.

// The following are read-only convenience methods to view properties without referring to the SdlContext:
// - getDimensions() : Return the texture dimensions


class SdlTexture
{
public:
    // Constructor is private: SdlTexture()
    ~SdlTexture();

    SdlTexture(const SdlTexture&) = delete;
    SdlTexture& operator=(const SdlTexture&) = delete;
    SdlTexture(SdlTexture&&) = delete;
    SdlTexture& operator=(SdlTexture&&) = delete;

    // Return the texture dimensions
    SdlDims getDimensions() const;

    // Friend declaration
    friend class SdlContext;

private:
    // Constructor, only available for SdlContext
    SdlTexture(SDL_Texture* iTexture, SdlDims iDimensions);

    // Retrieve the owned texture pointer, only available for SdlContext
    SDL_Texture* getOwnedTexture() const;

    SDL_Texture* _texture;

    SdlDims _dimensions;
};

#endif   // TOB_SDL_TEXTURE

/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "Sdl/SdlContext.hpp"

#include "Main/Log.hpp"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include <string>


bool SdlContext::_anySdlContextExists{false};

SdlContext::SdlContext() :
    _sdlInit{false},
    _sdlImgInit{false},
    _sdlTtfInit{false},
    _prefPathInit{false},
    _prefPath{},
    _basePath{},
    _window{nullptr},
    _render{nullptr}
{
    Main::Log::dbg("SdlContext() start");

    bool aOk{true};
    std::string aError{};

    if (_anySdlContextExists)
    {
        throw SdlCtxException{"SdlContext Error: Duplicate construction"};
    }
    else
    {
        _anySdlContextExists = true;
    }

    if (aOk)
    {
        if (SDL_Init(SDL_INIT_VIDEO) != 0)   // SDL_INIT_VIDEO implies SDL_INIT_EVENTS
        {
            aError = std::string{"SDL_Init Error: "} + SDL_GetError();
            aOk = false;
        }
        else
        {
            _sdlInit = true;
        }
    }

    if (aOk)
    {
        if ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG)
        {
            aError = std::string{"SDL IMG_Init Error: "} + IMG_GetError();
            aOk = false;
        }
        else
        {
            _sdlImgInit = true;
        }
    }

    if (aOk)
    {
        if (TTF_Init() != 0)
        {
            aError = std::string{"SDL TTF_Init Error: "} + TTF_GetError();
            aOk = false;
        }
        else
        {
            _sdlTtfInit = true;
        }
    }

    if (aOk)
    {
        _window = SDL_CreateWindow("Tower Of Babylon",
                                   SDL_WINDOWPOS_CENTERED,
                                   SDL_WINDOWPOS_CENTERED,
                                   50,
                                   50,
                                   SDL_WINDOW_HIDDEN);

        if (_window == nullptr)
        {
            aError = std::string{"SDL_CreateWindow Error: "} + SDL_GetError();
            aOk = false;
        }
    }

    if (aOk)
    {
        // At one itme, using SDL_RENDERER_PRESENTVSYNC caused weird delays at ~20 seconds.
        // Not reproduced for a long time, but leaving fallback code commented out just in case.
        //_render = SDL_CreateRenderer(_window, -1, SDL_RENDERER_ACCELERATED);
        _render = SDL_CreateRenderer(_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

        if (_render == nullptr)
        {
            aError = std::string{"SDL_CreateRenderer Error: "} + SDL_GetError();
            aOk = false;
        }
    }

    if (aOk)
    {
        if (SDL_SetRenderDrawBlendMode(_render, SDL_BLENDMODE_BLEND) != 0)
        {
            aError = std::string{"SDL_SetRenderDrawBlendMode Error: "} + SDL_GetError();
            aOk = false;
        }
    }

    if (aOk)
    {
        char* aBasePathTmp = SDL_GetBasePath();   // build directory
        if (aBasePathTmp == nullptr)
        {
            aError = std::string{"SDL_GetBasePath Error: "} + SDL_GetError();
            aOk = false;
        }
        else
        {
            _basePath = aBasePathTmp;
            SDL_free(aBasePathTmp);
            _basePath += "/../";
        }
    }

    if (!aOk)
    {
        clean();
        _anySdlContextExists = false;
        throw SdlCtxException{aError};
    }

    Main::Log::dbg("SdlContext() end");
}

SdlContext::~SdlContext()
{
    Main::Log::dbg("~SdlContext() start");
    clean();
    _anySdlContextExists = false;
    Main::Log::dbg("~SdlContext() end");
}

// OPERATING SYSTEM INTERACTION:

// Determine the preference path based on the org and app names, and create it if necessary
// Returns true if the path can be determined, and already exists or was created; false otherwise
bool SdlContext::createPrefPath(const std::string& iOrgName, const std::string& iAppName)
{
    if (!_prefPathInit)
    {
        _prefPathInit = true;
        char* aPrefPathTmp = SDL_GetPrefPath(iOrgName.c_str(), iAppName.c_str());

        if (aPrefPathTmp != nullptr)
        {
            _prefPath = aPrefPathTmp;
            SDL_free(aPrefPathTmp);
        }

        return !_prefPath.empty();
    }
    else
    {
        return false;
    }
}

// Return the preference path, where configuration files can be written
// Returns empty string if createPrefPath has not been successuly called previously
const std::string& SdlContext::getPrefPath() const
{
    return _prefPath;
}

// Set the text stored in the clipboard
void SdlContext::setClipboardText(const std::string& iText) const
{
    if (SDL_SetClipboardText(iText.c_str()) != 0)
    {
        std::string aError{"SDL_SetClipboardText Error: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }
}

// INPUTS:

// Poll (pop) one event from the event queue
bool SdlContext::pollEvents(SDL_Event& iEvent) const
{
    return SDL_PollEvent(&iEvent);
}

// Poll (pop) all events from the event queue
void SdlContext::pollAllEvents(std::vector<SDL_Event>& iAllEvents) const
{
    // It is up to the caller whether to clear the vector or not
    SDL_Event aEvent;

    while (SDL_PollEvent(&aEvent))
    {
        iAllEvents.push_back(std::move(aEvent));
    }
}

// GRAPHICS:

// Show the window
void SdlContext::showWindow() const
{
    SDL_ShowWindow(_window);
}

// Hide the window
void SdlContext::hideWindow() const
{
    SDL_HideWindow(_window);
}

void SdlContext::setWindowSize(SdlDims iDimensions) const
{
    if (iDimensions._w < 2 || iDimensions._h < 2)
        throw SdlCtxException{"setWindowSize Error: Invalid window dimensions"};

    SDL_SetWindowSize(_window, iDimensions._w, iDimensions._h);
    SDL_SetWindowPosition(_window, SDL_WINDOWPOS_CENTERED, 0 /*SDL_WINDOWPOS_CENTERED*/);
}

SdlDims SdlContext::getWindowSize() const
{
    SdlDims aDims{};
    SDL_GetWindowSize(_window, &aDims._w, &aDims._h);
    return aDims;
}

// Set the Vsync on or off
void SdlContext::setVsync(bool iVsync)
{
    if (SDL_RenderSetVSync(_render, (iVsync ? 1 : 0)) != 0)
    {
        std::string aError{"SDL_RenderSetVSync Error: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }
}

// Start the rendering for this frame
void SdlContext::renderStart() const
{
    if (SDL_RenderClear(_render) != 0)
    {
        std::string aError{"SDL_RenderClear Error: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }
}

// End the rendering for this frame
void SdlContext::renderEnd() const
{
    // void return, no error handling
    SDL_RenderPresent(_render);
}

// Blit / render the texture at the specified location
void SdlContext::blit(const std::shared_ptr<SdlTexture>& iTexture, SdlCoords iCoords) const
{
    SdlDims aDims{iTexture->getDimensions()};
    SDL_Rect aDestination{iCoords._x, iCoords._y, aDims._w, aDims._h};

    if (SDL_RenderCopy(_render, iTexture->getOwnedTexture(), nullptr, &aDestination) != 0)
    {
        std::string aError{"SDL_RenderCopy Error: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }

}

// Set drawing colour
void SdlContext::setDrawColour(SDL_Colour iColour) const
{
    setDrawColour(iColour.r, iColour.g, iColour.b, iColour.a);
}

// Set drawing colour, fully opaque by default
void SdlContext::setDrawColour(Uint8 iRed, Uint8 iGreen, Uint8 iBlue, Uint8 iAlpha) const
{
    if (SDL_SetRenderDrawColor(_render, iRed, iGreen, iBlue, iAlpha) != 0)
    {
        std::string aError{"SDL_SetRenderDrawColor Error: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }
}

// Set drawing colour
void SdlContext::setDrawColour(uint32_t iHexColour) const
{
    setDrawColour(static_cast<Uint8>((iHexColour & 0xff000000) >> 24),
                  static_cast<Uint8>((iHexColour & 0x00ff0000) >> 16),
                  static_cast<Uint8>((iHexColour & 0x0000ff00) >> 8),
                  static_cast<Uint8>((iHexColour & 0x000000ff)));
}

// Get drawing colour
SDL_Colour SdlContext::getDrawColour() const
{
    SDL_Colour aColour{};

    if (SDL_GetRenderDrawColor(_render, &aColour.r, &aColour.g, &aColour.b, &aColour.a) != 0)
    {
        std::string aError{"SDL_GetRenderDrawColor Error: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }

    return aColour;
}

// Draw a (filled) rectangle
void SdlContext::drawRectangle(SdlCoords iCoords, SdlDims iDimensions, bool iFill) const
{
    SDL_Rect aRectangle{iCoords._x, iCoords._y, iDimensions._w, iDimensions._h};
    if (iFill && SDL_RenderFillRect(_render, &aRectangle) != 0)
    {
        std::string aError{"SDL_RenderFillRect Error: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }
    else if (!iFill && SDL_RenderDrawRect(_render, &aRectangle) != 0)
    {
        std::string aError{"SDL_RenderDrawRect Error: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }
}

// Set a textures alpha value
void SdlContext::setTextureAlpha(std::shared_ptr<SdlTexture>& iTexture, Uint8 iAlpha) const
{
    if (SDL_SetTextureAlphaMod(iTexture->getOwnedTexture(), iAlpha) != 0)
    {
        std::string aError{"SDL_SetTextureAlphaMod Error: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }
}

// Get a textures alpha value
Uint8 SdlContext::getTextureAlpha(const std::shared_ptr<SdlTexture>& iTexture) const
{
    Uint8 aAlpha{};

    if (SDL_GetTextureAlphaMod(iTexture->getOwnedTexture(), &aAlpha) != 0)
    {
        std::string aError{"SDL_GetTextureAlphaMod Error: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }

    return aAlpha;
}

// IMAGES:

// Load image of any format
std::shared_ptr<SdlTexture> SdlContext::loadImage(const std::string& iFilename) const
{
    SDL_Texture* aImage = IMG_LoadTexture(_render, (_basePath + iFilename).c_str());

    if (aImage == nullptr)
    {
        std::string aError{"IMG_LoadTexture Error: "};
        throw SdlCtxException{aError + IMG_GetError()};
    }

    Uint32 aFormat;
    int aAccess;
    int aW;
    int aH;

    if (SDL_QueryTexture(aImage, &aFormat, &aAccess, &aW, &aH) != 0)
    {
        std::string aError{"SDL_QueryTexture Error: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }

    return std::shared_ptr<SdlTexture>{new SdlTexture{aImage, SdlDims{aW, aH}}};
}

// FONTS AND TEXT:

// Load font
std::shared_ptr<SdlFont> SdlContext::loadFont(const std::string& iFilename, int iPointSize) const
{
    // TTF_SetFontOutline -> 1 gives a nice outline effect, could use a big size with a different colour for framing
    TTF_Font* aFont = TTF_OpenFont((_basePath + iFilename).c_str(), iPointSize);

    if (aFont == nullptr)
    {
        std::string aError{"SDL TTF_OpenFont Error: "};
        throw SdlCtxException{aError + TTF_GetError()};
    }

    return std::shared_ptr<SdlFont>{new SdlFont{aFont}};
}

// Calculate the size of the text texture without rendering
SdlDims SdlContext::calculateTextSize(const std::shared_ptr<SdlFont>& iFont, const std::string& iText) const
{
    SdlDims aDims{};
    TTF_SizeUTF8(iFont->getOwnedFont(), iText.c_str(), &aDims._w, &aDims._h);
    return aDims;
}

// Print font information to the debug logs
void SdlContext::logFontInfo(const std::shared_ptr<SdlFont>& iFont) const
{
    Main::Log::dbg("TTF_GetFontStyle() returns ", TTF_GetFontStyle(iFont->getOwnedFont()));
    Main::Log::dbg("TTF_GetFontOutline() returns ", TTF_GetFontOutline(iFont->getOwnedFont()));
    Main::Log::dbg("TTF_GetFontHinting() returns ", TTF_GetFontHinting(iFont->getOwnedFont()));
    Main::Log::dbg("TTF_GetFontKerning() returns ", TTF_GetFontKerning(iFont->getOwnedFont()));
    Main::Log::dbg("TTF_FontHeight() returns ", TTF_FontHeight(iFont->getOwnedFont()));
    Main::Log::dbg("TTF_FontAscent() returns ", TTF_FontAscent(iFont->getOwnedFont()));
    Main::Log::dbg("TTF_FontDescent() returns ", TTF_FontDescent(iFont->getOwnedFont()));
    Main::Log::dbg("TTF_FontLineSkip() returns ", TTF_FontLineSkip(iFont->getOwnedFont()));
    Main::Log::dbg("TTF_FontFaces() returns ", TTF_FontFaces(iFont->getOwnedFont()));
    Main::Log::dbg("TTF_FontFaceIsFixedWidth() returns ", TTF_FontFaceIsFixedWidth(iFont->getOwnedFont()));
    Main::Log::dbg("TTF_FontFaceFamilyName() returns ", TTF_FontFaceFamilyName(iFont->getOwnedFont()));
    Main::Log::dbg("TTF_FontFaceStyleName() returns ", TTF_FontFaceStyleName(iFont->getOwnedFont()));
}

// Create new text object
std::shared_ptr<SdlText> SdlContext::createText(const std::shared_ptr<SdlFont>& iFont, const std::string& iText) const
{
    std::shared_ptr<SdlText> aText{new SdlText{iFont, iText}};

    // Generate at construction, so the texture dimensions are available immediately
    if (aText->isGenerateTextureNeeded(getDrawColour()))
    {
        generateTextTexture(aText);
    }

    return aText;
}

// Update the text content of an existing text object
// Returns true if the text actually changed, and the texture was regenerated; false otherwise
bool SdlContext::updateText(const std::string& iNewText, std::shared_ptr<SdlText>& ioText) const
{
    bool aUpdated = ioText->updateText(iNewText);

    if (aUpdated || ioText->isGenerateTextureNeeded(getDrawColour()))
    {
        generateTextTexture(ioText);
        aUpdated = true;
    }

    return aUpdated;
}

// Blit / render the text at the specified location
void SdlContext::blitText(SdlCoords iCoords, const std::shared_ptr<SdlText>& iText) const
{
    if (iText->isGenerateTextureNeeded(getDrawColour()))
    {
        generateTextTexture(iText);
    }

    blit(iText->getTexture(), iCoords);
}

// Cleaning method, only to be used by the contructors and destructor
void SdlContext::clean()
{
    if (_render)
    {
        SDL_DestroyRenderer(_render);
    }

    if (_window)
    {
        SDL_DestroyWindow(_window);
    }

    if (_sdlTtfInit)
    {
        TTF_Quit();
    }

    if (_sdlImgInit)
    {
        IMG_Quit();
    }

    if (_sdlInit)
    {
        SDL_Quit();
    }
}

// Helper method to generate and store the texture representing the text object
void SdlContext::generateTextTexture(const std::shared_ptr<SdlText>& iText) const
{
    SDL_Colour aColour = getDrawColour();
    SDL_Surface* aSurface = TTF_RenderUTF8_Blended(iText->getFont()->getOwnedFont(), iText->getText().c_str(), aColour);

    if (aSurface == nullptr)
    {
        std::string aError{"SDL TTF_RenderText_Blended Error: "};
        throw SdlCtxException{aError + TTF_GetError()};
    }

    SDL_Texture* aTexture = SDL_CreateTextureFromSurface(_render, aSurface);
    SDL_FreeSurface(aSurface);
    aSurface = nullptr;

    if (aTexture == nullptr)
    {
        std::string aError{"SDL_CreateTextureFromSurface Error: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }

    Uint32 aFormat;
    int aAccess;
    int aW;
    int aH;

    if (SDL_QueryTexture(aTexture, &aFormat, &aAccess, &aW, &aH) != 0)
    {
        std::string aError{"SDL_QueryTexture Error: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }

    std::shared_ptr<SdlTexture> aNewTexture{new SdlTexture{aTexture, SdlDims{aW, aH}}};
    iText->replaceTexture(iText->getFont()->getVersion(), aColour, aNewTexture);
}

/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_SDL_CONTEXT
#define TOB_SDL_CONTEXT

#include "Sdl/SdlFont.hpp"
#include "Sdl/SdlText.hpp"
#include "Sdl/SdlTexture.hpp"
#include "Sdl/SdlTypes.hpp"

#include <SDL2/SDL.h>

#include <memory>
#include <string>
#include <vector>


// This Sdl directory is NOT an attempt to provide a full Object Orientated wrapper around SDL.
// Rather, the goals are simpler:
// - wrap SDL function calls returning errors into methods with exceptions;
// - create a class for the most basic Windowing functions, and provide lifetime management over it;
// - provide a simple foundation for common GUI application patterns;
// - use that class as the entry point for calling the underlying and other SDL functions.

// The most important class is this SdlContext, which initialises SDL, and various SDL sub-projects (eg TTF).
// It provides methods that create and manipulate other SDL "objects", which are wrapped in opaque classes.

// The opaque classes do not modify their owned SDL "objects", but may provide read-only convenience methods to
//  view properties of those "objects", provided they are cached or otherwise known; ie no SDL functions are called.


class SdlContext
{
public:
    SdlContext();
    virtual ~SdlContext();

    SdlContext(const SdlContext&) = delete;
    SdlContext& operator=(const SdlContext&) = delete;
    SdlContext(SdlContext&&) = delete;
    SdlContext& operator=(SdlContext&&) = delete;

    // OPERATING SYSTEM INTERACTION:

    // Determine the preference path based on the org and app names, and create it if necessary
    // Returns true if the path can be determined, and already exists or was created; false otherwise
    bool createPrefPath(const std::string& iOrgName, const std::string& iAppName);

    // Return the preference path, where configuration files can be written
    // Returns empty string if createPrefPath has not been successuly called previously
    const std::string& getPrefPath() const;

    // Set the text stored in the clipboard
    void setClipboardText(const std::string& iText) const;

    // INPUTS:

    // Poll (pop) one event from the event queue
    bool pollEvents(SDL_Event& iEvent) const;

    // Poll (pop) all events from the event queue
    // It is up to the caller whether to clear the vector or not
    void pollAllEvents(std::vector<SDL_Event>& iAllEvents) const;

    // GRAPHICS:

    // Show the window
    void showWindow() const;

    // Hide the window
    void hideWindow() const;

    // Set the window size
    void setWindowSize(SdlDims iDimensions) const;

    // Get the window size
    SdlDims getWindowSize() const;

    // Set the Vsync on or off
    void setVsync(bool iVsync);

    // Start the rendering for this frame
    void renderStart() const;

    // End the rendering for this frame
    void renderEnd() const;

    // Blit / render the texture at the specified location
    void blit(const std::shared_ptr<SdlTexture>& iTexture, SdlCoords iCoords) const;

    // Set drawing colour
    void setDrawColour(SDL_Colour iColour) const;
    void setDrawColour(Uint8 iRed, Uint8 iGreen, Uint8 iBlue, Uint8 iAlpha = SDL_ALPHA_OPAQUE) const;
    void setDrawColour(uint32_t iHexColour) const;

    // Get drawing colour
    SDL_Colour getDrawColour() const;

    // Draw a filled (by-default) rectangle
    void drawRectangle(SdlCoords iCoords, SdlDims iDimensions, bool iFill = true) const;

    // Set a textures alpha value
    void setTextureAlpha(std::shared_ptr<SdlTexture>& iTexture, Uint8 iAlpha) const;

    // Get a textures alpha value
    Uint8 getTextureAlpha(const std::shared_ptr<SdlTexture>& iTexture) const;

    // IMAGES:

    // Load image of any format
    std::shared_ptr<SdlTexture> loadImage(const std::string& iFilename) const;

    // FONTS AND TEXT:

    // Load font
    std::shared_ptr<SdlFont> loadFont(const std::string& iFilename, int iPointSize) const;

    //XYZ reloadFont(const std::string& iFilename, int iPointSize, const std::shared_ptr<SdlFont>&) const;

    // Print font information to the debug logs
    void logFontInfo(const std::shared_ptr<SdlFont>& iFont) const;

    // Calculate the size of the text texture without rendering
    SdlDims calculateTextSize(const std::shared_ptr<SdlFont>& iFont, const std::string& iText) const;

    // Create new text object
    std::shared_ptr<SdlText> createText(const std::shared_ptr<SdlFont>& iFont, const std::string& iText = "") const;

    // Update the text content of an existing text object
    // Returns true if the text actually changed, and the texture was regenerated; false otherwise
    bool updateText(const std::string& iNewText, std::shared_ptr<SdlText>& ioText) const;

    // Blit / render the text at the specified location
    void blitText(SdlCoords iCoords, const std::shared_ptr<SdlText>& iText) const;

private:
    // Cleaning method, only to be used by the contructors and destructor
    void clean();

    // Helper method to generate and store the texture representing the text object
    void generateTextTexture(const std::shared_ptr<SdlText>& iText) const;

    // Enforce only 1 SDL Context exists at once
    static bool _anySdlContextExists;

    bool _sdlInit;
    bool _sdlImgInit;
    bool _sdlTtfInit;
    bool _prefPathInit;
    std::string _prefPath;
    std::string _basePath;
    SDL_Window* _window;
    SDL_Renderer* _render;
};

#endif   // TOB_SDL_CONTEXT

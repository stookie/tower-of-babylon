/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2023 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "Sdl/SdlFont.hpp"

#include "Main/Log.hpp"
#include "Sdl/SdlTypes.hpp"


SdlFont::SdlFont(TTF_Font* iFont)
{
    if (iFont == nullptr)
    {
        throw SdlCtxException{"SdlFont Error: nullptr"};
    }

    _font = iFont;
    _version = {0};
}

SdlFont::~SdlFont()
{
    Main::Log::dbg("~SdlFont() start");

    if (_font != nullptr)
    {
        TTF_CloseFont(_font);
    }

    _font = nullptr;
    Main::Log::dbg("~SdlFont() end");
}

// Return the current version of the loaded font
unsigned int SdlFont::getVersion() const
{
    return _version;
}

// Replace the old with the new font pointer, only available for SdlContext
void SdlFont::replaceOwnedFont(TTF_Font* iFont)
{
    if (iFont == nullptr)
    {
        throw SdlCtxException{"SdlFont Error: nullptr"};
    }

    if (_font != nullptr)
    {
        TTF_CloseFont(_font);
    }

    _font = iFont;
    _version++;
}

// Retrieve the owned font pointer, only available for SdlContext
TTF_Font* SdlFont::getOwnedFont() const
{
    return _font;
}

/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_SDL_TYPES
#define TOB_SDL_TYPES

#include <stdexcept>


// See SdlContext.hpp for an introduction into the classes in this directory.

// This file provides various type aliases and simple type definitions commonly used in the Sdl classes.


// The exception type used to indicate failures by the SDL Context
class SdlCtxException : public std::runtime_error
{
    using std::runtime_error::runtime_error;
};

// Structure for coordinates
struct SdlCoords
{
    int _x;
    int _y;
};

// Structure for dimensions
struct SdlDims
{
    int _w;
    int _h;
};

// SDL_Rect used as-is

// SDL_Colour used as-is


#endif   // TOB_SDL_TYPES

/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2023 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "Sdl/SdlText.hpp"

#include "Main/Log.hpp"
#include "Sdl/SdlTypes.hpp"

#include <SDL2/SDL_ttf.h>


SdlText::SdlText(const std::shared_ptr<SdlFont>& iFont, const std::string& iText)
{
    if (iFont == nullptr)
    {
        throw SdlCtxException{"SdlText Error: SdlFont nullptr"};
    }

    _font = iFont;
    _fontVersion = iFont->getVersion();
    _text = iText;
    _colour = SDL_Colour{};
    _texture = nullptr;
    _regenerateNeeded = true;
}

SdlText::~SdlText()
{
    Main::Log::dbg("~SdlText() start: ", _text);
    Main::Log::dbg("~SdlText() end: ", _text);
}

// Return the text dimensions
SdlDims SdlText::getDimensions() const
{
    if (_texture == nullptr)
    {
        throw SdlCtxException{"SdlText Error: SdlTexture nullptr"};
    }

    return _texture->getDimensions();
}

// Check if the texture representing the text requires regenerating, only available for SdlContext
bool SdlText::isGenerateTextureNeeded(SDL_Colour iColour)
{
    if (!_regenerateNeeded)
    {
        if (_font->getVersion() != _fontVersion || _texture == nullptr)
        {
            _regenerateNeeded = true;
        }
        else if (_colour.r != iColour.r || _colour.g != iColour.g || _colour.b != iColour.b || _colour.a != iColour.a)
        {
            _regenerateNeeded = true;
        }
    }

    return _regenerateNeeded;
}

// Update the text, only available for SdlContext
// Returns true if the text actually changes, and the texture needs to be regenerated; false otherwise
bool SdlText::updateText(const std::string& iText)
{
    if (iText != _text)
    {
        _text = iText;
        _texture.reset();   // in case getDimensions() is called before the texture is replaced
        _regenerateNeeded = true;
        return true;
    }
    else
    {
        return false;
    }
}

// Replace the texture representing the text, only available for SdlContext
void SdlText::replaceTexture(unsigned int iFontVersion, SDL_Colour iColour, const std::shared_ptr<SdlTexture>& iTexture)
{
    Main::Log::dbg("SdlText::replaceTexture(): ", _text);
    _fontVersion = iFontVersion;
    _colour = iColour;
    _texture = iTexture;
    _regenerateNeeded = false;
}

// Retrieve the texture representing the text, only available for SdlContext
std::shared_ptr<SdlTexture> SdlText::getTexture() const
{
    return _texture;
}

// Retrieve the font, only available for SdlContext
std::shared_ptr<SdlFont> SdlText::getFont() const
{
    return _font;
}

// Retrieve the text, only available for SdlContext
std::string SdlText::getText() const
{
    return _text;
}

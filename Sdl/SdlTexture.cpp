/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2023 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "Sdl/SdlTexture.hpp"

#include "Main/Log.hpp"


SdlTexture::SdlTexture(SDL_Texture* iTexture, SdlDims iDimensions)
{
    if (iTexture == nullptr)
    {
        throw SdlCtxException{"SdlTexture Error: nullptr"};
    }
    else if (iDimensions._w < 1 || iDimensions._h < 1)
    {
        throw SdlCtxException{"SdlTexture Error: invalid dimensions"};
    }

    _texture = iTexture;
    _dimensions = iDimensions;
}

SdlTexture::~SdlTexture()
{
    Main::Log::dbg("~SdlTexture() start");

    if (_texture != nullptr)
    {
        SDL_DestroyTexture(_texture);
    }

    _texture = nullptr;
    Main::Log::dbg("~SdlTexture() end");
}

// Return the texture dimensions
SdlDims SdlTexture::getDimensions() const
{
    return _dimensions;
}

// Retrieve the owned texture pointer, only available for SdlContext
SDL_Texture* SdlTexture::getOwnedTexture() const
{
    return _texture;
}

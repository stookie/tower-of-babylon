# Authors:
# stookie <4303586-stookie@users.noreply.gitlab.com>
# Copyright (C) 2021-2023 authors
# Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.

project(tower_of_babylon)

cmake_minimum_required(VERSION 2.8.9)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${tower_of_babylon_SOURCE_DIR}/cmake")

list(APPEND tower_of_babylon_SOURCES Main.cpp)

include(GameTob/CMakeLists.txt)
include(GameTob/Gui/CMakeLists.txt)
include(GameTob/Play/CMakeLists.txt)
include(Main/CMakeLists.txt)
include(Options/CMakeLists.txt)
include(Sdl/CMakeLists.txt)
include(Toolkit/GuiSimple/CMakeLists.txt)
include(Utils/CMakeLists.txt)

include_directories(${tower_of_babylon_SOURCE_DIR})

add_executable(tower_of_babylon ${tower_of_babylon_SOURCES})

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wno-unused-parameter -pedantic -std=c++11")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_DEBUG} -g")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_RELEASE} -O2")

find_package(SDL2 REQUIRED)
find_package(SDL2_image REQUIRED)
find_package(SDL2_ttf REQUIRED)

target_link_libraries(tower_of_babylon ${SDL2_LIBRARY} ${SDL2_IMAGE_LIBRARY} ${SDL2_TTF_LIBRARY})

include_directories(${SDL2_INCLUDE_DIR} ${SDL2_IMAGE_INCLUDE_DIR})

/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2023 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "Utils/RandomGenerator.hpp"

#include "Main/Log.hpp"

#include <chrono>


namespace Utils
{

RandomGenerator::RandomGenerator() :
    _generator{},   // uses implementation-defined fixed default seed, so the seed should be set after construction
    _uniformInt{},
    _diceUniformIntParams{1, 6}
{
}

// Sets the random engine seed, reinitialising it in the process
void RandomGenerator::setSeed(SeedType iSeed)
{
    Main::Log::info("Initialising random generator with the explicitly-set seed: ", iSeed);
    _generator.seed(iSeed);
}

// Sets the random engine seed to one based on the current time (ie pseudo-random), reinitialising it in the process
void RandomGenerator::setTimeBasedSeed()
{
    // Use the steady clock "now" time ticks-since-epoch for the random generator seed
    auto aTicksSinceEpoch = std::chrono::steady_clock::now().time_since_epoch().count();
    SeedType aSeed = static_cast<SeedType>(aTicksSinceEpoch);
    Main::Log::info("Initialising random generator with the time-based seed: ", aSeed);
    _generator.seed(aSeed);
}

// Returns random number between 1 and 6, with equal probability
RandomGenerator::UnsignedShort RandomGenerator::dieRoll()
{
    return _uniformInt(_generator, _diceUniformIntParams);
}

// Returns true or false, with equal probability
bool RandomGenerator::trueOrFalse()
{
    // For now, reuse the dice parameters
    return (_uniformInt(_generator, _diceUniformIntParams) <= 3);
}

// Returns random number between the minimum and maximum inclusive, with equal probability
RandomGenerator::UnsignedShort RandomGenerator::selectNumber(UnsignedShort iMin, UnsignedShort iMax)
{
    return _uniformInt(_generator, std::uniform_int_distribution<UnsignedShort>::param_type{iMin, iMax});
}

}   // namespace Utils

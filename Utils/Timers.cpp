/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2023 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "Utils/Timers.hpp"


namespace Utils
{

SimpleTimer::SimpleTimer() :
    _active{false},
    _startTime{},
    _duration{}
{
}

// Start the timer, for the specified duration
void SimpleTimer::start(const Main::GlobalContext& iCtx, Main::GlobalContext::SteadyDuration iDuration)
{
    _active = true;
    _duration = iDuration;
    _startTime = iCtx.getNowTime();
}

// Reset the timer (make inactive)
void SimpleTimer::reset()
{
    _active = false;
}

// Has the timer expired (and is active)?
bool SimpleTimer::isExpired(const Main::GlobalContext& iCtx) const
{
    return (_active && iCtx.getNowTime() >= _startTime + _duration);
}

// Is the timer still counting (and is active)?
bool SimpleTimer::isCounting(const Main::GlobalContext& iCtx) const
{
    return (_active && iCtx.getNowTime() < _startTime + _duration);
}

}   // namespace Utils

/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2023 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_UTILS_TIMERS
#define TOB_UTILS_TIMERS

#include "Main/GlobalContext.hpp"


namespace Utils
{

class SimpleTimer
{
public:
    SimpleTimer();

    ~SimpleTimer() = default;
    SimpleTimer(const SimpleTimer&) = delete;
    SimpleTimer& operator=(const SimpleTimer&) = delete;
    SimpleTimer(SimpleTimer&&) = delete;
    SimpleTimer& operator=(SimpleTimer&&) = delete;

    // Start the timer, for the specified duration
    void start(const Main::GlobalContext& iCtx, Main::GlobalContext::SteadyDuration iDuration);

    // Reset the timer (make inactive)
    void reset();

    // Has the timer expired (and is active)?
    bool isExpired(const Main::GlobalContext& iCtx) const;

    // Is the timer still counting (and is active)?
    bool isCounting(const Main::GlobalContext& iCtx) const;

private:
    bool _active;
    Main::GlobalContext::SteadyTimePoint _startTime;
    Main::GlobalContext::SteadyDuration _duration;
};

}   // namespace Utils

#endif   // TOB_UTILS_TIMERS

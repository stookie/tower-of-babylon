/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_UTILS_LOADER_HELPERS
#define TOB_UTILS_LOADER_HELPERS

#include <istream>
#include <string>


namespace Utils
{

// Retrieve the expected Key string from the stream; throws Main::RuntimeException if it is not successful.
// Mainly intended for use by other functions in this file, but could also be used externally.
inline void loadKeyOnly(std::istream& iStr, const std::string& iKeyString)
{
    using Exc = Main::RuntimeException;

    if (!iStr.good())
    {
        throw Exc("Utils::loadKeysOnly Error: stream not ready for reading '" + iKeyString + "'");
    }

    std::string aKey{};
    iStr >> aKey;

    if (!iStr.good())
    {
        throw Exc("Utils::loadKeysOnly Error: stream not ready after reading '" + iKeyString + "'");
    }
    else if (aKey != iKeyString)
    {
        throw Exc("Utils::loadKeysOnly Error: stream expected '" + iKeyString + "' but read '" + aKey + "'");
    }
}

// Retrieve a Value of type T from a stream; throws Main::RuntimeException if it is not successful.
template <typename T>
T loadKeysValue(std::istream& iStr, const std::string& iKeyString)
{
    using Exc = Main::RuntimeException;

    loadKeyOnly(iStr, iKeyString);

    T aT{};
    iStr >> aT;

    if (!iStr.good())
    {
        throw Exc("Utils::loadKeysValue Error: stream cannot read '" + iKeyString + "' value");
    }

    return aT;
}

// Retrieve the remainder of the line from a stream; throws Main::RuntimeException if it is not successful.
inline std::string loadKeysLine(std::istream& iStr, const std::string& iKeyString)
{
    using Exc = Main::RuntimeException;

    loadKeyOnly(iStr, iKeyString);

    char aC{};
    iStr.get(aC);

    if (!iStr.good() || aC != ' ')
    {
        throw Exc("Utils::loadKeysLine Error: stream expected a space character after reading '" + iKeyString + "'");
    }

    std::string aLine{};
    std::getline(iStr, aLine);

    if (!iStr.good())
    {
        throw Exc("Utils::loadKeysLine Error: stream cannot read '" + iKeyString + "' line remainder");
    }

    return aLine;
}

}   // namespace Utils

#endif   // TOB_UTILS_LOADER_HELPERS

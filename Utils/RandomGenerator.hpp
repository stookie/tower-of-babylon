/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2022 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_UTILS_RANDOM_GENERATOR
#define TOB_UTILS_RANDOM_GENERATOR

#include <random>


namespace Utils
{

class RandomGenerator
{
public:
    // SeedType = uint_fast32_t : fastest unsigned integer type with width of at least 32 bits
    using SeedType = std::minstd_rand::result_type;

    RandomGenerator();   // uses implementation-defined fixed default seed, so the seed should be set after construction
    ~RandomGenerator() = default;

    RandomGenerator(const RandomGenerator&) = delete;
    RandomGenerator& operator=(const RandomGenerator&) = delete;
    RandomGenerator(RandomGenerator&&) = delete;
    RandomGenerator& operator=(RandomGenerator&&) = delete;

    // Sets the random engine seed, reinitialising it in the process
    void setSeed(SeedType iSeed);

    // Sets the random engine seed to one based on the current time (ie pseudo-random), reinitialising it in the process
    void setTimeBasedSeed();

    using UnsignedShort = unsigned short int;

    // Returns random number between 1 and 6, with equal probability
    UnsignedShort dieRoll();

    // Returns true or false, with equal probability
    bool trueOrFalse();

    // Returns random number between the minimum and maximum inclusive, with equal probability
    UnsignedShort selectNumber(UnsignedShort iMin, UnsignedShort iMax);

private:
    std::minstd_rand _generator;
    std::uniform_int_distribution<UnsignedShort> _uniformInt;
    std::uniform_int_distribution<UnsignedShort>::param_type _diceUniformIntParams;
};

}   // namespace Utils

#endif   // TOB_UTILS_RANDOM_GENERATOR

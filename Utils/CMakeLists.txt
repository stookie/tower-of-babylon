# Authors:
# stookie <4303586-stookie@users.noreply.gitlab.com>
# Copyright (C) 2021-2023 authors
# Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.

list(APPEND tower_of_babylon_SOURCES
    ${CMAKE_CURRENT_LIST_DIR}/RandomGenerator.cpp
    ${CMAKE_CURRENT_LIST_DIR}/Timers.cpp
)

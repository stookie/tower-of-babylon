/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_GAME_TOB_CONSTANTS
#define TOB_GAME_TOB_CONSTANTS

#include <string>


// Constants related specifically to the TOB game


namespace GameTob
{

//XYZ THESE SHOULD BE DERIVED FROM WINDOW SIZE, LATER, ONCE DECIDED ON FONT; ALSO REWORKED TO REFER TO A RESOLUTION

// The font details (intended for 800x800 windows)
const std::string kFontFile{"resources/KaushanScript-Regular.otf"};
constexpr unsigned int kDefaultLargeFontSize{48};
constexpr unsigned int kDefaultNormalFontSize{36};
constexpr unsigned int kDefaultSmallFontSize{24};

// Images loaded from files (intended for 800x800 windows):

const std::string kFeltBackground{"resources/felt.jpg"};   // no restriction on image dimensions

// Die images should be 70x70
constexpr unsigned int kDieImageDimension{70};
const std::string kDieImageBlank_70{"resources/diceblank_70.png"};
const std::string kDieImage2_70{"resources/dice2_70.png"};
const std::string kDieImage3_70{"resources/dice3_70.png"};
const std::string kDieImage4_70{"resources/dice4_70.png"};
const std::string kDieImage5_70{"resources/dice5_70.png"};
const std::string kDieImage6_70{"resources/dice6_70.png"};
const std::string kDieImage10_70{"resources/dice10_70.png"};

// Player and round ranges
constexpr std::size_t kMinPlayerNumber{1};
constexpr std::size_t kMaxPlayerNumber{8};
constexpr std::size_t kMinRoundsNumber{1};
constexpr std::size_t kMaxRoundsNumber{7};

}   // namespace GameTob

#endif   // TOB_GAME_TOB_CONSTANTS

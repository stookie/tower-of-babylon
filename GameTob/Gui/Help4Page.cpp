/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "GameTob/Gui/Help4Page.hpp"

#include "GameTob/Gui/Help3Page.hpp"
#include "Main/GlobalContext.hpp"
#include "Toolkit/GuiObject.hpp"

#include <SDL2/SDL_events.h>


namespace
{
const std::string kText1a{"One catch, the first time you end your turn to bank points,"};
const std::string kText1b{"you must have scored 500 points or more."};

const std::string kText2a{"As a beginner's strategy, aim to keep 50 and 100 point dice."};
const std::string kText2b{"They score points individually and score high points as multiples."};

const std::string kText3{"Remember:"};
const std::string kText3a{"* On each roll, you must score extra points or you are bust;"};
const std::string kText3b{"* To continue rolling, you must keep some or all point scoring dice;"};
const std::string kText3c{"* Only dice that score extra points may be kept;"};
const std::string kText3d{"* None of it counts though, unless you end your turn and bank the points."};
}


namespace GameTob
{
namespace Gui
{

Help4Page::Help4Page(Main::GlobalContext& iCtx) :
    Toolkit::GuiPage{},
    _titleLabel{iCtx.sdlCtx(), iCtx.resources().getLargeFont(), "Help: Summary"},
    _backButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Back"},
    _prevButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Previous"},
    _nextButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Next"},
    _help1aLabel{iCtx.sdlCtx(), iCtx.resources().getSmallFont(), kText1a},
    _help1bLabel{iCtx.sdlCtx(), iCtx.resources().getSmallFont(), kText1b},
    _help2aLabel{iCtx.sdlCtx(), iCtx.resources().getSmallFont(), kText2a},
    _help2bLabel{iCtx.sdlCtx(), iCtx.resources().getSmallFont(), kText2b},
    _help3Label{iCtx.sdlCtx(), iCtx.resources().getSmallFont(), kText3},
    _help3aLabel{iCtx.sdlCtx(), iCtx.resources().getSmallFont(), kText3a},
    _help3bLabel{iCtx.sdlCtx(), iCtx.resources().getSmallFont(), kText3b},
    _help3cLabel{iCtx.sdlCtx(), iCtx.resources().getSmallFont(), kText3c},
    _help3dLabel{iCtx.sdlCtx(), iCtx.resources().getSmallFont(), kText3d},
    _allGuiObjects{&_titleLabel, &_backButton, &_prevButton, &_nextButton,
                   &_help1aLabel, &_help1bLabel, &_help2aLabel, &_help2bLabel,
                   &_help3Label, &_help3aLabel, &_help3bLabel, &_help3cLabel, &_help3dLabel}
{
    // Position the title half way along the x-axis
    _titleLabel.setPosition({((iCtx.sdlCtx().getWindowSize()._w - _titleLabel.getSize()._w) / 2), 0});

    _help1aLabel.setPosition({40,100});
    _help1bLabel.setPosition({40,150});
    _help2aLabel.setPosition({40,250});
    _help2bLabel.setPosition({40,300});
    _help3Label.setPosition({40,400});
    _help3aLabel.setPosition({40,450});
    _help3bLabel.setPosition({40,500});
    _help3cLabel.setPosition({40,550});
    _help3dLabel.setPosition({40,600});

    // Position the back then next then previous buttons at the bottom right, with some padding
    _backButton.setPosition({iCtx.sdlCtx().getWindowSize()._w * 99 / 100 - _backButton.getSize()._w,
                             iCtx.sdlCtx().getWindowSize()._h * 99 / 100 - _backButton.getSize()._h});

    _nextButton.setPosition({_backButton.getPosition()._x - iCtx.sdlCtx().getWindowSize()._w * 1 / 100 -
                                 _nextButton.getSize()._w,
                             iCtx.sdlCtx().getWindowSize()._h * 99 / 100 - _nextButton.getSize()._h});

    _prevButton.setPosition({_nextButton.getPosition()._x - iCtx.sdlCtx().getWindowSize()._w * 1 / 100 -
                                 _prevButton.getSize()._w,
                             iCtx.sdlCtx().getWindowSize()._h * 99 / 100 - _prevButton.getSize()._h});

    // No previous page from page 1
    _nextButton.setState(Toolkit::GuiObjectState::kInactive);
}

Help4Page::~Help4Page()
{
}

void Help4Page::handleEvents(const std::vector<SDL_Event>& iEvents, Main::GlobalContext& iCtx)
{
    for (auto* aGuiObject : _allGuiObjects)
    {
        aGuiObject->handleEvents(iEvents);
    }

    if (_backButton.wasClicked())
    {
        setPageState(Toolkit::GuiPageState::kReturn);
    }
    else if (_prevButton.wasClicked())
    {
        setPageState(Toolkit::GuiPageState::kSwapPage);
    }
    else
    {
        setPageState(Toolkit::GuiPageState::kActivePage);
    }
}

void Help4Page::draw(Main::GlobalContext& iCtx)
{
    iCtx.sdlCtx().setDrawColour(Main::kColourSolidRed);

    for (auto* aGuiObject : _allGuiObjects)
    {
        aGuiObject->draw(iCtx.sdlCtx());
    }
}

std::unique_ptr<Toolkit::GuiPage> Help4Page::newPage(Main::GlobalContext& iCtx)
{
    if (getPageState() == Toolkit::GuiPageState::kNextPage || getPageState() == Toolkit::GuiPageState::kSwapPage)
    {
        // Same order as handleEvents()
        if (_prevButton.wasClicked() && getPageState() == Toolkit::GuiPageState::kSwapPage)
        {
            return std::unique_ptr<Toolkit::GuiPage>{new Help3Page{iCtx}};
        }
    }

    return nullptr;
}

}   // namespace Gui
}   // namespace GameTob

/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_GAME_TOB_GUI_GAMEOPTIONSPAGE
#define TOB_GAME_TOB_GUI_GAMEOPTIONSPAGE

#include "GameTob/GameOptions.hpp"
#include "Toolkit/GuiPage.hpp"
#include "Toolkit/GuiSimple/TextButton.hpp"
#include "Toolkit/GuiSimple/TextCheckbox.hpp"
#include "Toolkit/GuiSimple/TextLabel.hpp"

#include <string>
#include <vector>

namespace Main
{
class GlobalContext;
}


namespace GameTob
{
namespace Gui
{

class GameOptionsPage : virtual public Toolkit::GuiPage
{
public:
    GameOptionsPage(Main::GlobalContext& iCtx, bool iForGameNotSettings);
    virtual ~GameOptionsPage() override;

    virtual void handleEvents(const std::vector<SDL_Event>& iEvents, Main::GlobalContext& iCtx) override;

    virtual void draw(Main::GlobalContext& iCtx) override;

    // The new page when the next page or a page swap is needed
    virtual std::unique_ptr<Toolkit::GuiPage> newPage(Main::GlobalContext& iCtx) override;

private:
    // Set the state of the GUI controls
    void setControlsState(Main::GlobalContext& iCtx, bool iPlayers, bool iRounds, bool iAnythingElse);

    // Update the player number and rounds number labels
    void updatePlayerNumberLabel(Main::GlobalContext& iCtx);
    void updateRoundsNumberLabel(Main::GlobalContext& iCtx);

    Toolkit::GuiSimple::TextLabel _titleLabel;
    Toolkit::GuiSimple::TextLabel _playerNumberLabel;
    Toolkit::GuiSimple::TextLabel _roundsNumberLabel;
    Toolkit::GuiSimple::TextLabel _hidePlayersTurnScoreLabel;
    Toolkit::GuiSimple::TextButton _playerNumberUpArrowButton;//XYZ ADD TO RESOURCES AS COMMON CLONEABLE?
    Toolkit::GuiSimple::TextButton _playerNumberDownArrowButton;
    Toolkit::GuiSimple::TextButton _roundsNumberUpArrowButton;
    Toolkit::GuiSimple::TextButton _roundsNumberDownArrowButton;
    Toolkit::GuiSimple::TextCheckbox _hidePlayersTurnScoreCheckbox;
    Toolkit::GuiSimple::TextButton _startOrAcceptButton;
    Toolkit::GuiSimple::TextButton _cancelButton;
    Toolkit::GuiSimple::TextButton _resetButton;

    // Collection of all GUI objects to allow easy iteration
    std::vector<Toolkit::GuiObject*> _allGuiObjects;

    const bool _forGameNotSettings;   // true when used for game preparation, false when used for defaults in settings
    GameTob::GameOptions _localOptions;
};

}   // namespace Gui
}   // namespace GameTob

#endif   // TOB_GAME_TOB_GUI_GAMEOPTIONSPAGE

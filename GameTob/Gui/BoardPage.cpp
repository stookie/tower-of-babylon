/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "GameTob/Gui/BoardPage.hpp"

#include "GameTob/GameOptions.hpp"
#include "GameTob/Gui/Resources.hpp"
#include "Main/Constants.hpp"
#include "Main/Exceptions.hpp"
#include "Main/GlobalContext.hpp"
#include "Sdl/SdlContext.hpp"
#include "Toolkit/GuiObject.hpp"

#include <SDL2/SDL_events.h>


namespace
{
const std::string kTitlePrefix{"Round "};
const std::string kTitleInfix{" of "};
const std::string kTurnScorePrefix{"Score: "};
}


namespace GameTob
{
namespace Gui
{

BoardPage::BoardPage(Main::GlobalContext& iCtx) :
    Toolkit::GuiPage{},
    _titleLabel{iCtx.sdlCtx(), iCtx.resources().getLargeFont(), "TBC"},
    _playersHeadingLabel{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Players"},
    _scoresHeadingLabel{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Score"},
    _roundsHeadingLabel{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Rounds"},
    _playerLabels{},   // dynamically add to _allGuiObjects later
    _scoreLabels{},   // dynamically add to _allGuiObjects later
    _roundLabels{},   // dynamically add to _allGuiObjects later
    _horizontalLine{},
    _verticalLine1{},
    _verticalLine2{},
    _activePlayerLabel{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "TBC"},
    _dieButtons{},   // dynamically add to _allGuiObjects later
    _keepDieCheckboxes{},   // dynamically add to _allGuiObjects later
    _keepDieLabels{},   // dynamically add to _allGuiObjects later
    _turnScoreLabel{iCtx.sdlCtx(), iCtx.resources().getSmallFont(), "TBC"},
    _rollAgainButton{iCtx.sdlCtx(), iCtx.resources().getSmallFont(), "Roll Again?"},
    _endTurnButton{iCtx.sdlCtx(), iCtx.resources().getSmallFont(), "End Turn?"},
    _quitButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Quit"},
    _fixedKeepLabel{iCtx.sdlCtx(), iCtx.resources().getSmallFont(), "Keep?"},   // do not add to _allGuiObjects
    _fixedKeptLabel{iCtx.sdlCtx(), iCtx.resources().getSmallFont(), "Kept!"},   // do not add to _allGuiObjects
    _allGuiObjects{&_titleLabel, &_playersHeadingLabel, &_scoresHeadingLabel, &_roundsHeadingLabel,
                   &_horizontalLine, &_verticalLine1, &_verticalLine2, &_activePlayerLabel,
                   &_turnScoreLabel, &_rollAgainButton, &_endTurnButton,
                   &_quitButton},
    _state{iCtx},
    _quitDoubleClickTimer{},
    _background{dynamic_cast<GameTob::Gui::Resources&>(iCtx.resources()).getFeltTexture(),
                Toolkit::GuiSimple::BackgroundType::kTiled}
{
    _titleLabel.setText(iCtx.sdlCtx(),
                        (kTitlePrefix + std::to_string(_state.getCurrentRound() + 1) +
                         kTitleInfix + std::to_string(_state.getMaximumRounds())),
                        false);

    if (_state.getPlayerNumber() == 0)
    {
        throw Main::LogicException{"BoardPage::BoardPage Error: player number is zero"};
    }

    // Very important to reserve in advance the necessary memory for the vectors. If they are allowed to grow
    //  dynamically, pointers to the vector content will be invalidated when reallocated. Necessary for the
    //  _allGuiObjects vector of pointers.
    const std::size_t aPlayersNumber{_state.getPlayerNumber()};
    _roundLabels.reserve(aPlayersNumber);
    _scoreLabels.reserve(aPlayersNumber);
    _playerLabels.reserve(aPlayersNumber);

    for (std::size_t aPlayerNo = 0; aPlayerNo < aPlayersNumber; aPlayerNo++)
    {
        _roundLabels.emplace_back(iCtx.sdlCtx(),
                                  iCtx.resources().getSmallFont(),
                                  std::to_string(_state.getPlayersRoundsWon(aPlayerNo)));

        _scoreLabels.emplace_back(iCtx.sdlCtx(),
                                  iCtx.resources().getSmallFont(),
                                  std::to_string(_state.getPlayersCurrentRoundScore(aPlayerNo)));

        _playerLabels.emplace_back(iCtx.sdlCtx(),
                                   iCtx.resources().getSmallFont(),
                                   getPlayerName(iCtx, aPlayerNo));

        Toolkit::GuiSimple::TextLabel& aRoundLabel = _roundLabels.back();
        Toolkit::GuiSimple::TextLabel& aScoreLabel = _scoreLabels.back();
        Toolkit::GuiSimple::TextLabel& aPlayerLabel = _playerLabels.back();

        _allGuiObjects.push_back(&aRoundLabel);
        _allGuiObjects.push_back(&aScoreLabel);
        _allGuiObjects.push_back(&aPlayerLabel);
    }

    const int aLineThickness{_playersHeadingLabel.getSize()._h * 5 / 100};
    _horizontalLine.setThickness(aLineThickness);
    _verticalLine1.setThickness(aLineThickness);
    _verticalLine2.setThickness(aLineThickness);

    _activePlayerLabel.setText(iCtx.sdlCtx(), getPlayerName(iCtx, _state.getCurrentPlayer()), false);

    static_assert(Play::Die::kDiceNumberInGame > 0, "Number of dice is zero, but required in code to be higher");

    GameTob::Gui::Resources& aResources{dynamic_cast<GameTob::Gui::Resources&>(iCtx.resources())};

    // Very important to reserve in advance the necessary memory for the vectors. If they are allowed to grow
    //  dynamically, pointers to the vector content will be invalidated when reallocated. Necessary for the
    //  _allGuiObjects vector of pointers.
    constexpr std::size_t aDiceNumberInGame{Play::Die::kDiceNumberInGame};
    _dieButtons.reserve(aDiceNumberInGame);
    _keepDieCheckboxes.reserve(aDiceNumberInGame);
    _keepDieLabels.reserve(aDiceNumberInGame);

    for (std::size_t aDieNo = 0; aDieNo < aDiceNumberInGame; aDieNo++)
    {
        _dieButtons.push_back(
            Toolkit::GuiSimple::ImageButton{Toolkit::GuiObject::CloneShareTag{},
                                            aResources.getDieButton(_state.getDieValue(aDieNo), true)});

        _keepDieCheckboxes.push_back(Toolkit::GuiSimple::TextCheckbox{Toolkit::GuiObject::CloneShareTag{},
                                                                      aResources.getSmallTextCheckbox(true)});

        _keepDieLabels.push_back(Toolkit::GuiSimple::TextLabel{Toolkit::GuiObject::CloneShareTag{}, _fixedKeepLabel});

        Toolkit::GuiSimple::ImageButton& aDieButton = _dieButtons.back();
        Toolkit::GuiSimple::TextCheckbox& aKeepDieCheckbox = _keepDieCheckboxes.back();
        Toolkit::GuiSimple::TextLabel& aKeepDieLabel = _keepDieLabels.back();

        _allGuiObjects.push_back(&aDieButton);
        _allGuiObjects.push_back(&aKeepDieCheckbox);
        _allGuiObjects.push_back(&aKeepDieLabel);
    }

    _turnScoreLabel.setText(iCtx.sdlCtx(), kTurnScorePrefix + _state.getCurrentPlayersTurnScoreStr(), false);

    // Layout the page; position the individual GUI elements
    layoutPage(iCtx);
}

BoardPage::~BoardPage()
{
}

void BoardPage::handleEvents(const std::vector<SDL_Event>& iEvents, Main::GlobalContext& iCtx)
{
    for (auto aGuiObject : _allGuiObjects)
    {
        aGuiObject->handleEvents(iEvents);
    }

    if (_quitButton.wasClicked())
    {
        if (_quitDoubleClickTimer.isCounting(iCtx))
        {
            _quitDoubleClickTimer.reset();
            setPageState(Toolkit::GuiPageState::kReturn);
        }
        else
        {
            _quitDoubleClickTimer.start(iCtx, std::chrono::milliseconds(1000));   //XYZ HARD CODED
        }
    }
    else
    {
        setPageState(Toolkit::GuiPageState::kActivePage);
    }

    // Link the dice buttons and the kept check boxes
    for (std::size_t aDieNo = 0; aDieNo < Play::Die::kDiceNumberInGame; aDieNo++)
    {
        if (_dieButtons[aDieNo].getState() == Toolkit::GuiObjectState::kActive &&
            _keepDieCheckboxes[aDieNo].getState() == Toolkit::GuiObjectState::kActive &&
            (_dieButtons[aDieNo].wasClicked() || _keepDieCheckboxes[aDieNo].wasClicked()))
        {
            _state.requestKeepDieFlip(aDieNo);
        }
    }

    if (_rollAgainButton.wasClicked())
    {
        _state.requestRollAgain(iCtx.randomNumbers());
    }

    if (_endTurnButton.wasClicked())
    {
        _state.requestEndTurn();
    }
}

void BoardPage::draw(Main::GlobalContext& iCtx)
{
    updatePageState(iCtx);

    _background.draw(iCtx.sdlCtx());
    iCtx.sdlCtx().setDrawColour(0xffa000ff);   // dull yellow-orange

    for (auto* aGuiObject : _allGuiObjects)
    {
        aGuiObject->draw(iCtx.sdlCtx());
    }

    // Highlight the active player in the player list by drawing a rectangle
    SdlDims aDimensions{(_roundsHeadingLabel.getPosition()._x +
                         _roundsHeadingLabel.getSize()._w -
                         _playerLabels[_state.getCurrentPlayer()].getPosition()._x),
                        _playerLabels[_state.getCurrentPlayer()].getSize()._h};

    iCtx.sdlCtx().drawRectangle(_playerLabels[_state.getCurrentPlayer()].getPosition(), aDimensions, false);

    if (!_state.isGameOver())
    {
        // Highlight the first player in the round in the player list by drawing a tiny rectangle
        SdlCoords aCoords = _playerLabels[_state.getFirstPlayerInThisRound()].getPosition();
        aCoords._x -= 12;
        aCoords._y += (_playerLabels[_state.getFirstPlayerInThisRound()].getSize()._h / 2) - 3;
        aDimensions = SdlDims{6, 6};   //XYZ ALL HARD CODED

        iCtx.sdlCtx().drawRectangle(aCoords, aDimensions, false);
    }

    // Represent the double-click action of the quit button by nesting two inner rectangles within the button
    if (_quitDoubleClickTimer.isCounting(iCtx))
    {
        SdlCoords aInnerCoords{_quitButton.getPosition()};
        SdlDims aInnerDims{_quitButton.getSize()};
        aInnerCoords._x += 2;
        aInnerCoords._y += 2;
        aInnerDims._w -= 4;
        aInnerDims._h -= 4;
        iCtx.sdlCtx().drawRectangle(aInnerCoords, aInnerDims, false);
        aInnerCoords._x += 2;
        aInnerCoords._y += 2;
        aInnerDims._w -= 4;
        aInnerDims._h -= 4;
        iCtx.sdlCtx().drawRectangle(aInnerCoords, aInnerDims, false);
    }
}

void BoardPage::cleanUp(Main::GlobalContext& iCtx)
{
    // Release the Game Options, they will be created if needed again
    iCtx.unsetUsedGameOptions();
}

// Layout the page (size and position), which would otherwise be in the constructor
void BoardPage::layoutPage(Main::GlobalContext& iCtx)
{
    // Position the title half way along the x-axis
    _titleLabel.setPosition({((iCtx.sdlCtx().getWindowSize()._w - _titleLabel.getSize()._w) / 2), 0});

    _roundsHeadingLabel.setPosition({iCtx.sdlCtx().getWindowSize()._w * 95 / 100 - _roundsHeadingLabel.getSize()._w,
                                     125});

    _scoresHeadingLabel.setPosition({_roundsHeadingLabel.getPosition()._x -
                                     iCtx.sdlCtx().getWindowSize()._w * 5 / 100 -
                                     _scoresHeadingLabel.getSize()._w,
                                     125});

    _playersHeadingLabel.setPosition({_scoresHeadingLabel.getPosition()._x -
                                     iCtx.sdlCtx().getWindowSize()._w * 5 / 100 -
                                     _playersHeadingLabel.getSize()._w,
                                     125});

    const std::size_t aPlayersNumber{_state.getPlayerNumber()};

    for (std::size_t aPlayerNo = 0; aPlayerNo < aPlayersNumber; aPlayerNo++)
    {
        _roundLabels[aPlayerNo].setPosition({_roundsHeadingLabel.getPosition()._x,
                                             225 + 50 * static_cast<int>(aPlayerNo)});

        _scoreLabels[aPlayerNo].setPosition({_scoresHeadingLabel.getPosition()._x,
                                             225 + 50 * static_cast<int>(aPlayerNo)});

        _playerLabels[aPlayerNo].setPosition({_playersHeadingLabel.getPosition()._x,
                                              225 + 50 * static_cast<int>(aPlayerNo)});
    }

    const int aTitleBottom{_playersHeadingLabel.getPosition()._y + _playersHeadingLabel.getSize()._h};

    _horizontalLine.setPosition(
        {_playersHeadingLabel.getPosition()._x,
         (aTitleBottom + _playerLabels[0].getPosition()._y - _horizontalLine.getThickness()) / 2});

    _horizontalLine.setWidth((_roundsHeadingLabel.getPosition()._x + _roundsHeadingLabel.getSize()._w) -
                             _playersHeadingLabel.getPosition()._x);

    const int aPlayersHeadingRhs{_playersHeadingLabel.getPosition()._x + _playersHeadingLabel.getSize()._w};
    const int aScoresHeadingRhs{_scoresHeadingLabel.getPosition()._x + _scoresHeadingLabel.getSize()._w};

    _verticalLine1.setPosition(
        {(_scoresHeadingLabel.getPosition()._x + aPlayersHeadingRhs - _verticalLine1.getThickness()) / 2,
         _playersHeadingLabel.getPosition()._y});

    _verticalLine2.setPosition(
        {(_roundsHeadingLabel.getPosition()._x + aScoresHeadingRhs - _verticalLine2.getThickness()) / 2,
         _playersHeadingLabel.getPosition()._y});

    const int aLastPlayerBottom{_playerLabels[aPlayersNumber - 1].getPosition()._y +
                                _playerLabels[aPlayersNumber - 1].getSize()._h};

    _verticalLine1.setHeight(aLastPlayerBottom - _playersHeadingLabel.getPosition()._y);
    _verticalLine2.setHeight(_verticalLine1.getHeight());

    _activePlayerLabel.setPosition({50,75});

    for (std::size_t aDieNo = 0; aDieNo < Play::Die::kDiceNumberInGame; aDieNo++)
    {
        _dieButtons[aDieNo].setPosition({50, 150 + 100 * static_cast<int>(aDieNo)});
        _keepDieCheckboxes[aDieNo].setPosition({130, 175 + 100 * static_cast<int>(aDieNo)});
        _keepDieLabels[aDieNo].setPosition({180, 175 + 100 * static_cast<int>(aDieNo)});
    }

    _turnScoreLabel.setPosition({50,750});
    _rollAgainButton.setPosition({250,750});
    _endTurnButton.setPosition({400,750});

    // Position the back button at the bottom right, with some padding
    _quitButton.setPosition({iCtx.sdlCtx().getWindowSize()._w * 99 / 100 - _quitButton.getSize()._w,
                             iCtx.sdlCtx().getWindowSize()._h * 99 / 100 - _quitButton.getSize()._h});
}

// Update the state of the individual GUI objects, based on the game state
void BoardPage::updatePageState(Main::GlobalContext& iCtx)
{
    //XYZ THIS IS ALL QUITE INEFFICIENT !!! SEE STATE CLASS FOR EFFICIENCY IDEAS

    _titleLabel.setText(iCtx.sdlCtx(),
                        (kTitlePrefix + std::to_string(_state.getCurrentRound() + 1) +
                         kTitleInfix + std::to_string(_state.getMaximumRounds())),
                        false);

    const std::size_t aPlayersNumber{_state.getPlayerNumber()};

    for (std::size_t aPlayerNo = 0; aPlayerNo < aPlayersNumber; aPlayerNo++)
    {
        _roundLabels[aPlayerNo].setText(iCtx.sdlCtx(), std::to_string(_state.getPlayersRoundsWon(aPlayerNo)), false);

        _scoreLabels[aPlayerNo].setText(iCtx.sdlCtx(),
                                        std::to_string(_state.getPlayersCurrentRoundScore(aPlayerNo)),
                                        false);

        // player label does not need updating
    }

    _activePlayerLabel.setText(iCtx.sdlCtx(), getPlayerName(iCtx, _state.getCurrentPlayer()), false);

    GameTob::Gui::Resources& aResources{dynamic_cast<GameTob::Gui::Resources&>(iCtx.resources())};

    for (std::size_t aDieNo = 0; aDieNo < Play::Die::kDiceNumberInGame; aDieNo++)
    {
        const bool aDieActive = _state.canKeepOrUnkeepDie(aDieNo);

        _dieButtons[aDieNo].copyAndShare(aResources.getDieButton(_state.getDieValue(aDieNo), aDieActive));

        _keepDieCheckboxes[aDieNo].copyAndShare(aResources.getSmallTextCheckbox(aDieActive));
        _keepDieCheckboxes[aDieNo].setChecked(_state.getDieChoice(aDieNo) != Play::Die::Choice::kDiscard);

        _keepDieLabels[aDieNo].copyAndShare(_state.getDieChoice(aDieNo) == Play::Die::Choice::kLocked ?
                                            _fixedKeptLabel :
                                            _fixedKeepLabel);
    }

    _turnScoreLabel.setText(iCtx.sdlCtx(), kTurnScorePrefix + _state.getCurrentPlayersTurnScoreStr(), false);

    _rollAgainButton.setState(_state.canRollAgain() ?
                              Toolkit::GuiObjectState::kActive :
                              Toolkit::GuiObjectState::kInactive);

    _endTurnButton.setState(_state.canEndTurn() ?
                            Toolkit::GuiObjectState::kActive :
                            Toolkit::GuiObjectState::kInactive);
}

// Retrieve the players name from the game options
const std::string& BoardPage::getPlayerName(Main::GlobalContext& iCtx, std::size_t iPlayerNo) const
{
    // at() method throws std::out_of_range
    const GameTob::GameOptions& aGameOptions{dynamic_cast<GameTob::GameOptions&>(iCtx.usedGameOptions())};
    return aGameOptions.getPlayerName(iPlayerNo);
}

}   // namespace Gui
}   // namespace GameTob

/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "GameTob/Gui/SettingsPage.hpp"

#include "GameTob/Gui/GameOptionsPage.hpp"
#include "GameTob/Gui/GraphicsSettingsPage.hpp"
#include "GameTob/Gui/Resources.hpp"
#include "Main/Constants.hpp"
#include "Main/GlobalContext.hpp"
#include "Sdl/SdlContext.hpp"

#include <SDL2/SDL_events.h>


namespace GameTob
{
namespace Gui
{

SettingsPage::SettingsPage(Main::GlobalContext& iCtx) :
    Toolkit::GuiPage{},
    _titleLabel{iCtx.sdlCtx(), iCtx.resources().getLargeFont(), "Settings and Options"},
    _backButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Back"},
    _saveButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Save"},
    _resetButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Reset to Default"},
    _graphicsButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Graphics"},
    _audioButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Audio"},
    _gameOptionsButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Game Options"},
    _allGuiObjects{&_titleLabel, &_backButton, &_saveButton, &_resetButton,
                   &_graphicsButton, &_audioButton, &_gameOptionsButton}
{
    // Position the title half way along the x-axis
    _titleLabel.setPosition({((iCtx.sdlCtx().getWindowSize()._w - _titleLabel.getSize()._w) / 2), 0});

    // Position the cancel then accept buttons at the bottom right, with some padding
    _backButton.setPosition({iCtx.sdlCtx().getWindowSize()._w * 99 / 100 - _backButton.getSize()._w,
                             iCtx.sdlCtx().getWindowSize()._h * 99 / 100 - _backButton.getSize()._h});

    _saveButton.setPosition({_backButton.getPosition()._x - iCtx.sdlCtx().getWindowSize()._w * 1 / 100 -
                                 _saveButton.getSize()._w,
                             iCtx.sdlCtx().getWindowSize()._h * 99 / 100 - _saveButton.getSize()._h});

    // Position the reset to default button at the bottom left, with some padding
    _resetButton.setPosition({iCtx.sdlCtx().getWindowSize()._w * 1 / 100,
                              iCtx.sdlCtx().getWindowSize()._h * 99 / 100 - _resetButton.getSize()._h});

    // Position other buttons in a row
    _graphicsButton.setPosition({((iCtx.sdlCtx().getWindowSize()._w - _graphicsButton.getSize()._w) / 2), 150});
    _audioButton.setPosition({((iCtx.sdlCtx().getWindowSize()._w - _audioButton.getSize()._w) / 2), 250});
    _gameOptionsButton.setPosition({((iCtx.sdlCtx().getWindowSize()._w - _gameOptionsButton.getSize()._w) / 2), 350});

    _saveButton.setState(iCtx.sdlCtx().getPrefPath().empty() ?
                         Toolkit::GuiObjectState::kInactive :
                         Toolkit::GuiObjectState::kActive);

    // Temporarily disable some buttons
    _audioButton.setState(Toolkit::GuiObjectState::kInactive);
}

SettingsPage::~SettingsPage()
{
}

void SettingsPage::handleEvents(const std::vector<SDL_Event>& iEvents, Main::GlobalContext& iCtx)
{
    for (auto* aGuiObject : _allGuiObjects)
    {
        aGuiObject->handleEvents(iEvents);
    }

    _resetButton.setState(iCtx.settings().isDefault() && iCtx.defaultGameOptions().isDefault() ?
                          Toolkit::GuiObjectState::kInactive :
                          Toolkit::GuiObjectState::kActive);

    if (_backButton.wasClicked())
    {
        setPageState(Toolkit::GuiPageState::kReturn);
    }
    else if (_saveButton.wasClicked())
    {
        iCtx.saveOptions();
        setPageState(Toolkit::GuiPageState::kReturn);
    }
    else if (_resetButton.wasClicked())
    {
        iCtx.settings().reset();
        iCtx.defaultGameOptions().reset();
    }
    else if (_graphicsButton.wasClicked())
    {
        setPageState(Toolkit::GuiPageState::kNextPage);
    }
    else if (_gameOptionsButton.wasClicked())
    {
        setPageState(Toolkit::GuiPageState::kNextPage);
    }
    else
    {
        setPageState(Toolkit::GuiPageState::kActivePage);
    }
}

void SettingsPage::draw(Main::GlobalContext& iCtx)
{
    iCtx.sdlCtx().setDrawColour(Main::kColourSolidRed);

    for (auto* aGuiObject : _allGuiObjects)
    {
        aGuiObject->draw(iCtx.sdlCtx());
    }
}

// The new page when the next page or a page swap is needed
std::unique_ptr<Toolkit::GuiPage> SettingsPage::newPage(Main::GlobalContext& iCtx)
{
    if (getPageState() == Toolkit::GuiPageState::kNextPage || getPageState() == Toolkit::GuiPageState::kSwapPage)
    {
        // Same order as handleEvents()
        if (_graphicsButton.wasClicked() && getPageState() == Toolkit::GuiPageState::kNextPage)
        {
            return std::unique_ptr<Toolkit::GuiPage>{new GraphicsSettingsPage{iCtx}};
        }
        else if (_gameOptionsButton.wasClicked() && getPageState() == Toolkit::GuiPageState::kNextPage)
        {
            return std::unique_ptr<Toolkit::GuiPage>{new GameOptionsPage{iCtx, false}};
        }
    }

    return nullptr;
}

}   // namespace Gui
}   // namespace GameTob

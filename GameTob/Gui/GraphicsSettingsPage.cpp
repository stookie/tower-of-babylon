/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "GameTob/Gui/GraphicsSettingsPage.hpp"

#include "GameTob/Gui/Resources.hpp"
#include "Main/Constants.hpp"
#include "Main/GlobalContext.hpp"
#include "Sdl/SdlContext.hpp"

#include <SDL2/SDL_events.h>


namespace GameTob
{
namespace Gui
{

GraphicsSettingsPage::GraphicsSettingsPage(Main::GlobalContext& iCtx) :
    Toolkit::GuiPage{},
    _titleLabel{iCtx.sdlCtx(), iCtx.resources().getLargeFont(), "Graphics Settings"},
    _cancelButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Cancel"},
    _acceptButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Accept"},
    _resetButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Reset to Default"},
    _vsyncCheckbox{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), false},
    _vsyncLabel{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "VSync enabled"},
    _allGuiObjects{&_titleLabel, &_cancelButton, &_acceptButton, &_resetButton,
                   &_vsyncCheckbox, &_vsyncLabel},
    _localSettings{iCtx.settings()}
{
    // Position the title half way along the x-axis
    _titleLabel.setPosition({((iCtx.sdlCtx().getWindowSize()._w - _titleLabel.getSize()._w) / 2), 0});

    // Position the cancel then accept buttons at the bottom right, with some padding
    _cancelButton.setPosition({iCtx.sdlCtx().getWindowSize()._w * 99 / 100 - _cancelButton.getSize()._w,
                               iCtx.sdlCtx().getWindowSize()._h * 99 / 100 - _cancelButton.getSize()._h});

    _acceptButton.setPosition({iCtx.sdlCtx().getWindowSize()._w * 97 / 100 -
                                   _acceptButton.getSize()._w - _cancelButton.getSize()._w,
                               iCtx.sdlCtx().getWindowSize()._h * 99 / 100 - _acceptButton.getSize()._h});

    // Position the reset to default button at the bottom left, with some padding
    _resetButton.setPosition({iCtx.sdlCtx().getWindowSize()._w * 1 / 100,
                              iCtx.sdlCtx().getWindowSize()._h * 99 / 100 - _resetButton.getSize()._h});

    _vsyncCheckbox.setPosition({50, 150});
    _vsyncLabel.setPosition({125, 150});

    setControlsState(iCtx.settings());
}

GraphicsSettingsPage::~GraphicsSettingsPage()
{
}

void GraphicsSettingsPage::handleEvents(const std::vector<SDL_Event>& iEvents, Main::GlobalContext& iCtx)
{
    for (auto* aGuiObject : _allGuiObjects)
    {
        aGuiObject->handleEvents(iEvents);
    }

    bool aSomethingChanged{false};

    // Handle changes to local state before potentially copying it with Accept
    if (_vsyncCheckbox.wasClicked())
    {
        _localSettings.setVsync(_vsyncCheckbox.getChecked());
        aSomethingChanged = true;
    }

    if (_cancelButton.wasClicked())
    {
        setPageState(Toolkit::GuiPageState::kReturn);
    }
    else if (_acceptButton.wasClicked())
    {
        iCtx.settings().copyGraphics(_localSettings);
        setPageState(Toolkit::GuiPageState::kReturn);
    }
    else if (_resetButton.wasClicked())
    {
        _localSettings.resetGraphics();
        aSomethingChanged = true;
        setPageState(Toolkit::GuiPageState::kActivePage);
    }
    else
    {
        setPageState(Toolkit::GuiPageState::kActivePage);
    }

    if (aSomethingChanged)
    {
        setControlsState(iCtx.settings());
    }
}

void GraphicsSettingsPage::draw(Main::GlobalContext& iCtx)
{
    iCtx.sdlCtx().setDrawColour(Main::kColourSolidRed);

    for (auto* aGuiObject : _allGuiObjects)
    {
        aGuiObject->draw(iCtx.sdlCtx());
    }
}

// Set the state of the GUI controls
void GraphicsSettingsPage::setControlsState(const Options::Settings& iGlobalSettings)
{
    _vsyncCheckbox.setChecked(_localSettings.getVsync());

    _acceptButton.setState(_localSettings.equalGraphics(iGlobalSettings) ?
                           Toolkit::GuiObjectState::kInactive :
                           Toolkit::GuiObjectState::kActive);

    _resetButton.setState(_localSettings.isDefaultGraphics() ?
                          Toolkit::GuiObjectState::kInactive :
                          Toolkit::GuiObjectState::kActive);
}

}   // namespace Gui
}   // namespace GameTob

/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_GAME_TOB_RESOURCES
#define TOB_GAME_TOB_RESOURCES

#include "GameTob/Play/State.hpp"
#include "Toolkit/GuiResources.hpp"
#include "Toolkit/GuiSimple/ImageButton.hpp"
#include "Toolkit/GuiSimple/TextCheckbox.hpp"

#include <map>
#include <memory>

class SdlFont;
class SdlTexture;

namespace Main
{
class GlobalContext;
}

namespace Options
{
class GameOptions;
}

namespace Toolkit
{
class GuiPage;

namespace GuiSimple
{
class TextButton;
}
}


namespace GameTob
{
namespace Gui
{

class Resources : virtual public Toolkit::GuiResources
{
public:
    Resources(Main::GlobalContext& iGlobalCtx);   // throws Main::RuntimeException if dice image dimensions are wrong
    virtual ~Resources() override;

    Resources(const Resources&) = delete;
    Resources& operator=(const Resources&) = delete;
    Resources(Resources&&) = delete;
    Resources& operator=(Resources&&) = delete;

    // Create an instance of the game options object
    virtual std::unique_ptr<Options::GameOptions> createGameOptions() const override;

    // Create the first page
    virtual std::unique_ptr<Toolkit::GuiPage> createFirstPage(Main::GlobalContext& iCtx) const override;

    // Access fonts
    virtual std::shared_ptr<SdlFont> getLargeFont() const override;
    virtual std::shared_ptr<SdlFont> getNormalFont() const override;
    virtual std::shared_ptr<SdlFont> getSmallFont() const override;

    virtual std::shared_ptr<SdlTexture> getFeltTexture() const;

    //XYZ ADD READ-ONLY SHARING TO TextXyz TO PREVENT SHARED RESOURCE UPDATES

    // Access a small-font text checkbox, active or inactive
    const Toolkit::GuiSimple::TextCheckbox& getSmallTextCheckbox(bool iActiveCheckbox) const;

    // Access a button for the specified die face, active or inactive; throws std::out_of_range if out of range
    const Toolkit::GuiSimple::ImageButton& getDieButton(Play::Die::Value iValue, bool iActiveButton) const;

private:
    std::shared_ptr<SdlFont> _largeFont;    // Font used for titles, etc; the largest size
    std::shared_ptr<SdlFont> _normalFont;   // Font used for buttons etc; the middle size
    std::shared_ptr<SdlFont> _smallFont;    // Font used for text etc; the smallest size

    std::shared_ptr<SdlTexture> _feltTexture;   // Image of felt to be used in backgrounds

    Toolkit::GuiSimple::TextCheckbox _activeSmallTextCheckbox;
    Toolkit::GuiSimple::TextCheckbox _inactiveSmallTextCheckbox;

    std::map<Play::Die::Value, Toolkit::GuiSimple::ImageButton> _activeDieButtons;   // Buttons used for active dice
    std::map<Play::Die::Value, Toolkit::GuiSimple::ImageButton> _inactiveDieButtons;   // Buttons used for inactive dice
};

}   // namespace Gui
}   // namespace GameTob

#endif   // TOB_GAME_TOB_RESOURCES

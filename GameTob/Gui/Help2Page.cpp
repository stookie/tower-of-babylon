/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "GameTob/Gui/Help2Page.hpp"

#include "GameTob/Gui/Help1Page.hpp"
#include "GameTob/Gui/Help3Page.hpp"
#include "GameTob/Gui/Resources.hpp"
#include "GameTob/Play/State.hpp"
#include "Main/GlobalContext.hpp"
#include "Toolkit/GuiObject.hpp"

#include <SDL2/SDL_events.h>


namespace
{
const std::string kSingleDiceText{"These two die values score points by themselves"};
const std::string kSingle50Text{" scores 50 points"};
const std::string kSingle100Text{" scores 100 points"};
const std::string kOneOfEachLabelText{"One of each die value scores 1000 points"};
const std::string kSixOfAKindLabelText{"Six of the same die value scores 5000 points"};
}


namespace GameTob
{
namespace Gui
{

using GameTob::Play::Die;

Help2Page::Help2Page(Main::GlobalContext& iCtx) :
    Toolkit::GuiPage{},
    _titleLabel{iCtx.sdlCtx(), iCtx.resources().getLargeFont(), "Help: Scoring (1 of 2)"},
    _backButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Back"},
    _prevButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Previous"},
    _nextButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Next"},
    _singleDiceLabel{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), kSingleDiceText},
    _single50Label{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), kSingle50Text},
    _single100Label{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), kSingle100Text},
    _oneOfEachLabel{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), kOneOfEachLabelText},
    _sixOfAKindLabel{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), kSixOfAKindLabelText},

    _oneOfEachButton1{Toolkit::GuiObject::CloneShareTag{},
                      dynamic_cast<GameTob::Gui::Resources&>(iCtx.resources()).getDieButton(Die::Value::k1, true)},
    _oneOfEachButton2{Toolkit::GuiObject::CloneShareTag{},
                      dynamic_cast<GameTob::Gui::Resources&>(iCtx.resources()).getDieButton(Die::Value::k2, true)},
    _oneOfEachButton3{Toolkit::GuiObject::CloneShareTag{},
                      dynamic_cast<GameTob::Gui::Resources&>(iCtx.resources()).getDieButton(Die::Value::k3, true)},
    _oneOfEachButton4{Toolkit::GuiObject::CloneShareTag{},
                      dynamic_cast<GameTob::Gui::Resources&>(iCtx.resources()).getDieButton(Die::Value::k4, true)},
    _oneOfEachButton5{Toolkit::GuiObject::CloneShareTag{},
                      dynamic_cast<GameTob::Gui::Resources&>(iCtx.resources()).getDieButton(Die::Value::k5, true)},
    _oneOfEachButton6{Toolkit::GuiObject::CloneShareTag{},
                      dynamic_cast<GameTob::Gui::Resources&>(iCtx.resources()).getDieButton(Die::Value::k6, true)},

    _sixOfAKindButton1{Toolkit::GuiObject::CloneShareTag{}, _oneOfEachButton2},
    _sixOfAKindButton2{Toolkit::GuiObject::CloneShareTag{}, _oneOfEachButton2},
    _sixOfAKindButton3{Toolkit::GuiObject::CloneShareTag{}, _oneOfEachButton2},
    _sixOfAKindButton4{Toolkit::GuiObject::CloneShareTag{}, _oneOfEachButton2},
    _sixOfAKindButton5{Toolkit::GuiObject::CloneShareTag{}, _oneOfEachButton2},
    _sixOfAKindButton6{Toolkit::GuiObject::CloneShareTag{}, _oneOfEachButton2},
    _single50Button{Toolkit::GuiObject::CloneShareTag{}, _oneOfEachButton5},
    _single100Button{Toolkit::GuiObject::CloneShareTag{}, _oneOfEachButton1},

    _allGuiObjects{&_titleLabel, &_backButton, &_prevButton, &_nextButton,
                   &_singleDiceLabel, &_single50Label, &_single100Label, &_oneOfEachLabel, &_sixOfAKindLabel,
                   &_single50Button, &_single100Button,
                   &_oneOfEachButton1, &_oneOfEachButton2, &_oneOfEachButton3,
                   &_oneOfEachButton4, &_oneOfEachButton5, &_oneOfEachButton6,
                   &_sixOfAKindButton1, &_sixOfAKindButton2, &_sixOfAKindButton3,
                   &_sixOfAKindButton4, &_sixOfAKindButton5, &_sixOfAKindButton6}
{
    // Position the title half way along the x-axis
    _titleLabel.setPosition({((iCtx.sdlCtx().getWindowSize()._w - _titleLabel.getSize()._w) / 2), 0});

    _singleDiceLabel.setPosition({50, 125});
    _single50Button.setPosition({50, 200});
    _single50Label.setPosition({125, 200});
    _single100Button.setPosition({400, 200});
    _single100Label.setPosition({475, 200});

    _oneOfEachLabel.setPosition({50, 325});
    _oneOfEachButton2.setPosition({100, 400});
    _oneOfEachButton3.setPosition({200, 400});
    _oneOfEachButton4.setPosition({300, 400});
    _oneOfEachButton5.setPosition({400, 400});
    _oneOfEachButton6.setPosition({500, 400});
    _oneOfEachButton1.setPosition({600, 400});

    _sixOfAKindLabel.setPosition({50, 525});
    _sixOfAKindButton1.setPosition({100, 600});
    _sixOfAKindButton2.setPosition({200, 600});
    _sixOfAKindButton3.setPosition({300, 600});
    _sixOfAKindButton4.setPosition({400, 600});
    _sixOfAKindButton5.setPosition({500, 600});
    _sixOfAKindButton6.setPosition({600, 600});

    // Position the back then next then previous buttons at the bottom right, with some padding
    _backButton.setPosition({iCtx.sdlCtx().getWindowSize()._w * 99 / 100 - _backButton.getSize()._w,
                             iCtx.sdlCtx().getWindowSize()._h * 99 / 100 - _backButton.getSize()._h});

    _nextButton.setPosition({_backButton.getPosition()._x - iCtx.sdlCtx().getWindowSize()._w * 1 / 100 -
                                 _nextButton.getSize()._w,
                             iCtx.sdlCtx().getWindowSize()._h * 99 / 100 - _nextButton.getSize()._h});

    _prevButton.setPosition({_nextButton.getPosition()._x - iCtx.sdlCtx().getWindowSize()._w * 1 / 100 -
                                 _prevButton.getSize()._w,
                             iCtx.sdlCtx().getWindowSize()._h * 99 / 100 - _prevButton.getSize()._h});
}

Help2Page::~Help2Page()
{
}

void Help2Page::handleEvents(const std::vector<SDL_Event>& iEvents, Main::GlobalContext& iCtx)
{
    for (auto* aGuiObject : _allGuiObjects)
    {
        aGuiObject->handleEvents(iEvents);
    }

    if (_backButton.wasClicked())
    {
        setPageState(Toolkit::GuiPageState::kReturn);
    }
    else if (_prevButton.wasClicked())
    {
        setPageState(Toolkit::GuiPageState::kSwapPage);
    }
    else if (_nextButton.wasClicked())
    {
        setPageState(Toolkit::GuiPageState::kSwapPage);
    }
    else
    {
        setPageState(Toolkit::GuiPageState::kActivePage);
    }
}

void Help2Page::draw(Main::GlobalContext& iCtx)
{
    iCtx.sdlCtx().setDrawColour(Main::kColourSolidRed);

    for (auto* aGuiObject : _allGuiObjects)
    {
        aGuiObject->draw(iCtx.sdlCtx());
    }
}

std::unique_ptr<Toolkit::GuiPage> Help2Page::newPage(Main::GlobalContext& iCtx)
{
    if (getPageState() == Toolkit::GuiPageState::kNextPage || getPageState() == Toolkit::GuiPageState::kSwapPage)
    {
        // Same order as handleEvents()
        if (_prevButton.wasClicked() && getPageState() == Toolkit::GuiPageState::kSwapPage)
        {
            return std::unique_ptr<Toolkit::GuiPage>{new Help1Page{iCtx}};
        }
        else if (_nextButton.wasClicked() && getPageState() == Toolkit::GuiPageState::kSwapPage)
        {
            return std::unique_ptr<Toolkit::GuiPage>{new Help3Page{iCtx}};
        }
    }

    return nullptr;
}

}   // namespace Gui
}   // namespace GameTob

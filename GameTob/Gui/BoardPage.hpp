/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_GAME_TOB_GUI_BOARDPAGE
#define TOB_GAME_TOB_GUI_BOARDPAGE

#include "GameTob/Play/State.hpp"
#include "Toolkit/GuiPage.hpp"
#include "Toolkit/GuiSimple/BackgroundImage.hpp"
#include "Toolkit/GuiSimple/HorizontalLine.hpp"
#include "Toolkit/GuiSimple/ImageButton.hpp"
#include "Toolkit/GuiSimple/TextButton.hpp"
#include "Toolkit/GuiSimple/TextCheckbox.hpp"
#include "Toolkit/GuiSimple/TextLabel.hpp"
#include "Toolkit/GuiSimple/VerticalLine.hpp"
#include "Utils/Timers.hpp"

#include <vector>

namespace Main
{
class GlobalContext;
}


namespace GameTob
{
namespace Gui
{

class BoardPage : virtual public Toolkit::GuiPage
{
public:
    BoardPage(Main::GlobalContext& iCtx);   // throws Main::LogicException if game state player number is invalid
    virtual ~BoardPage() override;

    virtual void handleEvents(const std::vector<SDL_Event>& iEvents, Main::GlobalContext& iCtx) override;

    virtual void draw(Main::GlobalContext& iCtx) override;

    virtual void cleanUp(Main::GlobalContext& iCtx) override;

protected:
    // Layout the page (size and position), which would otherwise be in the constructor
    virtual void layoutPage(Main::GlobalContext& iCtx);

    // Update the state of the individual GUI objects, based on the game state
    virtual void updatePageState(Main::GlobalContext& iCtx);

    // Retrieve the players name from the game options; throws std::out_of_range if out of range
    virtual const std::string& getPlayerName(Main::GlobalContext& iCtx, std::size_t iPlayerNo) const;

private:
    Toolkit::GuiSimple::TextLabel _titleLabel;
    Toolkit::GuiSimple::TextLabel _playersHeadingLabel;
    Toolkit::GuiSimple::TextLabel _scoresHeadingLabel;
    Toolkit::GuiSimple::TextLabel _roundsHeadingLabel;
    std::vector<Toolkit::GuiSimple::TextLabel> _playerLabels;
    std::vector<Toolkit::GuiSimple::TextLabel> _scoreLabels;
    std::vector<Toolkit::GuiSimple::TextLabel> _roundLabels;
    Toolkit::GuiSimple::HorizontalLine _horizontalLine;
    Toolkit::GuiSimple::VerticalLine _verticalLine1;
    Toolkit::GuiSimple::VerticalLine _verticalLine2;
    Toolkit::GuiSimple::TextLabel _activePlayerLabel;
    std::vector<Toolkit::GuiSimple::ImageButton> _dieButtons;
    std::vector<Toolkit::GuiSimple::TextCheckbox> _keepDieCheckboxes;
    std::vector<Toolkit::GuiSimple::TextLabel> _keepDieLabels;
    Toolkit::GuiSimple::TextLabel _turnScoreLabel;
    Toolkit::GuiSimple::TextButton _rollAgainButton;
    Toolkit::GuiSimple::TextButton _endTurnButton;
    Toolkit::GuiSimple::TextButton _quitButton;
    Toolkit::GuiSimple::TextLabel _fixedKeepLabel;   // reference label for cloning, not displayed directly on the GUI
    Toolkit::GuiSimple::TextLabel _fixedKeptLabel;   // reference label for cloning, not displayed directly on the GUI

    // Collection of all GUI objects to allow easy iteration
    std::vector<Toolkit::GuiObject*> _allGuiObjects;

    // The game state and logic
    GameTob::Play::State _state;

    // Timer to enforce the quit button double click
    Utils::SimpleTimer _quitDoubleClickTimer;

    // Background image
    Toolkit::GuiSimple::BackgroundImage _background;
};

}   // namespace Gui
}   // namespace GameTob

#endif   // TOB_GAME_TOB_GUI_BOARDPAGE

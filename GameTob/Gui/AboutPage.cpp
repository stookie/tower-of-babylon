/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "GameTob/Gui/AboutPage.hpp"

#include "GameTob/Gui/Resources.hpp"
#include "Main/Constants.hpp"
#include "Main/GlobalContext.hpp"
#include "Sdl/SdlContext.hpp"

#include <SDL2/SDL_events.h>


namespace
{
const std::string kProjectLink{"https://gitlab.com/stookie/tower-of-babylon"};
const std::string kCopyrightLink{"https://gitlab.com/stookie/tower-of-babylon/-/blob/master/COPYING.md"};
const std::string kSourceLicenseLink{"https://gitlab.com/stookie/tower-of-babylon/-/blob/master/LICENSE"};

const Main::GlobalContext::SteadyDuration::rep kClipboardNotificationTime_ms{2000};
}


namespace GameTob
{
namespace Gui
{

AboutPage::AboutPage(Main::GlobalContext& iCtx) :
    Toolkit::GuiPage{},
    _titleLabel{iCtx.sdlCtx(), iCtx.resources().getLargeFont(), "Tower Of Babylon"},
    _backButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Back"},
    _versionLabel{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Version: None (in development)"},
    _authorsLabel{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Authors:"},
    _author1Label{iCtx.sdlCtx(), iCtx.resources().getSmallFont(), "temp"},
    _copyrightLabel{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Copyright (C) 2021-2024 authors"},
    _licenseLabel{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Released under GNU AGPL v3 only"},
    _projectButton{iCtx.sdlCtx(), iCtx.resources().getSmallFont(), "temp"},
    _copyrightButton{iCtx.sdlCtx(), iCtx.resources().getSmallFont(), "temp"},
    _sourceCodeLicenseButton{iCtx.sdlCtx(), iCtx.resources().getSmallFont() , "temp"},
    _clipboardLabel{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "(Copied to clipboard)"},
    _allGuiObjects{&_titleLabel, &_backButton,
                   &_versionLabel, &_authorsLabel, &_author1Label, &_copyrightLabel, &_licenseLabel,
                   &_projectButton, &_copyrightButton, &_sourceCodeLicenseButton, &_clipboardLabel},
    _clipboardTimer{}
{
    _author1Label.setText(iCtx.sdlCtx(), "  stookie ( 4303586-stookie@users.noreply.gitlab.com )", false);
    _projectButton.setText(iCtx.sdlCtx(), " Project page link ", false);
    _copyrightButton.setText(iCtx.sdlCtx(), " Copyright and license information link ", false);
    _sourceCodeLicenseButton.setText(iCtx.sdlCtx(), " Source code license link ", false);

    // Position the title half way along the x-axis
    _titleLabel.setPosition({((iCtx.sdlCtx().getWindowSize()._w - _titleLabel.getSize()._w) / 2), 0});

    // Position the back button at the bottom right, with some padding
    _backButton.setPosition({iCtx.sdlCtx().getWindowSize()._w * 99 / 100 - _backButton.getSize()._w,
                             iCtx.sdlCtx().getWindowSize()._h * 99 / 100 - _backButton.getSize()._h});

    _versionLabel.setPosition({50, 100});
    _authorsLabel.setPosition({50, 175});
    _author1Label.setPosition({50, 225});
    _copyrightLabel.setPosition({50, 300});
    _licenseLabel.setPosition({50, 375});
    _projectButton.setPosition({50, 500});
    _copyrightButton.setPosition({50, 550});
    _sourceCodeLicenseButton.setPosition({50, 600});

    _clipboardLabel.setPosition({((iCtx.sdlCtx().getWindowSize()._w - _clipboardLabel.getSize()._w) / 2),
                                 iCtx.sdlCtx().getWindowSize()._h * 99 / 100 - _clipboardLabel.getSize()._h});

    _clipboardLabel.setState(Toolkit::GuiObjectState::kHidden);
}

AboutPage::~AboutPage()
{
}

void AboutPage::handleEvents(const std::vector<SDL_Event>& iEvents, Main::GlobalContext& iCtx)
{
    for (auto* aGuiObject : _allGuiObjects)
    {
        aGuiObject->handleEvents(iEvents);
    }

    if (_backButton.wasClicked())
    {
        setPageState(Toolkit::GuiPageState::kReturn);
    }
    else
    {
        setPageState(Toolkit::GuiPageState::kActivePage);
    }

    if (_projectButton.wasClicked())
    {
        iCtx.sdlCtx().setClipboardText(kProjectLink);
        _clipboardTimer.start(iCtx, std::chrono::milliseconds{kClipboardNotificationTime_ms});
    }
    else if (_copyrightButton.wasClicked())
    {
        iCtx.sdlCtx().setClipboardText(kCopyrightLink);
        _clipboardTimer.start(iCtx, std::chrono::milliseconds{kClipboardNotificationTime_ms});
    }
    else if (_sourceCodeLicenseButton.wasClicked())
    {
        iCtx.sdlCtx().setClipboardText(kSourceLicenseLink);
        _clipboardTimer.start(iCtx, std::chrono::milliseconds{kClipboardNotificationTime_ms});
    }

    if (_clipboardTimer.isCounting(iCtx))
    {
        _clipboardLabel.setState(Toolkit::GuiObjectState::kActive);
    }
    else if (_clipboardTimer.isExpired(iCtx))
    {
        _clipboardTimer.reset();
        _clipboardLabel.setState(Toolkit::GuiObjectState::kHidden);
    }
}

void AboutPage::draw(Main::GlobalContext& iCtx)
{
    iCtx.sdlCtx().setDrawColour(Main::kColourSolidRed);

    for (auto* aGuiObject : _allGuiObjects)
    {
        aGuiObject->draw(iCtx.sdlCtx());
    }
}

}   // namespace Gui
}   // namespace GameTob

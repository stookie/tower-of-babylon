/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_GAME_TOB_GUI_GRAPHICSSETTINGSPAGE
#define TOB_GAME_TOB_GUI_GRAPHICSSETTINGSPAGE

#include "Options/Settings.hpp"
#include "Toolkit/GuiPage.hpp"
#include "Toolkit/GuiSimple/TextButton.hpp"
#include "Toolkit/GuiSimple/TextCheckbox.hpp"
#include "Toolkit/GuiSimple/TextLabel.hpp"

#include <vector>

namespace Main
{
class GlobalContext;
}


namespace GameTob
{
namespace Gui
{

class GraphicsSettingsPage : virtual public Toolkit::GuiPage
{
public:
    GraphicsSettingsPage(Main::GlobalContext& iCtx);
    virtual ~GraphicsSettingsPage() override;

    virtual void handleEvents(const std::vector<SDL_Event>& iEvents, Main::GlobalContext& iCtx) override;

    virtual void draw(Main::GlobalContext& iCtx) override;

    // The new page when the next page or a page swap is needed
    // virtual std::unique_ptr<Toolkit::GuiPage> newPage(Main::GlobalContext& iCtx) override;

private:
    // Set the state of the GUI controls
    void setControlsState(const Options::Settings& iGlobalSettings);

    Toolkit::GuiSimple::TextLabel _titleLabel;
    Toolkit::GuiSimple::TextButton _cancelButton;
    Toolkit::GuiSimple::TextButton _acceptButton;
    Toolkit::GuiSimple::TextButton _resetButton;
    Toolkit::GuiSimple::TextCheckbox _vsyncCheckbox;
    Toolkit::GuiSimple::TextLabel _vsyncLabel;

    // Collection of all GUI objects to allow easy iteration
    std::vector<Toolkit::GuiObject*> _allGuiObjects;

    Options::Settings _localSettings;
};

}   // namespace Gui
}   // namespace GameTob

#endif   // TOB_GAME_TOB_GUI_GRAPHICSSETTINGSPAGE

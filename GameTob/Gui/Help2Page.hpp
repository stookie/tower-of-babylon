/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_GAME_TOB_GUI_HELP2PAGE
#define TOB_GAME_TOB_GUI_HELP2PAGE

#include "Toolkit/GuiPage.hpp"
#include "Toolkit/GuiSimple/ImageButton.hpp"
#include "Toolkit/GuiSimple/TextButton.hpp"
#include "Toolkit/GuiSimple/TextLabel.hpp"

#include <vector>

namespace Main
{
class GlobalContext;
}

namespace Toolkit
{
class GuiObject;
}


namespace GameTob
{
namespace Gui
{

class Help2Page : virtual public Toolkit::GuiPage
{
public:
    Help2Page(Main::GlobalContext& iCtx);
    virtual ~Help2Page() override;

    virtual void handleEvents(const std::vector<SDL_Event>& iEvents, Main::GlobalContext& iCtx) override;

    virtual void draw(Main::GlobalContext& iCtx) override;

    // The new page when the next page or a page swap is needed
    virtual std::unique_ptr<Toolkit::GuiPage> newPage(Main::GlobalContext& iCtx) override;

private:
    Toolkit::GuiSimple::TextLabel _titleLabel;
    Toolkit::GuiSimple::TextButton _backButton;
    Toolkit::GuiSimple::TextButton _prevButton;
    Toolkit::GuiSimple::TextButton _nextButton;
    Toolkit::GuiSimple::TextLabel _singleDiceLabel;
    Toolkit::GuiSimple::TextLabel _single50Label;
    Toolkit::GuiSimple::TextLabel _single100Label;
    Toolkit::GuiSimple::TextLabel _oneOfEachLabel;
    Toolkit::GuiSimple::TextLabel _sixOfAKindLabel;
    Toolkit::GuiSimple::ImageButton _oneOfEachButton1;
    Toolkit::GuiSimple::ImageButton _oneOfEachButton2;
    Toolkit::GuiSimple::ImageButton _oneOfEachButton3;
    Toolkit::GuiSimple::ImageButton _oneOfEachButton4;
    Toolkit::GuiSimple::ImageButton _oneOfEachButton5;
    Toolkit::GuiSimple::ImageButton _oneOfEachButton6;
    Toolkit::GuiSimple::ImageButton _sixOfAKindButton1;
    Toolkit::GuiSimple::ImageButton _sixOfAKindButton2;
    Toolkit::GuiSimple::ImageButton _sixOfAKindButton3;
    Toolkit::GuiSimple::ImageButton _sixOfAKindButton4;
    Toolkit::GuiSimple::ImageButton _sixOfAKindButton5;
    Toolkit::GuiSimple::ImageButton _sixOfAKindButton6;
    Toolkit::GuiSimple::ImageButton _single50Button;
    Toolkit::GuiSimple::ImageButton _single100Button;

    // Collection of all GUI objects to allow easy iteration
    std::vector<Toolkit::GuiObject*> _allGuiObjects;
};

}   // namespace Gui
}   // namespace GameTob

#endif   // TOB_GAME_TOB_GUI_HELP2PAGE

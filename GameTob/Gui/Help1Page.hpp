/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_GAME_TOB_GUI_HELP1PAGE
#define TOB_GAME_TOB_GUI_HELP1PAGE

#include "Toolkit/GuiPage.hpp"
#include "Toolkit/GuiSimple/TextButton.hpp"
#include "Toolkit/GuiSimple/TextLabel.hpp"

#include <vector>

namespace Main
{
class GlobalContext;
}

namespace Toolkit
{
class GuiObject;
}


namespace GameTob
{
namespace Gui
{

class Help1Page : virtual public Toolkit::GuiPage
{
public:
    Help1Page(Main::GlobalContext& iCtx);
    virtual ~Help1Page() override;

    virtual void handleEvents(const std::vector<SDL_Event>& iEvents, Main::GlobalContext& iCtx) override;

    virtual void draw(Main::GlobalContext& iCtx) override;

    // The new page when the next page or a page swap is needed
    virtual std::unique_ptr<Toolkit::GuiPage> newPage(Main::GlobalContext& iCtx) override;

private:
    Toolkit::GuiSimple::TextLabel _titleLabel;
    Toolkit::GuiSimple::TextButton _backButton;
    Toolkit::GuiSimple::TextButton _prevButton;
    Toolkit::GuiSimple::TextButton _nextButton;
    Toolkit::GuiSimple::TextLabel _help1Label;
    Toolkit::GuiSimple::TextLabel _help2aLabel;
    Toolkit::GuiSimple::TextLabel _help2bLabel;
    Toolkit::GuiSimple::TextLabel _help3Label;
    Toolkit::GuiSimple::TextLabel _help4aLabel;
    Toolkit::GuiSimple::TextLabel _help4bLabel;
    Toolkit::GuiSimple::TextLabel _help5Label;
    Toolkit::GuiSimple::TextLabel _help6aLabel;
    Toolkit::GuiSimple::TextLabel _help6bLabel;
    Toolkit::GuiSimple::TextLabel _help6cLabel;

    // Collection of all GUI objects to allow easy iteration
    std::vector<Toolkit::GuiObject*> _allGuiObjects;
};

}   // namespace Gui
}   // namespace GameTob

#endif   // TOB_GAME_TOB_GUI_HELP1PAGE

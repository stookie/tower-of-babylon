/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "GameTob/Gui/Resources.hpp"

#include "GameTob/GameOptions.hpp"
#include "GameTob/Gui/MainPage.hpp"
#include "Main/Exceptions.hpp"
#include "Main/GlobalContext.hpp"
#include "Main/Log.hpp"
#include "Options/Settings.hpp"
#include "Sdl/SdlContext.hpp"
#include "Sdl/SdlFont.hpp"
#include "Sdl/SdlTexture.hpp"
#include "Toolkit/GuiPage.hpp"


namespace GameTob
{
namespace Gui
{

Resources::Resources(Main::GlobalContext& iGlobalCtx) :
    _largeFont{iGlobalCtx.sdlCtx().loadFont(kFontFile, kDefaultLargeFontSize)},
    _normalFont{iGlobalCtx.sdlCtx().loadFont(kFontFile, kDefaultNormalFontSize)},
    _smallFont{iGlobalCtx.sdlCtx().loadFont(kFontFile, kDefaultSmallFontSize)},
    _feltTexture{iGlobalCtx.sdlCtx().loadImage(kFeltBackground)},
    _activeSmallTextCheckbox{iGlobalCtx.sdlCtx(), _smallFont, false},
    _inactiveSmallTextCheckbox{iGlobalCtx.sdlCtx(), _smallFont, false},
    _activeDieButtons{},
    _inactiveDieButtons{}
{
    Main::Log::dbg("Resources()");

    SdlContext& aSdlCtx = iGlobalCtx.sdlCtx();

    _inactiveSmallTextCheckbox.setState(Toolkit::GuiObjectState::kInactive);

    _activeDieButtons.emplace(Play::Die::Value::kBlank,
                              Toolkit::GuiSimple::ImageButton{aSdlCtx,
                                                              aSdlCtx.loadImage(GameTob::kDieImageBlank_70)});
    _activeDieButtons.emplace(Play::Die::Value::k1,
                              Toolkit::GuiSimple::ImageButton{aSdlCtx,
                                                              aSdlCtx.loadImage(GameTob::kDieImage10_70)});
    _activeDieButtons.emplace(Play::Die::Value::k2,
                              Toolkit::GuiSimple::ImageButton{aSdlCtx,
                                                              aSdlCtx.loadImage(GameTob::kDieImage2_70)});
    _activeDieButtons.emplace(Play::Die::Value::k3,
                              Toolkit::GuiSimple::ImageButton{aSdlCtx,
                                                              aSdlCtx.loadImage(GameTob::kDieImage3_70)});
    _activeDieButtons.emplace(Play::Die::Value::k4,
                              Toolkit::GuiSimple::ImageButton{aSdlCtx,
                                                              aSdlCtx.loadImage(GameTob::kDieImage4_70)});
    _activeDieButtons.emplace(Play::Die::Value::k5,
                              Toolkit::GuiSimple::ImageButton{aSdlCtx,
                                                              aSdlCtx.loadImage(GameTob::kDieImage5_70)});
    _activeDieButtons.emplace(Play::Die::Value::k6,
                              Toolkit::GuiSimple::ImageButton{aSdlCtx,
                                                              aSdlCtx.loadImage(GameTob::kDieImage6_70)});

    _inactiveDieButtons.emplace(Play::Die::Value::kBlank,
                                Toolkit::GuiSimple::ImageButton{aSdlCtx,
                                                                aSdlCtx.loadImage(GameTob::kDieImageBlank_70)});
    _inactiveDieButtons.emplace(Play::Die::Value::k1,
                                Toolkit::GuiSimple::ImageButton{aSdlCtx,
                                                                aSdlCtx.loadImage(GameTob::kDieImage10_70)});
    _inactiveDieButtons.emplace(Play::Die::Value::k2,
                                Toolkit::GuiSimple::ImageButton{aSdlCtx,
                                                                aSdlCtx.loadImage(GameTob::kDieImage2_70)});
    _inactiveDieButtons.emplace(Play::Die::Value::k3,
                                Toolkit::GuiSimple::ImageButton{aSdlCtx,
                                                                aSdlCtx.loadImage(GameTob::kDieImage3_70)});
    _inactiveDieButtons.emplace(Play::Die::Value::k4,
                                Toolkit::GuiSimple::ImageButton{aSdlCtx,
                                                                aSdlCtx.loadImage(GameTob::kDieImage4_70)});
    _inactiveDieButtons.emplace(Play::Die::Value::k5,
                                Toolkit::GuiSimple::ImageButton{aSdlCtx,
                                                                aSdlCtx.loadImage(GameTob::kDieImage5_70)});
    _inactiveDieButtons.emplace(Play::Die::Value::k6,
                                Toolkit::GuiSimple::ImageButton{aSdlCtx,
                                                                aSdlCtx.loadImage(GameTob::kDieImage6_70)});

    SdlDims aDims{};
    for (auto& aDieButton : _inactiveDieButtons)
    {
        // The active dice buttons are the same size
        aDims = aDieButton.second.getSize();

        if (aDims._w != GameTob::kDieImageDimension || aDims._h != GameTob::kDieImageDimension)
        {
            throw Main::RuntimeException{"Resources::Resources Error: Dice image is not the required dimension of " +
                                         std::to_string(GameTob::kDieImageDimension) + "x" +
                                         std::to_string(GameTob::kDieImageDimension) + " for dice number " +
                                         std::to_string(static_cast<unsigned int>(aDieButton.first))};
        }

        aDieButton.second.setState(Toolkit::GuiObjectState::kInactive);
    }
}

Resources::~Resources()
{
    Main::Log::dbg("~Resources()");
}

// Create an instance of the game options object
std::unique_ptr<Options::GameOptions> Resources::createGameOptions() const
{
    return std::unique_ptr<Options::GameOptions>{new GameTob::GameOptions{}};
}

// Create the first page
std::unique_ptr<Toolkit::GuiPage> Resources::createFirstPage(Main::GlobalContext& iCtx) const
{
    return std::unique_ptr<Toolkit::GuiPage>{new GameTob::Gui::MainPage{iCtx}};
}

std::shared_ptr<SdlFont> Resources::getLargeFont() const
{
    return _largeFont;
}

std::shared_ptr<SdlFont> Resources::getNormalFont() const
{
    return _normalFont;
}

std::shared_ptr<SdlFont> Resources::getSmallFont() const
{
    return _smallFont;
}

std::shared_ptr<SdlTexture> Resources::getFeltTexture() const
{
    return _feltTexture;
}

// Access a small-font text checkbox, active or inactive
const Toolkit::GuiSimple::TextCheckbox& Resources::getSmallTextCheckbox(bool iActiveCheckbox) const
{
    return (iActiveCheckbox ? _activeSmallTextCheckbox : _inactiveSmallTextCheckbox);
}

// Access a button for the specified die face, active or inactive; throws std::out_of_range if out of range
const Toolkit::GuiSimple::ImageButton& Resources::getDieButton(Play::Die::Value iValue, bool iActiveButton) const
{
    // at() method throws std::out_of_range
    return (iActiveButton ? _activeDieButtons.at(iValue) : _inactiveDieButtons.at(iValue));
}

}   // namespace Gui
}   // namespace GameTob

/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "GameTob/Gui/Help3Page.hpp"

#include "GameTob/Gui/Help2Page.hpp"
#include "GameTob/Gui/Help4Page.hpp"
#include "GameTob/Gui/Resources.hpp"
#include "GameTob/Play/State.hpp"
#include "Main/GlobalContext.hpp"
#include "Toolkit/GuiObject.hpp"

#include <SDL2/SDL_events.h>


namespace
{
const std::string kMultiplesText{"These multiples of the same die value score:"};
const std::string kMultiple200Text{"200 points"};
const std::string kMultiple300Text{"300 points"};
const std::string kMultiple400Text{"400 points"};
const std::string kMultiple500Text{"500 points"};
const std::string kMultiple600Text{"600 points"};
const std::string kMultiple1000Text{"1000 points"};
}


namespace GameTob
{
namespace Gui
{

using GameTob::Play::Die;

Help3Page::Help3Page(Main::GlobalContext& iCtx) :
    Toolkit::GuiPage{},
    _titleLabel{iCtx.sdlCtx(), iCtx.resources().getLargeFont(), "Help: Scoring (2 of 2)"},
    _backButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Back"},
    _prevButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Previous"},
    _nextButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Next"},

    _multiplesLabel{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), kMultiplesText},
    _multiple200Label{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), kMultiple200Text},
    _multiple300Label{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), kMultiple300Text},
    _multiple400Label{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), kMultiple400Text},
    _multiple500Label{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), kMultiple500Text},
    _multiple600Label{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), kMultiple600Text},
    _multiple1000Label{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), kMultiple1000Text},

    _multiple2001Button{Toolkit::GuiObject::CloneShareTag{},
                       dynamic_cast<GameTob::Gui::Resources&>(iCtx.resources()).getDieButton(Die::Value::k2, true)},
    _multiple2002Button{Toolkit::GuiObject::CloneShareTag{}, _multiple2001Button},
    _multiple2003Button{Toolkit::GuiObject::CloneShareTag{}, _multiple2001Button},
    _multiple3001Button{Toolkit::GuiObject::CloneShareTag{},
                        dynamic_cast<GameTob::Gui::Resources&>(iCtx.resources()).getDieButton(Die::Value::k3, true)},
    _multiple3002Button{Toolkit::GuiObject::CloneShareTag{}, _multiple3001Button},
    _multiple3003Button{Toolkit::GuiObject::CloneShareTag{}, _multiple3001Button},
    _multiple4001Button{Toolkit::GuiObject::CloneShareTag{},
                        dynamic_cast<GameTob::Gui::Resources&>(iCtx.resources()).getDieButton(Die::Value::k4, true)},
    _multiple4002Button{Toolkit::GuiObject::CloneShareTag{}, _multiple4001Button},
    _multiple4003Button{Toolkit::GuiObject::CloneShareTag{}, _multiple4001Button},
    _multiple5001Button{Toolkit::GuiObject::CloneShareTag{},
                        dynamic_cast<GameTob::Gui::Resources&>(iCtx.resources()).getDieButton(Die::Value::k5, true)},
    _multiple5002Button{Toolkit::GuiObject::CloneShareTag{}, _multiple5001Button},
    _multiple5003Button{Toolkit::GuiObject::CloneShareTag{}, _multiple5001Button},
    _multiple6001Button{Toolkit::GuiObject::CloneShareTag{},
                        dynamic_cast<GameTob::Gui::Resources&>(iCtx.resources()).getDieButton(Die::Value::k6, true)},
    _multiple6002Button{Toolkit::GuiObject::CloneShareTag{}, _multiple6001Button},
    _multiple6003Button{Toolkit::GuiObject::CloneShareTag{}, _multiple6001Button},
    _multiple10001Button{Toolkit::GuiObject::CloneShareTag{},
                         dynamic_cast<GameTob::Gui::Resources&>(iCtx.resources()).getDieButton(Die::Value::k1, true)},
    _multiple10002Button{Toolkit::GuiObject::CloneShareTag{}, _multiple10001Button},
    _multiple10003Button{Toolkit::GuiObject::CloneShareTag{}, _multiple10001Button},
    _multiple10004Button{Toolkit::GuiObject::CloneShareTag{}, _multiple10001Button},

    _allGuiObjects{&_titleLabel, &_backButton, &_prevButton, &_nextButton, &_multiplesLabel,
                   &_multiple2001Button, &_multiple2002Button, &_multiple2003Button, &_multiple200Label,
                   &_multiple3001Button, &_multiple3002Button, &_multiple3003Button, &_multiple300Label,
                   &_multiple4001Button, &_multiple4002Button, &_multiple4003Button, &_multiple400Label,
                   &_multiple5001Button, &_multiple5002Button, &_multiple5003Button, &_multiple500Label,
                   &_multiple6001Button, &_multiple6002Button, &_multiple6003Button, &_multiple600Label,
                   &_multiple10001Button, &_multiple10002Button, &_multiple10003Button, &_multiple10004Button,
                   &_multiple1000Label}
{
    // Position the title half way along the x-axis
    _titleLabel.setPosition({((iCtx.sdlCtx().getWindowSize()._w - _titleLabel.getSize()._w) / 2), 0});

    _multiplesLabel.setPosition({50, 100});

    _multiple2001Button.setPosition({50, 675});
    _multiple2002Button.setPosition({150, 675});
    _multiple2003Button.setPosition({250, 675});
    _multiple200Label.setPosition({350, 675});
    _multiple3001Button.setPosition({50, 575});
    _multiple3002Button.setPosition({150, 575});
    _multiple3003Button.setPosition({250, 575});
    _multiple300Label.setPosition({350, 575});
    _multiple4001Button.setPosition({50, 475});
    _multiple4002Button.setPosition({150, 475});
    _multiple4003Button.setPosition({250, 475});
    _multiple400Label.setPosition({350, 475});
    _multiple5001Button.setPosition({50, 375});
    _multiple5002Button.setPosition({150, 375});
    _multiple5003Button.setPosition({250, 375});
    _multiple500Label.setPosition({350, 375});
    _multiple6001Button.setPosition({50, 275});
    _multiple6002Button.setPosition({150, 275});
    _multiple6003Button.setPosition({250, 275});
    _multiple600Label.setPosition({350, 275});
    _multiple10001Button.setPosition({50, 175});
    _multiple10002Button.setPosition({150, 175});
    _multiple10003Button.setPosition({250, 175});
    _multiple10004Button.setPosition({350, 175});
    _multiple1000Label.setPosition({450, 175});

    // Position the back then next then previous buttons at the bottom right, with some padding
    _backButton.setPosition({iCtx.sdlCtx().getWindowSize()._w * 99 / 100 - _backButton.getSize()._w,
                             iCtx.sdlCtx().getWindowSize()._h * 99 / 100 - _backButton.getSize()._h});

    _nextButton.setPosition({_backButton.getPosition()._x - iCtx.sdlCtx().getWindowSize()._w * 1 / 100 -
                                 _nextButton.getSize()._w,
                             iCtx.sdlCtx().getWindowSize()._h * 99 / 100 - _nextButton.getSize()._h});

    _prevButton.setPosition({_nextButton.getPosition()._x - iCtx.sdlCtx().getWindowSize()._w * 1 / 100 -
                                 _prevButton.getSize()._w,
                             iCtx.sdlCtx().getWindowSize()._h * 99 / 100 - _prevButton.getSize()._h});
}

Help3Page::~Help3Page()
{
}

void Help3Page::handleEvents(const std::vector<SDL_Event>& iEvents, Main::GlobalContext& iCtx)
{
    for (auto* aGuiObject : _allGuiObjects)
    {
        aGuiObject->handleEvents(iEvents);
    }

    if (_backButton.wasClicked())
    {
        setPageState(Toolkit::GuiPageState::kReturn);
    }
    else if (_prevButton.wasClicked())
    {
        setPageState(Toolkit::GuiPageState::kSwapPage);
    }
    else if (_nextButton.wasClicked())
    {
        setPageState(Toolkit::GuiPageState::kSwapPage);
    }
    else
    {
        setPageState(Toolkit::GuiPageState::kActivePage);
    }
}

void Help3Page::draw(Main::GlobalContext& iCtx)
{
    iCtx.sdlCtx().setDrawColour(Main::kColourSolidRed);

    for (auto* aGuiObject : _allGuiObjects)
    {
        aGuiObject->draw(iCtx.sdlCtx());
    }
}

std::unique_ptr<Toolkit::GuiPage> Help3Page::newPage(Main::GlobalContext& iCtx)
{
    if (getPageState() == Toolkit::GuiPageState::kNextPage || getPageState() == Toolkit::GuiPageState::kSwapPage)
    {
        // Same order as handleEvents()
        if (_prevButton.wasClicked() && getPageState() == Toolkit::GuiPageState::kSwapPage)
        {
            return std::unique_ptr<Toolkit::GuiPage>{new Help2Page{iCtx}};
        }
        else if (_nextButton.wasClicked() && getPageState() == Toolkit::GuiPageState::kSwapPage)
        {
            return std::unique_ptr<Toolkit::GuiPage>{new Help4Page{iCtx}};
        }
    }

    return nullptr;
}

}   // namespace Gui
}   // namespace GameTob

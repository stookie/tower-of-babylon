/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "GameTob/Gui/GameOptionsPage.hpp"

#include "GameTob/GameOptions.hpp"
#include "GameTob/Gui/BoardPage.hpp"
#include "GameTob/Gui/Resources.hpp"
#include "Main/Constants.hpp"
#include "Main/GlobalContext.hpp"
#include "Sdl/SdlContext.hpp"

#include <SDL2/SDL_events.h>

#include <algorithm>
#include <sstream>


namespace
{
const std::string kPlayerNumberLabelText{"Number of players: "};
const std::string kRoundsNumberLabelText{"Number of rounds: "};
const std::string kHidePlayersTurnScoreLabelText{"Hide player's turn score until turn ended?"};
}


namespace GameTob
{
namespace Gui
{

GameOptionsPage::GameOptionsPage(Main::GlobalContext& iCtx, bool iForGameNotSettings) :
    Toolkit::GuiPage{},
    _titleLabel{iCtx.sdlCtx(), iCtx.resources().getLargeFont(), "Game Options"},
    _playerNumberLabel{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "TEMP"},
    _roundsNumberLabel{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "TEMP"},
    _hidePlayersTurnScoreLabel{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), kHidePlayersTurnScoreLabelText},
    _playerNumberUpArrowButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "+"},
    _playerNumberDownArrowButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "-"},
    _roundsNumberUpArrowButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "+"},
    _roundsNumberDownArrowButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "-"},
    _hidePlayersTurnScoreCheckbox{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), false},
    _startOrAcceptButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Start"},
    _cancelButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Cancel"},
    _resetButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Reset to Default"},
    _allGuiObjects{&_titleLabel,
                   &_playerNumberLabel, &_playerNumberUpArrowButton, &_playerNumberDownArrowButton,
                   &_roundsNumberLabel, &_roundsNumberUpArrowButton, &_roundsNumberDownArrowButton,
                   &_hidePlayersTurnScoreLabel, &_hidePlayersTurnScoreCheckbox,
                   &_startOrAcceptButton, &_cancelButton, &_resetButton},
    _forGameNotSettings{iForGameNotSettings},
    _localOptions{}
{
    if (iForGameNotSettings)
    {
        _resetButton.setState(Toolkit::GuiObjectState::kHidden);   // only for settings
        iCtx.setUsedGameOptionsFromDefault();
        _localOptions.copy(dynamic_cast<const GameTob::GameOptions&>(iCtx.usedGameOptions()));
    }
    else
    {
        _startOrAcceptButton.setText(iCtx.sdlCtx(), "Accept", false);
        _localOptions.copy(dynamic_cast<const GameTob::GameOptions&>(iCtx.defaultGameOptions()));
    }

    // Format the player number and rounds number labels text, and set the state of the controls
    updatePlayerNumberLabel(iCtx);
    updateRoundsNumberLabel(iCtx);
    setControlsState(iCtx, true, true, true);   // update everything

    // Make plus/minus buttons square
    int aSquareDim{0};
    aSquareDim = std::max(aSquareDim, _playerNumberUpArrowButton.getSize()._w);
    aSquareDim = std::max(aSquareDim, _playerNumberUpArrowButton.getSize()._h);
    aSquareDim = std::max(aSquareDim, _playerNumberDownArrowButton.getSize()._w);
    aSquareDim = std::max(aSquareDim, _playerNumberDownArrowButton.getSize()._h);

    const SdlDims aUpDownButtonDims{aSquareDim, aSquareDim};
    _playerNumberUpArrowButton.setSize(aUpDownButtonDims);
    _playerNumberDownArrowButton.setSize(aUpDownButtonDims);
    _roundsNumberUpArrowButton.setSize(aUpDownButtonDims);   // assumed the same as the players buttons
    _roundsNumberDownArrowButton.setSize(aUpDownButtonDims);

    // Position the title half way along the x-axis
    _titleLabel.setPosition({((iCtx.sdlCtx().getWindowSize()._w - _titleLabel.getSize()._w) / 2), 0});

    // Position buttons
    //XYZ FOR LATER USE TO LEFT JUSTIFY _startButton.setPosition({iSdlCtx.getWindowSize()._w * 10 / 100, 150);
    _playerNumberLabel.setPosition({50, 150});//XYZ HACK
    _playerNumberUpArrowButton.setPosition({400, 150});
    _playerNumberDownArrowButton.setPosition({500, 150});
    _roundsNumberLabel.setPosition({50, 250});//XYZ HACK
    _roundsNumberUpArrowButton.setPosition({400, 250});
    _roundsNumberDownArrowButton.setPosition({500, 250});
    _hidePlayersTurnScoreLabel.setPosition({50, 350});
    _hidePlayersTurnScoreCheckbox.setPosition({700, 350});

    // Position the cancel then accept buttons at the bottom right, with some padding
    _cancelButton.setPosition({iCtx.sdlCtx().getWindowSize()._w * 99 / 100 - _cancelButton.getSize()._w,
                               iCtx.sdlCtx().getWindowSize()._h * 99 / 100 - _cancelButton.getSize()._h});

    _startOrAcceptButton.setPosition({iCtx.sdlCtx().getWindowSize()._w * 97 / 100 -
                                          _startOrAcceptButton.getSize()._w - _cancelButton.getSize()._w,
                                      iCtx.sdlCtx().getWindowSize()._h * 99 / 100 - _startOrAcceptButton.getSize()._h});

    // Position the reset to default button at the bottom left, with some padding
    _resetButton.setPosition({iCtx.sdlCtx().getWindowSize()._w * 1 / 100,
                              iCtx.sdlCtx().getWindowSize()._h * 99 / 100 - _resetButton.getSize()._h});
}

GameOptionsPage::~GameOptionsPage()
{
}

void GameOptionsPage::handleEvents(const std::vector<SDL_Event>& iEvents, Main::GlobalContext& iCtx)
{
    for (auto* aGuiObject : _allGuiObjects)
    {
        aGuiObject->handleEvents(iEvents);
    }

    bool aPlayerNumberUpdated{false};
    bool aRoundsNumberUpdated{false};
    bool aAnythingElseUpdated{false};

    if (_playerNumberUpArrowButton.wasClicked())
    {
        _localOptions.setPlayerNumber(_localOptions.getPlayerNumber() + 1);
        aPlayerNumberUpdated = true;
    }
    else if (_playerNumberDownArrowButton.wasClicked())
    {
        _localOptions.setPlayerNumber(_localOptions.getPlayerNumber() - 1);
        aPlayerNumberUpdated = true;
    }
    else if (_roundsNumberUpArrowButton.wasClicked())
    {
        _localOptions.setRoundsNumber(_localOptions.getRoundsNumber() + 1);
        aRoundsNumberUpdated = true;
    }
    else if (_roundsNumberDownArrowButton.wasClicked())
    {
        _localOptions.setRoundsNumber(_localOptions.getRoundsNumber() - 1);
        aRoundsNumberUpdated = true;
    }
    else if (_hidePlayersTurnScoreCheckbox.wasClicked())
    {
        _localOptions.setHidePlayersTurnScore(_hidePlayersTurnScoreCheckbox.getChecked());
        aAnythingElseUpdated = true;
    }

    if (_cancelButton.wasClicked())
    {
        setPageState(Toolkit::GuiPageState::kReturn);
    }
    else if (_startOrAcceptButton.wasClicked())
    {
        if (_forGameNotSettings)
        {
            (dynamic_cast<GameTob::GameOptions&>(iCtx.usedGameOptions())).copy(_localOptions);
            setPageState(Toolkit::GuiPageState::kSwapPage);
        }
        else
        {
            (dynamic_cast<GameTob::GameOptions&>(iCtx.defaultGameOptions())).copy(_localOptions);
            setPageState(Toolkit::GuiPageState::kReturn);
        }
    }
    else if (_resetButton.wasClicked() && !_forGameNotSettings)
    {
        _localOptions.reset();
        aPlayerNumberUpdated = true;   // dont go to heroic efforts to determine these
        aRoundsNumberUpdated = true;
        aAnythingElseUpdated = true;
        setPageState(Toolkit::GuiPageState::kActivePage);
    }
    else
    {
        setPageState(Toolkit::GuiPageState::kActivePage);
    }

    if (aPlayerNumberUpdated)
    {
        updatePlayerNumberLabel(iCtx);
    }

    if (aRoundsNumberUpdated)
    {
        updateRoundsNumberLabel(iCtx);
    }

    if (aPlayerNumberUpdated || aRoundsNumberUpdated || aAnythingElseUpdated)
    {
        setControlsState(iCtx, aPlayerNumberUpdated, aRoundsNumberUpdated, aAnythingElseUpdated);
    }
}

void GameOptionsPage::draw(Main::GlobalContext& iCtx)
{
    iCtx.sdlCtx().setDrawColour(Main::kColourSolidRed);

    for (auto* aGuiObject : _allGuiObjects)
    {
        aGuiObject->draw(iCtx.sdlCtx());
    }
}

// The new page when the next page or a page swap is needed
std::unique_ptr<Toolkit::GuiPage> GameOptionsPage::newPage(Main::GlobalContext& iCtx)
{
    if (getPageState() == Toolkit::GuiPageState::kNextPage || getPageState() == Toolkit::GuiPageState::kSwapPage)
    {
        // Same order as handleEvents()
        if (_startOrAcceptButton.wasClicked() &&
            getPageState() == Toolkit::GuiPageState::kSwapPage &&
            _forGameNotSettings)
        {
            return std::unique_ptr<Toolkit::GuiPage>{new BoardPage{iCtx}};
        }
    }

    return nullptr;
}

// Set the state of the GUI controls
void GameOptionsPage::setControlsState(Main::GlobalContext& iCtx, bool iPlayers, bool iRounds, bool iAnythingElse)
{
    if (iAnythingElse)
    {
        if (_hidePlayersTurnScoreCheckbox.getChecked() != _localOptions.getHidePlayersTurnScore())
        {
            _hidePlayersTurnScoreCheckbox.setChecked(_localOptions.getHidePlayersTurnScore());
        }
    }

    if (iPlayers)
    {
        _playerNumberUpArrowButton.setState((_localOptions.getPlayerNumber() < _localOptions.getMaxPlayerNumber()) ?
                                            Toolkit::GuiObjectState::kActive :
                                            Toolkit::GuiObjectState::kInactive);

        _playerNumberDownArrowButton.setState((_localOptions.getPlayerNumber() > _localOptions.getMinPlayerNumber()) ?
                                              Toolkit::GuiObjectState::kActive :
                                              Toolkit::GuiObjectState::kInactive);
    }

    if (iRounds)
    {
        _roundsNumberUpArrowButton.setState((_localOptions.getRoundsNumber() < _localOptions.getMaxRoundsNumber()) ?
                                            Toolkit::GuiObjectState::kActive :
                                            Toolkit::GuiObjectState::kInactive);

        _roundsNumberDownArrowButton.setState((_localOptions.getRoundsNumber() > _localOptions.getMinRoundsNumber()) ?
                                              Toolkit::GuiObjectState::kActive :
                                              Toolkit::GuiObjectState::kInactive);
    }

    if (!_forGameNotSettings)
    {
        _startOrAcceptButton.setState(_localOptions.equal(iCtx.defaultGameOptions()) ?
                                      Toolkit::GuiObjectState::kInactive :
                                      Toolkit::GuiObjectState::kActive);

        _resetButton.setState(_localOptions.isDefault() ?
                              Toolkit::GuiObjectState::kInactive :
                              Toolkit::GuiObjectState::kActive);
    }
}

void GameOptionsPage::updatePlayerNumberLabel(Main::GlobalContext& iCtx)
{
    std::ostringstream aSS{};
    aSS << kPlayerNumberLabelText << _localOptions.getPlayerNumber();
    _playerNumberLabel.setText(iCtx.sdlCtx(), aSS.str(), false);
}

void GameOptionsPage::updateRoundsNumberLabel(Main::GlobalContext& iCtx)
{
    std::ostringstream aSS{};
    aSS << kRoundsNumberLabelText << _localOptions.getRoundsNumber();
    _roundsNumberLabel.setText(iCtx.sdlCtx(), aSS.str(), false);
}

}   // namespace Gui
}   // namespace GameTob

/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_GAME_TOB_GUI_HELP3PAGE
#define TOB_GAME_TOB_GUI_HELP3PAGE

#include "Toolkit/GuiPage.hpp"
#include "Toolkit/GuiSimple/ImageButton.hpp"
#include "Toolkit/GuiSimple/TextButton.hpp"
#include "Toolkit/GuiSimple/TextLabel.hpp"

#include <vector>

namespace Main
{
class GlobalContext;
}

namespace Toolkit
{
class GuiObject;
}


namespace GameTob
{
namespace Gui
{

class Help3Page : virtual public Toolkit::GuiPage
{
public:
    Help3Page(Main::GlobalContext& iCtx);
    virtual ~Help3Page() override;

    virtual void handleEvents(const std::vector<SDL_Event>& iEvents, Main::GlobalContext& iCtx) override;

    virtual void draw(Main::GlobalContext& iCtx) override;

    // The new page when the next page or a page swap is needed
    virtual std::unique_ptr<Toolkit::GuiPage> newPage(Main::GlobalContext& iCtx) override;

private:
    Toolkit::GuiSimple::TextLabel _titleLabel;
    Toolkit::GuiSimple::TextButton _backButton;
    Toolkit::GuiSimple::TextButton _prevButton;
    Toolkit::GuiSimple::TextButton _nextButton;

    Toolkit::GuiSimple::TextLabel _multiplesLabel;
    Toolkit::GuiSimple::TextLabel _multiple200Label;
    Toolkit::GuiSimple::TextLabel _multiple300Label;
    Toolkit::GuiSimple::TextLabel _multiple400Label;
    Toolkit::GuiSimple::TextLabel _multiple500Label;
    Toolkit::GuiSimple::TextLabel _multiple600Label;
    Toolkit::GuiSimple::TextLabel _multiple1000Label;

    Toolkit::GuiSimple::ImageButton _multiple2001Button;
    Toolkit::GuiSimple::ImageButton _multiple2002Button;
    Toolkit::GuiSimple::ImageButton _multiple2003Button;
    Toolkit::GuiSimple::ImageButton _multiple3001Button;
    Toolkit::GuiSimple::ImageButton _multiple3002Button;
    Toolkit::GuiSimple::ImageButton _multiple3003Button;
    Toolkit::GuiSimple::ImageButton _multiple4001Button;
    Toolkit::GuiSimple::ImageButton _multiple4002Button;
    Toolkit::GuiSimple::ImageButton _multiple4003Button;
    Toolkit::GuiSimple::ImageButton _multiple5001Button;
    Toolkit::GuiSimple::ImageButton _multiple5002Button;
    Toolkit::GuiSimple::ImageButton _multiple5003Button;
    Toolkit::GuiSimple::ImageButton _multiple6001Button;
    Toolkit::GuiSimple::ImageButton _multiple6002Button;
    Toolkit::GuiSimple::ImageButton _multiple6003Button;
    Toolkit::GuiSimple::ImageButton _multiple10001Button;
    Toolkit::GuiSimple::ImageButton _multiple10002Button;
    Toolkit::GuiSimple::ImageButton _multiple10003Button;
    Toolkit::GuiSimple::ImageButton _multiple10004Button;

    // Collection of all GUI objects to allow easy iteration
    std::vector<Toolkit::GuiObject*> _allGuiObjects;
};

}   // namespace Gui
}   // namespace GameTob

#endif   // TOB_GAME_TOB_GUI_HELP3PAGE

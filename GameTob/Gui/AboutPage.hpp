/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2023 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_GAME_TOB_GUI_ABOUTPAGE
#define TOB_GAME_TOB_GUI_ABOUTPAGE

#include "Toolkit/GuiPage.hpp"
#include "Toolkit/GuiSimple/TextButton.hpp"
#include "Toolkit/GuiSimple/TextLabel.hpp"
#include "Utils/Timers.hpp"

#include <vector>

namespace Main
{
class GlobalContext;
}


namespace GameTob
{
namespace Gui
{

class AboutPage : virtual public Toolkit::GuiPage
{
public:
    AboutPage(Main::GlobalContext& iCtx);
    virtual ~AboutPage() override;

    virtual void handleEvents(const std::vector<SDL_Event>& iEvents, Main::GlobalContext& iCtx) override;

    virtual void draw(Main::GlobalContext& iCtx) override;

private:
    Toolkit::GuiSimple::TextLabel _titleLabel;
    Toolkit::GuiSimple::TextButton _backButton;
    Toolkit::GuiSimple::TextLabel _versionLabel;
    Toolkit::GuiSimple::TextLabel _authorsLabel;
    Toolkit::GuiSimple::TextLabel _author1Label;
    Toolkit::GuiSimple::TextLabel _copyrightLabel;
    Toolkit::GuiSimple::TextLabel _licenseLabel;
    Toolkit::GuiSimple::TextButton _projectButton;
    Toolkit::GuiSimple::TextButton _copyrightButton;
    Toolkit::GuiSimple::TextButton _sourceCodeLicenseButton;
    Toolkit::GuiSimple::TextLabel _clipboardLabel;

    // Collection of all GUI objects to allow easy iteration
    std::vector<Toolkit::GuiObject*> _allGuiObjects;

    Utils::SimpleTimer _clipboardTimer;
};

}   // namespace Gui
}   // namespace GameTob

#endif   // TOB_GAME_TOB_GUI_ABOUTPAGE

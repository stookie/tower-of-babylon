/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_GAME_TOB_GUI_HELP4PAGE
#define TOB_GAME_TOB_GUI_HELP4PAGE

#include "Toolkit/GuiPage.hpp"
#include "Toolkit/GuiSimple/TextButton.hpp"
#include "Toolkit/GuiSimple/TextLabel.hpp"

#include <vector>

namespace Main
{
class GlobalContext;
}

namespace Toolkit
{
class GuiObject;
}


namespace GameTob
{
namespace Gui
{

class Help4Page : virtual public Toolkit::GuiPage
{
public:
    Help4Page(Main::GlobalContext& iCtx);
    virtual ~Help4Page() override;

    virtual void handleEvents(const std::vector<SDL_Event>& iEvents, Main::GlobalContext& iCtx) override;

    virtual void draw(Main::GlobalContext& iCtx) override;

    // The new page when the next page or a page swap is needed
    virtual std::unique_ptr<Toolkit::GuiPage> newPage(Main::GlobalContext& iCtx) override;

private:
    Toolkit::GuiSimple::TextLabel _titleLabel;
    Toolkit::GuiSimple::TextButton _backButton;
    Toolkit::GuiSimple::TextButton _prevButton;
    Toolkit::GuiSimple::TextButton _nextButton;
    Toolkit::GuiSimple::TextLabel _help1aLabel;
    Toolkit::GuiSimple::TextLabel _help1bLabel;
    Toolkit::GuiSimple::TextLabel _help2aLabel;
    Toolkit::GuiSimple::TextLabel _help2bLabel;
    Toolkit::GuiSimple::TextLabel _help3Label;
    Toolkit::GuiSimple::TextLabel _help3aLabel;
    Toolkit::GuiSimple::TextLabel _help3bLabel;
    Toolkit::GuiSimple::TextLabel _help3cLabel;
    Toolkit::GuiSimple::TextLabel _help3dLabel;

    // Collection of all GUI objects to allow easy iteration
    std::vector<Toolkit::GuiObject*> _allGuiObjects;
};

}   // namespace Gui
}   // namespace GameTob

#endif   // TOB_GAME_TOB_GUI_HELP4PAGE

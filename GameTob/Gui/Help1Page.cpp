/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "GameTob/Gui/Help1Page.hpp"

#include "GameTob/Gui/Help2Page.hpp"
#include "Main/GlobalContext.hpp"
#include "Toolkit/GuiObject.hpp"

#include <SDL2/SDL_events.h>


namespace
{
const std::string kText1{"Take turns to roll dice in a race to score 5000 points!"};

const std::string kText2a{"When it is your turn, six dice are rolled."};
const std::string kText2b{"If you score no points, you are bust and your turn is over."};

const std::string kText3{"If you score points, you can end your turn, and your points are banked."};

const std::string kText4a{"Otherwise, you can keep some or all of the point scoring dice,"};
const std::string kText4b{"and roll the remaining dice again."};

const std::string kText5{"As before, if you then score no extra points, you are bust and your turn is over."};

const std::string kText6a{"As long as you score extra points each roll,"};
const std::string kText6b{"you can end your turn and bank your points,"};
const std::string kText6c{"or keep some point scoring dice and roll the remaining dice again."};
}


namespace GameTob
{
namespace Gui
{

Help1Page::Help1Page(Main::GlobalContext& iCtx) :
    Toolkit::GuiPage{},
    _titleLabel{iCtx.sdlCtx(), iCtx.resources().getLargeFont(), "Help: General Idea"},
    _backButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Back"},
    _prevButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Previous"},
    _nextButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Next"},
    _help1Label{iCtx.sdlCtx(), iCtx.resources().getSmallFont(), kText1},
    _help2aLabel{iCtx.sdlCtx(), iCtx.resources().getSmallFont(), kText2a},
    _help2bLabel{iCtx.sdlCtx(), iCtx.resources().getSmallFont(), kText2b},
    _help3Label{iCtx.sdlCtx(), iCtx.resources().getSmallFont(), kText3},
    _help4aLabel{iCtx.sdlCtx(), iCtx.resources().getSmallFont(), kText4a},
    _help4bLabel{iCtx.sdlCtx(), iCtx.resources().getSmallFont(), kText4b},
    _help5Label{iCtx.sdlCtx(), iCtx.resources().getSmallFont(), kText5},
    _help6aLabel{iCtx.sdlCtx(), iCtx.resources().getSmallFont(), kText6a},
    _help6bLabel{iCtx.sdlCtx(), iCtx.resources().getSmallFont(), kText6b},
    _help6cLabel{iCtx.sdlCtx(), iCtx.resources().getSmallFont(), kText6c},
    _allGuiObjects{&_titleLabel, &_backButton, &_prevButton, &_nextButton,
                   &_help1Label, &_help2aLabel, &_help2bLabel, &_help3Label, &_help4aLabel, &_help4bLabel,
                   &_help5Label, &_help6aLabel, &_help6bLabel, &_help6cLabel}
{
    // Position the title half way along the x-axis
    _titleLabel.setPosition({((iCtx.sdlCtx().getWindowSize()._w - _titleLabel.getSize()._w) / 2), 0});

    _help1Label.setPosition({40,100});
    _help2aLabel.setPosition({40,175});
    _help2bLabel.setPosition({40,225});
    _help3Label.setPosition({40,300});
    _help4aLabel.setPosition({40,375});
    _help4bLabel.setPosition({40,425});
    _help5Label.setPosition({40,500});
    _help6aLabel.setPosition({40,575});
    _help6bLabel.setPosition({40,625});
    _help6cLabel.setPosition({40,675});

    // Position the back then next then previous buttons at the bottom right, with some padding
    _backButton.setPosition({iCtx.sdlCtx().getWindowSize()._w * 99 / 100 - _backButton.getSize()._w,
                             iCtx.sdlCtx().getWindowSize()._h * 99 / 100 - _backButton.getSize()._h});

    _nextButton.setPosition({_backButton.getPosition()._x - iCtx.sdlCtx().getWindowSize()._w * 1 / 100 -
                                 _nextButton.getSize()._w,
                             iCtx.sdlCtx().getWindowSize()._h * 99 / 100 - _nextButton.getSize()._h});

    _prevButton.setPosition({_nextButton.getPosition()._x - iCtx.sdlCtx().getWindowSize()._w * 1 / 100 -
                                 _prevButton.getSize()._w,
                             iCtx.sdlCtx().getWindowSize()._h * 99 / 100 - _prevButton.getSize()._h});

    // No previous page from page 1
    _prevButton.setState(Toolkit::GuiObjectState::kInactive);
}

Help1Page::~Help1Page()
{
}

void Help1Page::handleEvents(const std::vector<SDL_Event>& iEvents, Main::GlobalContext& iCtx)
{
    for (auto* aGuiObject : _allGuiObjects)
    {
        aGuiObject->handleEvents(iEvents);
    }

    if (_backButton.wasClicked())
    {
        setPageState(Toolkit::GuiPageState::kReturn);
    }
    else if (_nextButton.wasClicked())
    {
        setPageState(Toolkit::GuiPageState::kSwapPage);
    }
    else
    {
        setPageState(Toolkit::GuiPageState::kActivePage);
    }
}

void Help1Page::draw(Main::GlobalContext& iCtx)
{
    iCtx.sdlCtx().setDrawColour(Main::kColourSolidRed);

    for (auto* aGuiObject : _allGuiObjects)
    {
        aGuiObject->draw(iCtx.sdlCtx());
    }
}

std::unique_ptr<Toolkit::GuiPage> Help1Page::newPage(Main::GlobalContext& iCtx)
{
    if (getPageState() == Toolkit::GuiPageState::kNextPage || getPageState() == Toolkit::GuiPageState::kSwapPage)
    {
        // Same order as handleEvents()
        if (_nextButton.wasClicked() && getPageState() == Toolkit::GuiPageState::kSwapPage)
        {
            return std::unique_ptr<Toolkit::GuiPage>{new Help2Page{iCtx}};
        }
    }

    return nullptr;
}

}   // namespace Gui
}   // namespace GameTob

/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2023 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_GAME_TOB_GUI_MAINPAGE
#define TOB_GAME_TOB_GUI_MAINPAGE

#include "Toolkit/GuiPage.hpp"
#include "Toolkit/GuiSimple/TextButton.hpp"
#include "Toolkit/GuiSimple/TextLabel.hpp"

#include <memory>
#include <vector>

namespace Main
{
class GlobalContext;
}


namespace GameTob
{
namespace Gui
{

class MainPage : virtual public Toolkit::GuiPage
{
public:
    MainPage(Main::GlobalContext& iCtx);
    virtual ~MainPage() override;

    virtual void handleEvents(const std::vector<SDL_Event>& iEvents, Main::GlobalContext& iCtx) override;

    virtual void draw(Main::GlobalContext& iCtx) override;

    // The new page when the next page or a page swap is needed
    virtual std::unique_ptr<Toolkit::GuiPage> newPage(Main::GlobalContext& iCtx) override;

private:
    Toolkit::GuiSimple::TextButton _quitButton;
    Toolkit::GuiSimple::TextButton _aboutButton;
    Toolkit::GuiSimple::TextButton _helpButton;
    Toolkit::GuiSimple::TextButton _playButton;
    Toolkit::GuiSimple::TextButton _settingsButton;
    Toolkit::GuiSimple::TextLabel _titleLabel;

    // Collection of all GUI objects to allow easy iteration
    std::vector<Toolkit::GuiObject*> _allGuiObjects;
};

}   // namespace Gui
}   // namespace GameTob

#endif   // TOB_GAME_TOB_GUI_MAINPAGE

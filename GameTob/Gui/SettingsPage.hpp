/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_GAME_TOB_GUI_SETTINGSPAGE
#define TOB_GAME_TOB_GUI_SETTINGSPAGE

#include "Toolkit/GuiPage.hpp"
#include "Toolkit/GuiSimple/TextButton.hpp"
#include "Toolkit/GuiSimple/TextLabel.hpp"

#include <vector>

namespace Main
{
class GlobalContext;
}


namespace GameTob
{
namespace Gui
{

class SettingsPage : virtual public Toolkit::GuiPage
{
public:
    SettingsPage(Main::GlobalContext& iCtx);
    virtual ~SettingsPage() override;

    virtual void handleEvents(const std::vector<SDL_Event>& iEvents, Main::GlobalContext& iCtx) override;

    virtual void draw(Main::GlobalContext& iCtx) override;

    // The new page when the next page or a page swap is needed
    virtual std::unique_ptr<Toolkit::GuiPage> newPage(Main::GlobalContext& iCtx) override;

private:
    Toolkit::GuiSimple::TextLabel _titleLabel;
    Toolkit::GuiSimple::TextButton _backButton;
    Toolkit::GuiSimple::TextButton _saveButton;
    Toolkit::GuiSimple::TextButton _resetButton;
    Toolkit::GuiSimple::TextButton _graphicsButton;
    Toolkit::GuiSimple::TextButton _audioButton;
    Toolkit::GuiSimple::TextButton _gameOptionsButton;

    // Collection of all GUI objects to allow easy iteration
    std::vector<Toolkit::GuiObject*> _allGuiObjects;
};

}   // namespace Gui
}   // namespace GameTob

#endif   // TOB_GAME_TOB_GUI_SETTINGSPAGE

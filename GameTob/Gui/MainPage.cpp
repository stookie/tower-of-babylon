/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "GameTob/Gui/MainPage.hpp"

#include "GameTob/Gui/AboutPage.hpp"
#include "GameTob/Gui/GameOptionsPage.hpp"
#include "GameTob/Gui/Help1Page.hpp"
#include "GameTob/Gui/Resources.hpp"
#include "GameTob/Gui/SettingsPage.hpp"
#include "Main/Constants.hpp"
#include "Main/GlobalContext.hpp"
#include "Sdl/SdlContext.hpp"

#include <SDL2/SDL_events.h>


namespace GameTob
{
namespace Gui
{

MainPage::MainPage(Main::GlobalContext& iCtx) :
    Toolkit::GuiPage{},
    _quitButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Quit"},
    _aboutButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "About"},
    _helpButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Help"},
    _playButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Play !"},
    _settingsButton{iCtx.sdlCtx(), iCtx.resources().getNormalFont(), "Settings"},
    _titleLabel{iCtx.sdlCtx(), iCtx.resources().getLargeFont(), "Tower Of Babylon"},
    _allGuiObjects{&_quitButton, &_aboutButton, &_helpButton, &_playButton, &_settingsButton, &_titleLabel}
{
    //XYZ WAY TOO MUCH HARD CODING...
    // Position the quit button at the bottom right, with some padding
    _quitButton.setPosition({iCtx.sdlCtx().getWindowSize()._w * 99 / 100 - _quitButton.getSize()._w,
                             iCtx.sdlCtx().getWindowSize()._h * 99 / 100 - _quitButton.getSize()._h});

    // Position other buttons in a row
    _playButton.setPosition({((iCtx.sdlCtx().getWindowSize()._w - _playButton.getSize()._w) / 2), 150});
    _settingsButton.setPosition({((iCtx.sdlCtx().getWindowSize()._w - _settingsButton.getSize()._w) / 2), 250});
    _helpButton.setPosition({((iCtx.sdlCtx().getWindowSize()._w - _helpButton.getSize()._w) / 2), 350});
    _aboutButton.setPosition({((iCtx.sdlCtx().getWindowSize()._w - _aboutButton.getSize()._w) / 2), 450});

    // Position the title half way along the x-axis
    _titleLabel.setPosition({((iCtx.sdlCtx().getWindowSize()._w - _titleLabel.getSize()._w) / 2), 0});
}

MainPage::~MainPage()
{
}

void MainPage::handleEvents(const std::vector<SDL_Event>& iEvents, Main::GlobalContext& iCtx)
{
    for (auto* aGuiObject : _allGuiObjects)
    {
        aGuiObject->handleEvents(iEvents);
    }

    if (_quitButton.wasClicked())
    {
        setPageState(Toolkit::GuiPageState::kReturn);
    }
    else if (_playButton.wasClicked())
    {
        setPageState(Toolkit::GuiPageState::kNextPage);
    }
    else if (_settingsButton.wasClicked())
    {
        setPageState(Toolkit::GuiPageState::kNextPage);
    }
    else if (_helpButton.wasClicked())
    {
        setPageState(Toolkit::GuiPageState::kNextPage);
    }
    else if (_aboutButton.wasClicked())
    {
        setPageState(Toolkit::GuiPageState::kNextPage);
    }
    else
    {
        setPageState(Toolkit::GuiPageState::kActivePage);
    }
}

void MainPage::draw(Main::GlobalContext& iCtx)
{
    iCtx.sdlCtx().setDrawColour(Main::kColourSolidRed);

    for (auto* aGuiObject : _allGuiObjects)
    {
        aGuiObject->draw(iCtx.sdlCtx());
    }
}

// The new page when the next page or a page swap is needed
std::unique_ptr<Toolkit::GuiPage> MainPage::newPage(Main::GlobalContext& iCtx)
{
    if (getPageState() == Toolkit::GuiPageState::kNextPage || getPageState() == Toolkit::GuiPageState::kSwapPage)
    {
        // Same order as handleEvents()
        if (_playButton.wasClicked() && getPageState() == Toolkit::GuiPageState::kNextPage)
        {
            return std::unique_ptr<Toolkit::GuiPage>{new GameOptionsPage{iCtx, true}};
        }
        else if (_settingsButton.wasClicked() && getPageState() == Toolkit::GuiPageState::kNextPage)
        {
            return std::unique_ptr<Toolkit::GuiPage>{new SettingsPage{iCtx}};
        }
        else if (_helpButton.wasClicked() && getPageState() == Toolkit::GuiPageState::kNextPage)
        {
            return std::unique_ptr<Toolkit::GuiPage>{new Help1Page{iCtx}};
        }
        else if (_aboutButton.wasClicked() && getPageState() == Toolkit::GuiPageState::kNextPage)
        {
            return std::unique_ptr<Toolkit::GuiPage>{new AboutPage{iCtx}};
        }
    }

    return nullptr;
}

}   // namespace Gui
}   // namespace GameTob

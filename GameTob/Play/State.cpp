/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "GameTob/Play/State.hpp"

#include "GameTob/GameOptions.hpp"
#include "Main/Exceptions.hpp"
#include "Main/GlobalContext.hpp"
#include "Utils/RandomGenerator.hpp"


namespace GameTob
{
namespace Play
{

State::State(Main::GlobalContext& iCtx) :
    _maximumRounds{},
    _currentRound{},
    _players{},
    _firstPlayerInThisRound{},
    _currentPlayer{},
    _currentPlayersTurnScore{},
    _currentPlayerIsBust{},
    _hidePlayersTurnScore{},
    _hidingCurrentPlayersTurnScore{},
    _dice{}
{
    const GameTob::GameOptions& aGameOptions{dynamic_cast<GameTob::GameOptions&>(iCtx.usedGameOptions())};

    if (aGameOptions.getRoundsNumber() == 0)
    {
        throw Main::LogicException{"State::State Error: rounds number is zero"};
    }
    else if (aGameOptions.getPlayerNumber() == 0)
    {
        throw Main::LogicException{"State::State Error: player number is zero"};
    }

    _maximumRounds = aGameOptions.getRoundsNumber();
    _currentRound = 0;
    _players.resize(aGameOptions.getPlayerNumber(), Player{});
    _hidePlayersTurnScore = aGameOptions.getHidePlayersTurnScore();

    const auto aMaxPlayerNumber = static_cast<Utils::RandomGenerator::UnsignedShort>(aGameOptions.getPlayerNumber());

    if (aMaxPlayerNumber != aGameOptions.getPlayerNumber())
    {
        throw Main::LogicException{"State::State Error: player number is too large"};
    }

    _firstPlayerInThisRound = iCtx.randomNumbers().selectNumber(0, aMaxPlayerNumber - 1);

    nextRound(true);
}

bool State::requestRollAgain(Utils::RandomGenerator& iRandomNumbers)
{
    // Rules are:
    //  the score of the currently kept and locked dice is noted;
    //  each locked die is skipped;
    //  each kept die is updated to locked;
    //  each discarded die is re-rolled;
    //  if the new score of all dice is not higher than the noted kept/locked dice score (ie must score each roll),
    //   the player is bust;
    //  else if the current score this turn and round score are less than 500 and no more moves can be made,
    //   the player is bust;   (eg kept 22255, roll 1, score is 400, no more moves)
    //  else,
    //   the new score becomes the current turn score;

    if (!canRollAgain())
    {
        return false;
    }

    const Score aKeptOrLockedDiceTurnScore = calculateCurrentDiceScore(true);   // kept or locked dice only

    for (auto& aDie : _dice)
    {
        if (aDie._choice == Die::Choice::kKeep)
        {
            aDie._choice = Die::Choice::kLocked;
        }
        else if (aDie._choice == Die::Choice::kDiscard)
        {
            auto aDieRoll{iRandomNumbers.dieRoll()};
            aDie._value = static_cast<Die::Value>(aDieRoll);
        }
    }

    const Score aNewTurnScore = calculateCurrentDiceScore(false);   // all dice

    if (aNewTurnScore <= aKeptOrLockedDiceTurnScore)
    {
        _currentPlayersTurnScore = 0;
        _currentPlayerIsBust = true;

        if (_hidePlayersTurnScore && _hidingCurrentPlayersTurnScore)
        {
            _hidingCurrentPlayersTurnScore = false;
        }
    }
    else if (aNewTurnScore < 500 && _players[_currentPlayer]._currentRoundScore < 500 && areNoMoreMoves())
    {
        _currentPlayersTurnScore = aNewTurnScore;
        _currentPlayerIsBust = true;

        if (_hidePlayersTurnScore && _hidingCurrentPlayersTurnScore)
        {
            _hidingCurrentPlayersTurnScore = false;
        }
    }
    else
    {
        _currentPlayersTurnScore = aNewTurnScore;
    }

    return true;
}

bool State::requestEndTurn()
{
    // Rules are:
    //  if the current player is bust,
    //   start the next turn;
    //  else,
    //   bank the current turn score;
    //   if the current player has won the round,
    //    bank the round win;
    //    start the next round;
    //   else,
    //    start the next turn;

    if (!canEndTurn())
    {
        return false;
    }

    if (_currentPlayerIsBust)
    {
        nextTurn(false);
    }
    else if (_hidePlayersTurnScore && _hidingCurrentPlayersTurnScore)
    {
        // End turn once to reveal score, and again to actually end turn
        _hidingCurrentPlayersTurnScore = false;
    }
    else
    {
        _players[_currentPlayer]._currentRoundScore += _currentPlayersTurnScore;

        if (_players[_currentPlayer]._currentRoundScore >= 5000)
        {
            _players[_currentPlayer]._roundsWon++;
            nextRound(false);
        }
        else
        {
            nextTurn(false);
        }
    }

    return true;
}

bool State::requestKeepDie(DieNum iDieNum, bool iKept)
{
    // Rules are:
    //  if the specified die has been locked,
    //   do nothing;
    //  else,
    //   keep/unkeep the die as requested

    if (!canKeepOrUnkeepDie(iDieNum))
    {
        return false;
    }

    // at() method throws std::out_of_range
    if (_dice.at(iDieNum)._choice == Die::Choice::kLocked)
    {
        return false;
    }
    else
    {
        _dice.at(iDieNum)._choice = (iKept ? Die::Choice::kKeep : Die::Choice::kDiscard);
        return true;
    }
}

bool State::requestKeepDieFlip(DieNum iDieNum)
{
    // (see requestKeepDie for rules)

    if (!canKeepOrUnkeepDie(iDieNum))
    {
        return false;
    }

    const bool aKept = (getDieChoice(iDieNum) == Die::Choice::kDiscard) ? true : false;
    return requestKeepDie(iDieNum, aKept);
}

bool State::canRollAgain() const
{
    // Rules are, can roll again if:
    //  the game is not over; and
    //  the current player is not bust; and
    //  either the game option to hide players turn scores is not set,
    //   or it is and the current players turn score is still hidden; and
    //  either the current points this turn is zero (ie yet to roll),
    //   or the dice choices (kept or discarded) are valid

    if (!isGameOver() && !_currentPlayerIsBust && (!_hidePlayersTurnScore || _hidingCurrentPlayersTurnScore))
    {
        if (_currentPlayersTurnScore == 0)
        {
            return true;
        }
        else
        {
            return areDiceChoicesValid();
        }
    }
    else
    {
        return false;
    }
}

bool State::canEndTurn() const
{
    // Rules are, can end turn if:
    //  the game is not over; and
    //  either the current player is bust,
    //   or has accumulated some points this turn; and
    //   either current score this turn or round score is greater than or equal to 500,

    return (!isGameOver() &&
            (_currentPlayerIsBust ||
             (_currentPlayersTurnScore > 0 && _players[_currentPlayer]._currentRoundScore >= 500) ||
             _currentPlayersTurnScore >= 500));
}

bool State::canKeepOrUnkeepDie(DieNum iDieNum) const
{
    // Rules are, can keep/unkeep a die if:
    //  the game is not over; and
    //  the current player is not bust; and
    //  either the game option to hide players turn scores is not set,
    //   or it is and the current players turn score is still hidden; and
    //  the specified die has not been locked;
    //  and the specified die is not the blank value

    // at() method throws std::out_of_range if out of range
    return (!isGameOver() &&
            !_currentPlayerIsBust &&
            (!_hidePlayersTurnScore || _hidingCurrentPlayersTurnScore) &&
            _dice.at(iDieNum)._choice != Die::Choice::kLocked &&
            _dice.at(iDieNum)._value != Die::Value::kBlank);
}

std::size_t State::getMaximumRounds() const
{
    return _maximumRounds;
}

std::size_t State::getCurrentRound() const
{
    if (isGameOver())
    {
        // For cosmetics, rely on every action being forbidden to not corrupt state
        return _currentRound - 1;
    }
    else
    {
        return _currentRound;
    }
}

bool State::isGameOver() const
{
    return _currentRound >= _maximumRounds;
}

PlayerNum State::getPlayerNumber() const
{
    return _players.size();
}

RoundScore State::getPlayersRoundsWon(PlayerNum iPlayerNum) const
{
    // at() method throws std::out_of_range
    return _players.at(iPlayerNum)._roundsWon;
}

Score State::getPlayersCurrentRoundScore(PlayerNum iPlayerNum) const
{
    // at() method throws std::out_of_range
    return _players.at(iPlayerNum)._currentRoundScore;
}

PlayerNum State::getCurrentPlayer() const
{
    return _currentPlayer;
}

std::string State::getCurrentPlayersTurnScoreStr() const
{
    if (_hidePlayersTurnScore && _currentPlayersTurnScore == 0 && _currentPlayerIsBust == false)
    {
        return "";
    }
    else if (_hidePlayersTurnScore && _hidingCurrentPlayersTurnScore)
    {
        return "???";
    }
    else
    {
        return std::to_string(_currentPlayersTurnScore);
    }
}

PlayerNum State::getFirstPlayerInThisRound() const
{
    return _firstPlayerInThisRound;
}

Die::Choice State::getDieChoice(DieNum iDieNum) const
{
    // at() method throws std::out_of_range
    return _dice.at(iDieNum)._choice;
}

Die::Value State::getDieValue(DieNum iDieNum) const
{
    // at() method throws std::out_of_range
    return _dice.at(iDieNum)._value;
}

// Current players turn score, protected to allow turn score hiding
Score State::getCurrentPlayersTurnScore() const
{
    return _currentPlayersTurnScore;
}

// Start the next round; does not consider the maximum number of rounds, so the caller must do this
void State::nextRound(bool iFirstRound)
{
    if (!iFirstRound)
    {
        _currentRound++;
        _firstPlayerInThisRound++;

        if (_firstPlayerInThisRound >= _players.size())
        {
            _firstPlayerInThisRound = 0;
        }
    }

    if (!isGameOver())
    {
        for (auto& aPlayer : _players)
        {
            aPlayer._currentRoundScore = 0;
        }

        _currentPlayer = _firstPlayerInThisRound;

        nextTurn(true);
    }
}

// Start the next turn; does not consider the round victory or points accumulation, so the caller must do this
void State::nextTurn(bool iFirstTurn)
{
    for (auto& aDie : _dice)
    {
        aDie._choice = Die::Choice::kDiscard;
        aDie._value = Die::Value::kBlank;
    }

    if (!iFirstTurn)
    {
        _currentPlayer++;

        if (_currentPlayer >= _players.size())
        {
            _currentPlayer = 0;
        }
    }

    _currentPlayersTurnScore = 0;
    _currentPlayerIsBust = false;

    if (_hidePlayersTurnScore)
    {
        _hidingCurrentPlayersTurnScore = true;
    }
}

// Return whether the keep / discard dice choices are valid
bool State::areDiceChoicesValid() const
{
    // Rules are, dice choices are valid if:
    //  at least one die has been requested to be kept; and
    //  at least one die has been requested to be discarded; and
    //  kept dice all meet these combinations: { 1*, 5*, 222, 333, 444, 666 }
    //   (can keep any number of 1 or 5; but three and only three of 2, 3, 4, 6)
    //   (six-of-a-kind and one-of-each are only considered in score calculations)

    bool aAnyDiceKept{false};
    bool aAnyDiceDiscarded{false};
    std::size_t aNumberOf2s{0};
    std::size_t aNumberOf3s{0};
    std::size_t aNumberOf4s{0};
    std::size_t aNumberOf6s{0};
    std::size_t aNumberOfBlanks{0};

    for (const auto& aDie : _dice)
    {
        if (aDie._choice == Die::Choice::kDiscard)
        {
            aAnyDiceDiscarded = true;
        }
        else if (aDie._choice == Die::Choice::kKeep)
        {
            aAnyDiceKept = true;

            if (aDie._value == Die::Value::kBlank)
            {
                aNumberOfBlanks++;
            }
            else if (aDie._value == Die::Value::k2)
            {
                aNumberOf2s++;
            }
            else if (aDie._value == Die::Value::k3)
            {
                aNumberOf3s++;
            }
            else if (aDie._value == Die::Value::k4)
            {
                aNumberOf4s++;
            }
            else if (aDie._value == Die::Value::k6)
            {
                aNumberOf6s++;
            }
        }
    }

    return (aAnyDiceKept && aAnyDiceDiscarded &&
            (aNumberOf2s == 0 || aNumberOf2s == 3) &&
            (aNumberOf3s == 0 || aNumberOf3s == 3) &&
            (aNumberOf4s == 0 || aNumberOf4s == 3) &&
            (aNumberOf6s == 0 || aNumberOf6s == 3) &&
            aNumberOfBlanks == 0);
}

// Return whether the keep / discard and locked dice choices mean no more moves can be made
// Specifically used for making a player bust if they have not scored 500; eg locked 22255, roll 1, score is 400
bool State::areNoMoreMoves() const
{
    // Rules are, assuming the player has just rolled, no more moves can be made if:
    //  either the dice selected to keep or discard is only a 1 or 5, and 5 dice are locked,
    //   or the dice selected to keep or discard are 222 or 333 or 444 or 666, and 3 dice are locked

    std::size_t aNumberOf1s{0};
    std::size_t aNumberOf2s{0};
    std::size_t aNumberOf3s{0};
    std::size_t aNumberOf4s{0};
    std::size_t aNumberOf5s{0};
    std::size_t aNumberOf6s{0};
    std::size_t aNumberOfBlanks{0};
    std::size_t aNumberOfLocked{0};

    for (const auto& aDie : _dice)
    {
        if (aDie._choice == Die::Choice::kDiscard || aDie._choice == Die::Choice::kKeep)
        {
            if (aDie._value == Die::Value::kBlank)
            {
                aNumberOfBlanks++;
            }
            else if (aDie._value == Die::Value::k1)
            {
                aNumberOf1s++;
            }
            else if (aDie._value == Die::Value::k2)
            {
                aNumberOf2s++;
            }
            else if (aDie._value == Die::Value::k3)
            {
                aNumberOf3s++;
            }
            else if (aDie._value == Die::Value::k4)
            {
                aNumberOf4s++;
            }
            else if (aDie._value == Die::Value::k5)
            {
                aNumberOf5s++;
            }
            else if (aDie._value == Die::Value::k6)
            {
                aNumberOf6s++;
            }
        }
        else if (aDie._choice == Die::Choice::kLocked)
        {
            aNumberOfLocked++;
        }
    }

    return aNumberOfBlanks == 0 &&
           ((aNumberOfLocked == 5 && (aNumberOf1s == 1 || aNumberOf5s == 1)) ||
            (aNumberOfLocked == 3 && (aNumberOf2s == 3 || aNumberOf3s == 3 || aNumberOf4s == 3 || aNumberOf6s == 3)));
}

Score State::calculateCurrentDiceScore(bool iKeptOrLockedDiceOnly) const
{
    // Rules are:
    //  4x 1 = 1000; single 1 not part of the 4x = 100;
    //  3x 5 = 500; single 5 not part of the 3x = 50;
    //  3x 2 = 200;
    //  3x 3 = 300;
    //  3x 4 = 400;
    //  3x 6 = 600;
    //  6x any = 5000;
    //  one of each (ie 1,2,3,4,5,6) = 1000

    std::size_t aNumberOf1s{0};
    std::size_t aNumberOf2s{0};
    std::size_t aNumberOf3s{0};
    std::size_t aNumberOf4s{0};
    std::size_t aNumberOf5s{0};
    std::size_t aNumberOf6s{0};

    for (const auto& aDie : _dice)
    {
        if ((iKeptOrLockedDiceOnly && aDie._choice != Die::Choice::kDiscard) || !iKeptOrLockedDiceOnly)
        {
            if (aDie._value == Die::Value::k1)
            {
                aNumberOf1s++;
            }
            else if (aDie._value == Die::Value::k2)
            {
                aNumberOf2s++;
            }
            else if (aDie._value == Die::Value::k3)
            {
                aNumberOf3s++;
            }
            else if (aDie._value == Die::Value::k4)
            {
                aNumberOf4s++;
            }
            else if (aDie._value == Die::Value::k5)
            {
                aNumberOf5s++;
            }
            else if (aDie._value == Die::Value::k6)
            {
                aNumberOf6s++;
            }
        }
    }

    Score aScore{0};

    if ((aNumberOf1s == 6) || (aNumberOf2s == 6) || (aNumberOf3s == 6) ||
        (aNumberOf4s == 6) || (aNumberOf5s == 6) || (aNumberOf6s == 6))
    {
        aScore = 5000;
    }
    else if ((aNumberOf1s == 1) && (aNumberOf2s == 1) && (aNumberOf3s == 1) &&
             (aNumberOf4s == 1) && (aNumberOf5s == 1) && (aNumberOf6s == 1))
    {
        aScore = 1000;
    }
    else
    {
        if (aNumberOf1s > 0)
        {
            aScore += (aNumberOf1s >= 4 ? (1000 + (aNumberOf1s - 4) * 100) : (aNumberOf1s * 100));
        }

        if (aNumberOf2s >= 3)
        {
            aScore += 200;
        }

        if (aNumberOf3s >= 3)
        {
            aScore += 300;
        }

        if (aNumberOf4s >= 3)
        {
            aScore += 400;
        }

        if (aNumberOf5s > 0)
        {
            aScore += (aNumberOf5s >= 3 ? (500 + (aNumberOf5s - 3) * 50) : (aNumberOf5s * 50));
        }

        if (aNumberOf6s >= 3)
        {
            aScore += 600;
        }
    }

    return aScore;
}

}   // namespace Play
}   // namespace GameTob

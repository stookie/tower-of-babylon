/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_GAME_TOB_PLAY_STATE
#define TOB_GAME_TOB_PLAY_STATE

#include <array>
#include <string>
#include <utility>
#include <vector>

namespace Main
{
class GlobalContext;
}

namespace Utils
{
class RandomGenerator;
}


namespace GameTob
{
namespace Play
{

// Aliases, to make the interface clearer
using Score = unsigned int;
using RoundScore = unsigned int;
using PlayerNum = std::size_t;
using DieNum = std::size_t;


class Player
{
public:
    // Allow main State class to treat this as a structure
    friend class State;

    ~Player() = default;

    Player(const Player&) = default;
    Player& operator=(const Player&) = default;
    Player(Player&&) = default;
    Player& operator=(Player&&) = default;

private:
    Player() = default;

    RoundScore _roundsWon{0};
    Score _currentRoundScore{0};
};


class Die
{
public:
    // Enum representing the values of a 6 sided die, including allowance for blank or unset
    enum class Value : unsigned char
    {
        // Note k1 must have the enumerated value 1, and so forth, to allow casting to integers and vice versa
        kBlank = 0,
        k1 = 1,
        k2 = 2,
        k3 = 3,
        k4 = 4,
        k5 = 5,
        k6 = 6
    };

    // Enum representing the choice of whether to keep or discard a die, or whether it has been locked already
    enum class Choice : unsigned char
    {
        kDiscard,
        kKeep,
        kLocked
    };

    // Allow main State class to treat this as a structure
    friend class State;

    static constexpr DieNum kDiceNumberInGame{6};

    ~Die() = default;

    Die(const Die&) = default;
    Die& operator=(const Die&) = default;
    Die(Die&&) = default;
    Die& operator=(Die&&) = default;

private:
    Die() = default;

    Choice _choice{Choice::kDiscard};
    Value _value{Value::kBlank};
};


class State
{
public:
    State(Main::GlobalContext& iCtx);   // throws Main::LogicException if player or rounds number is invalid
    virtual ~State() = default;

    State(const State&) = delete;
    State& operator=(const State&) = delete;
    State(State&&) = delete;
    State& operator=(State&&) = delete;

    // Interaction with the game; these return true if successfully applied, false otherwise

    bool requestRollAgain(Utils::RandomGenerator& iRandomNumbers);
    bool requestEndTurn();
    bool requestKeepDie(DieNum iDieNum, bool iKept);   // throws std::out_of_range if out of range
    bool requestKeepDieFlip(DieNum iDieNum);   // throws std::out_of_range if out of range

    // Querying the game interaction possibilities

    bool canRollAgain() const;
    bool canEndTurn() const;
    bool canKeepOrUnkeepDie(DieNum iDieNum) const;   // throws std::out_of_range if out of range

    // Querying the game state
    //XYZ CONSIDER IF THIS CAN BE OPTIMISED? ONLY RETURN CHANGES SINCE LAST CALL? COMBINE INTO FEWER CALLS?

    std::size_t getMaximumRounds() const;
    std::size_t getCurrentRound() const;
    bool isGameOver() const;

    PlayerNum getPlayerNumber() const;
    RoundScore getPlayersRoundsWon(PlayerNum iPlayerNum) const;   // throws std::out_of_range if out of range
    Score getPlayersCurrentRoundScore(PlayerNum iPlayerNum) const;   // throws std::out_of_range if out of range
    PlayerNum getCurrentPlayer() const;
    std::string getCurrentPlayersTurnScoreStr() const;
    PlayerNum getFirstPlayerInThisRound() const;

    Die::Choice getDieChoice(DieNum iDieNum) const;   // throws std::out_of_range if out of range
    Die::Value getDieValue(DieNum iDieNum) const;   // throws std::out_of_range if out of range

protected:
    // Current players turn score, protected to allow turn score hiding
    Score getCurrentPlayersTurnScore() const;

    // Start the next round; does not consider the maximum number of rounds, so the caller must do this
    void nextRound(bool iFirstRound);

    // Start the next turn; does not consider the round victory or points accumulation, so the caller must do this
    void nextTurn(bool iFirstTurn);

    // Return whether the keep / discard dice choices are valid
    bool areDiceChoicesValid() const;

    // Return whether the keep / discard and locked dice choices mean no more moves can be made
    // Specifically used for making a player bust if they have not scored 500; eg locked 22255, roll 1, score is 400
    bool areNoMoreMoves() const;

    // Calculate the current score of the rolled dice
    Score calculateCurrentDiceScore(bool iKeptOrLockedDiceOnly) const;

private:
    std::size_t _maximumRounds;
    std::size_t _currentRound;

    std::vector<Player> _players;
    PlayerNum _firstPlayerInThisRound;
    PlayerNum _currentPlayer;
    Score _currentPlayersTurnScore;
    bool _currentPlayerIsBust;

    bool _hidePlayersTurnScore;   // whether or not to hide a players turn score until finalised; from game options
    bool _hidingCurrentPlayersTurnScore;   // whether or not the current players turns score is being hid

    std::array<Die, Die::kDiceNumberInGame> _dice;
};

}   // namespace Play
}   // namespace GameTob

#endif   // TOB_GAME_TOB_PLAY_STATE

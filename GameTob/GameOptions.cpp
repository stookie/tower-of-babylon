/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "GameTob/GameOptions.hpp"

#include "GameTob/Constants.hpp"
#include "Main/Exceptions.hpp"
#include "Utils/LoaderHelpers.hpp"


namespace
{

// Validation functions: keep these bare minimum/maximum numbers separate from Constants intentionally
constexpr bool validatePlayerNumber(std::size_t iP)
{
    return (iP > 0);
}

constexpr bool validateRoundsNumber(std::size_t iR)
{
    return (iR > 0);
}

static_assert(validatePlayerNumber(GameTob::kMinPlayerNumber), "Minimum player number is unsupported");
static_assert(validatePlayerNumber(GameTob::kMaxPlayerNumber), "Maximum player number is unsupported");
static_assert(validatePlayerNumber(GameTob::kMinPlayerNumber <= GameTob::kMaxPlayerNumber),
              "Minimum player number greater than maximum player number which is unsupported");
static_assert(validateRoundsNumber(GameTob::kMinRoundsNumber), "Minimum rounds number is unsupported");
static_assert(validateRoundsNumber(GameTob::kMaxRoundsNumber), "Maximum rounds number is unsupported");
static_assert(validatePlayerNumber(GameTob::kMinRoundsNumber <= GameTob::kMaxRoundsNumber),
              "Minimum rounds number greater than maximum rounds number which is unsupported");


// Constants defining the current options format version, and the oldest compatible options format version.
// The options format is only backwards compatible (code reads old files), not forwards (code tries to read new files).
// A version of "0" is special, indicating a non-versioned, development-only version.
std::size_t kCurrentOptionsVersion{0};
std::size_t kCompatibleOptionsVersion{0};

// Constants for the save/load file
const std::string kOptionsDelimiters{"Options:"};
const std::string kOptionsVersion{"OptionsVersion:"};
const std::string kPlayerNumber{"PlayerNumber:"};
const std::string kPlayerName{"PlayerName:"};
const std::string kRoundsNumber{"RoundsNumber:"};
const std::string kHidePlayersTurnScore{"HidePlayersTurnScore:"};

// Misc constants
constexpr bool kDefaultHidePlayersTurnScore{false};   // not worth adding to game constants
const std::string kDefaultPlayerNamePrefix{"Player "};

// Convenience function for generating the default name for a player, based on which player number it is, indexed from 0
std::string generateDefaultPlayerName(std::size_t iPlayerNumber)
{
    return (kDefaultPlayerNamePrefix + std::to_string(iPlayerNumber + 1));
}

}   // unnamed namespace


namespace GameTob
{

GameOptions::GameOptions() :
    _playerNames{},
    _roundsNumber{kMinRoundsNumber},
    _hidePlayersTurnScore{kDefaultHidePlayersTurnScore}
{
    _playerNames.reserve(kMaxPlayerNumber);
    setPlayerNumber(kMinPlayerNumber, true);   // generate default names
}

GameOptions::~GameOptions()
{
}

// Saving to a stream
void GameOptions::save(std::ostream& ioStr) const
{
    ioStr << std::boolalpha;
    ioStr << kOptionsDelimiters << ' ' << "START" << '\n';
    ioStr << kOptionsVersion << ' ' << kCurrentOptionsVersion << '\n';
    ioStr << kPlayerNumber << ' ' << getPlayerNumber() << '\n';
    for (const auto& aName : getPlayerNames())
    {
        ioStr << kPlayerName << ' ' << aName << '\n';
    }
    ioStr << kRoundsNumber << ' ' << getRoundsNumber() << '\n';
    ioStr << kHidePlayersTurnScore << ' ' << getHidePlayersTurnScore() << '\n';
    ioStr << kOptionsDelimiters << ' ' << "END" << '\n';
}

// Loading from a stream; throws if setting a game option does
void GameOptions::load(std::istream& iStr)
{
    iStr >> std::boolalpha;
    Utils::loadKeysValue<std::string>(iStr, kOptionsDelimiters);   // Dont care about the value

    std::size_t aFileVersion = Utils::loadKeysValue<std::size_t>(iStr, kOptionsVersion);

    if (aFileVersion != 0 && aFileVersion < kCompatibleOptionsVersion)
    {
        throw Main::RuntimeException("GameOptions::load Error: settings file version " + std::to_string(aFileVersion) +
                                     " is too low, the minimum supported is " +
                                     std::to_string(kCompatibleOptionsVersion));
    }

    setPlayerNumber(Utils::loadKeysValue<std::size_t>(iStr, kPlayerNumber), false);   // dont generate default names
    for (std::size_t aPlayerNumber = 0; aPlayerNumber < getPlayerNumber(); aPlayerNumber++)
    {
        setPlayerName(aPlayerNumber, Utils::loadKeysLine(iStr, kPlayerName));
    }
    setRoundsNumber(Utils::loadKeysValue<std::size_t>(iStr, kRoundsNumber));
    setHidePlayersTurnScore(Utils::loadKeysValue<bool>(iStr, kHidePlayersTurnScore));
    Utils::loadKeysValue<std::string>(iStr, kOptionsDelimiters);   // Dont care about the value
}

// Comparing two GameOptions instances
bool GameOptions::equal(const Options::GameOptions& iGameOptions) const
{
    auto aGameOptions = dynamic_cast<const GameOptions* const>(&iGameOptions);

    return (aGameOptions != nullptr &&
            _playerNames == aGameOptions->_playerNames &&
            _roundsNumber == aGameOptions->_roundsNumber &&
            _hidePlayersTurnScore == aGameOptions->_hidePlayersTurnScore);
}

// Comparing the GameOptions instance to default values
bool GameOptions::isDefault() const
{
    // Ignore the comparison of the player names
    return (_playerNames.size() == kMinPlayerNumber &&
            _roundsNumber == kMinRoundsNumber &&
            _hidePlayersTurnScore == kDefaultHidePlayersTurnScore);
}

// Copying from another GameOptions instances
void GameOptions::copy(const Options::GameOptions& iGameOptions)
{
    auto aGameOptions = dynamic_cast<const GameOptions* const>(&iGameOptions);

    if (aGameOptions != nullptr)
    {
        _playerNames = aGameOptions->_playerNames;
        _roundsNumber = aGameOptions->_roundsNumber;
        _hidePlayersTurnScore = aGameOptions->_hidePlayersTurnScore;
    }
}

// Resetting the GameOptions instance
void GameOptions::reset()
{
    _playerNames.clear();
    setPlayerNumber(kMinPlayerNumber, true);
    _roundsNumber = kMinRoundsNumber;
    _hidePlayersTurnScore = kDefaultHidePlayersTurnScore;
}

std::size_t GameOptions::getPlayerNumber() const
{
    return _playerNames.size();
}

// throws Main::LogicException if player number is invalid
void GameOptions::setPlayerNumber(std::size_t iPlayerNumber)
{
    setPlayerNumber(iPlayerNumber, true);
}

const std::vector<std::string>& GameOptions::getPlayerNames() const
{
    return _playerNames;
}

// throws Main::LogicException if player number is invalid
const std::string& GameOptions::getPlayerName(std::size_t iPlayerNumber) const
{
    if (iPlayerNumber >= _playerNames.size())
    {
        throw Main::LogicException{"GameOptions::getPlayerName Error: player number is invalid: " + iPlayerNumber};
    }

    return _playerNames[iPlayerNumber];
}

// throws Main::LogicException if player number is invalid
void GameOptions::setPlayerName(std::size_t iPlayerNumber, const std::string& iName)
{
    if (iPlayerNumber >= _playerNames.size())
    {
        throw Main::LogicException{"GameOptions::setPlayerName Error: player number is invalid: " + iPlayerNumber};
    }

    _playerNames[iPlayerNumber] = iName;
}

std::size_t GameOptions::getRoundsNumber() const
{
    return _roundsNumber;
}

// throws Main::LogicException if rounds number is invalid
void GameOptions::setRoundsNumber(std::size_t iRoundsNumber)
{
    if (iRoundsNumber < kMinRoundsNumber || iRoundsNumber > kMaxRoundsNumber)
    {
        throw Main::LogicException{"GameOptions::setRoundsNumber Error: round number is invalid: " + iRoundsNumber};
    }

    _roundsNumber = iRoundsNumber;
}

bool GameOptions::getHidePlayersTurnScore() const
{
    return _hidePlayersTurnScore;
}

void GameOptions::setHidePlayersTurnScore(bool iHidePlayersTurnScore)
{
    _hidePlayersTurnScore = iHidePlayersTurnScore;
}

// Set the number of players; either using blank strings or default generated names if increasing the number
//  throws Main::LogicException if player number is invalid
void GameOptions::setPlayerNumber(std::size_t iPlayerNumber, bool iGenerateDefaultNames)
{
    if (iPlayerNumber < kMinPlayerNumber || iPlayerNumber > kMaxPlayerNumber)
    {
        throw Main::LogicException{"GameOptions::setPlayerNumber Error: player number is invalid: " + iPlayerNumber};
    }

    if (iPlayerNumber > getPlayerNumber() && iGenerateDefaultNames)
    {
        for (std::size_t aNewPlayerNumber = getPlayerNumber(); aNewPlayerNumber < iPlayerNumber; aNewPlayerNumber++)
        {
            _playerNames.push_back(generateDefaultPlayerName(aNewPlayerNumber));
        }
    }
    else if (iPlayerNumber != getPlayerNumber())
    {
        _playerNames.resize(iPlayerNumber);
    }
}

}   // namespace GameTob

/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_GAME_TOB_GAMEOPTIONS
#define TOB_GAME_TOB_GAMEOPTIONS

#include "GameTob/Constants.hpp"

#include "Options/GameOptions.hpp"

#include <istream>
#include <ostream>
#include <string>
#include <vector>


namespace GameTob
{

class GameOptions : virtual public Options::GameOptions
{
public:
    GameOptions();
    virtual ~GameOptions() override;

    GameOptions(const GameOptions&) = default;
    GameOptions& operator=(const GameOptions&) = default;
    GameOptions(GameOptions&&) = delete;
    GameOptions& operator=(GameOptions&&) = delete;

    // Saving to a stream
    virtual void save(std::ostream& ioStr) const override;

    // Loading from a stream; throws if setting a game option does
    virtual void load(std::istream& iStr) override;

    // Comparing two GameOptions instances
    virtual bool equal(const Options::GameOptions& iGameOptions) const override;

    // Comparing the GameOptions instance to default values
    virtual bool isDefault() const override;

    // Copying from another GameOptions instances
    virtual void copy(const Options::GameOptions& iGameOptions) override;

    // Resetting the GameOptions instance
    virtual void reset() override;

    static constexpr std::size_t getMinPlayerNumber();
    static constexpr std::size_t getMaxPlayerNumber();
    static constexpr std::size_t getMinRoundsNumber();
    static constexpr std::size_t getMaxRoundsNumber();

    std::size_t getPlayerNumber() const;
    void setPlayerNumber(std::size_t iPlayerNumber);   // throws Main::LogicException if player number is invalid
    const std::vector<std::string>& getPlayerNames() const;
    const std::string& getPlayerName(std::size_t iPlayerNumber) const;   // throws Main::LogicException like above
    void setPlayerName(std::size_t iPlayerNumber, const std::string& iName);   // throws Main::LogicException like above

    std::size_t getRoundsNumber() const;
    void setRoundsNumber(std::size_t iRoundsNumber);   // throws Main::LogicException if rounds number is invalid

    bool getHidePlayersTurnScore() const;
    void setHidePlayersTurnScore(bool iHidePlayersTurnScore);

private:
    // Set the number of players; either using blank strings or default generated names if increasing the number;
    //  throws Main::LogicException if player number is invalid
    void setPlayerNumber(std::size_t iPlayerNumber, bool iGenerateDefaultNames);

    std::vector<std::string> _playerNames;
    std::size_t _roundsNumber;
    bool _hidePlayersTurnScore;
};

inline constexpr std::size_t GameOptions::getMinPlayerNumber()
{
    return kMinPlayerNumber;
}

inline constexpr std::size_t GameOptions::getMaxPlayerNumber()
{
    return kMaxPlayerNumber;
}

inline constexpr std::size_t GameOptions::getMinRoundsNumber()
{
    return kMinRoundsNumber;
}

inline constexpr std::size_t GameOptions::getMaxRoundsNumber()
{
    return kMaxRoundsNumber;
}

}   // namespace GameTob

#endif   // TOB_GAME_TOB_GAMEOPTIONS

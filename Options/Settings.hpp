/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_OPTIONS_SETTINGS
#define TOB_OPTIONS_SETTINGS

#include <istream>
#include <ostream>


namespace Options
{

class Settings
{
public:
    Settings();
    virtual ~Settings() = default;

    Settings(const Settings&) = default;
    Settings& operator=(const Settings&) = default;
    Settings(Settings&&) = delete;
    Settings& operator=(Settings&&) = delete;

    // Saving to a stream
    void save(std::ostream& ioStr) const;

    // Loading from a stream; throws if setting a setting does
    void load(std::istream& iStr);

    // Comparing two Settings instances
    bool equal(const Settings& iSettings) const;
    bool equalGraphics(const Settings& iSettings) const;

    // Comparing the Settings instance to default values
    bool isDefault() const;
    bool isDefaultGraphics() const;

    // Copying from another Settings instances
    void copy(const Settings& iSettings);
    void copyGraphics(const Settings& iSettings);

    // Resetting the Settings instance
    void reset();
    void resetGraphics();

    // Graphics:
    void setVsync(bool iVsync);
    bool getVsync();
    bool hasVsyncChanged();

    int getWindowWidth() const;
    int getWindowHeight() const;
    void setWindowWidth(int iW);   // throws Main::LogicException if window width is invalid
    void setWindowHeight(int iH);   // throws Main::LogicException if window height is invalid

private:
    // Graphics
    bool _vsync;
    bool _vsyncChanged;   // has the Vsync settings changed since it was last checked; NOT saved/loaded

    int _windowWidth;
    int _windowHeight;

    // Audio

    // Network
};

}   // namespace Options

#endif   // TOB_OPTIONS_SETTINGS

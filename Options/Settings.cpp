/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "Options/Settings.hpp"

#include "Main/Constants.hpp"
#include "Main/Exceptions.hpp"
#include "Main/Log.hpp"
#include "Utils/LoaderHelpers.hpp"


namespace
{

// Validation functions: keep these bare minimum dimensions separate from Constants intentionally
constexpr bool validateWindowWidth(int iW)
{
    return (iW >= 200);
}

constexpr bool validateWindowHeight(int iH)
{
    return (iH >= 200);
}

static_assert(validateWindowWidth(Main::kInitialWindowWidth), "Initial Window width is unsupported");
static_assert(validateWindowHeight(Main::kInitialWindowHeight), "Initial Window height is unsupported");


// Constants defining the current settings format version, and the oldest compatible settings format version.
// The settings format is only backwards compatible (code reads old files), not forwards (code tries to read new files).
// A version of "0" is special, indicating a non-versioned, development-only version.
std::size_t kCurrentSettingsVersion{0};
std::size_t kCompatibleSettingsVersion{0};

// Constants for the save/load file
const std::string kSettingsDelimiters{"Settings:"};
const std::string kSettingsVersion{"SettingsVersion:"};
const std::string kSettingsVsync{"Vsync:"};
const std::string kSettingsWindowWidth{"WindowWidth:"};
const std::string kSettingsWindowHeight{"WindowHeight:"};

}   // unnamed namespace


namespace Options
{

Settings::Settings() :
    _vsync{Main::kInitialVsync},
    _vsyncChanged{false},
    _windowWidth{Main::kInitialWindowWidth},
    _windowHeight{Main::kInitialWindowHeight}
{
}

// Saving to a stream
void Settings::save(std::ostream& ioStr) const
{
    ioStr << std::boolalpha;
    ioStr << kSettingsDelimiters << ' ' << "START" << '\n';
    ioStr << kSettingsVersion << ' ' << kCurrentSettingsVersion << '\n';
    ioStr << kSettingsVsync << ' ' << _vsync << '\n';
    ioStr << kSettingsWindowWidth << ' ' << _windowWidth << '\n';
    ioStr << kSettingsWindowHeight << ' ' << _windowHeight << '\n';
    ioStr << kSettingsDelimiters << ' ' << "END" << '\n';
}

// Loading from a stream; throws if setting a setting does
void Settings::load(std::istream& iStr)
{
    iStr >> std::boolalpha;
    Utils::loadKeysValue<std::string>(iStr, kSettingsDelimiters);   // Dont care about the value

    std::size_t aFileVersion = Utils::loadKeysValue<std::size_t>(iStr, kSettingsVersion);

    if (aFileVersion != 0 && aFileVersion < kCompatibleSettingsVersion)
    {
        throw Main::RuntimeException("Settings::load Error: settings file version " + std::to_string(aFileVersion) +
                                     " is too low, the minimum supported is " +
                                     std::to_string(kCompatibleSettingsVersion));
    }

    setVsync(Utils::loadKeysValue<bool>(iStr, kSettingsVsync));
    setWindowWidth(Utils::loadKeysValue<int>(iStr, kSettingsWindowWidth));
    setWindowHeight(Utils::loadKeysValue<int>(iStr, kSettingsWindowHeight));
    Utils::loadKeysValue<std::string>(iStr, kSettingsDelimiters);   // Dont care about the value
}

// Comparing two Settings instances
bool Settings::equal(const Settings& iSettings) const
{
    return equalGraphics(iSettings);
}

bool Settings::equalGraphics(const Settings& iSettings) const
{
    return (_vsync == iSettings._vsync &&
            _windowWidth == iSettings._windowWidth &&
            _windowHeight == iSettings._windowHeight);
}

// Comparing the Settings instance to default values
bool Settings::isDefault() const
{
    return isDefaultGraphics();
}

bool Settings::isDefaultGraphics() const
{
    return (_vsync == Main::kInitialVsync &&
            _windowWidth == Main::kInitialWindowWidth &&
            _windowHeight == Main::kInitialWindowHeight);
}

// Copying from another Settings instances
void Settings::copy(const Settings& iSettings)
{
    copyGraphics(iSettings);;
}

void Settings::copyGraphics(const Settings& iSettings)
{
    setVsync(iSettings._vsync);
    _windowWidth = iSettings._windowWidth;
    _windowHeight = iSettings._windowHeight;
}

// Resetting the Settings instance
void Settings::reset()
{
    resetGraphics();
}

void Settings::resetGraphics()
{
    setVsync(Main::kInitialVsync);
    _windowWidth = Main::kInitialWindowWidth;
    _windowHeight = Main::kInitialWindowHeight;
}

// Graphics:

void Settings::setVsync(bool iVsync)
{
    if (_vsync != iVsync)
    {
        _vsyncChanged = true;
        _vsync = iVsync;
    }
}

bool Settings::getVsync()
{
    _vsyncChanged = false;
    return _vsync;
}

bool Settings::hasVsyncChanged()
{
    bool aVsyncChanged = _vsyncChanged;
    _vsyncChanged = false;
    return aVsyncChanged;
}

int Settings::getWindowWidth() const
{
    return _windowWidth;
}

int Settings::getWindowHeight() const
{
    return _windowHeight;
}

// throws Main::LogicException if window width is invalid
void Settings::setWindowWidth(int iW)
{
    if (!validateWindowWidth(iW))
    {
        throw Main::LogicException{"Settings::setWindowWidth Error: window width is invalid: " + iW};
    }

    _windowWidth = iW;
}

// throws Main::LogicException if window height is invalid
void  Settings::setWindowHeight(int iH)
{
    if (!validateWindowHeight(iH))
    {
        throw Main::LogicException{"Settings::setWindowHeight Error: window height is invalid: " + iH};
    }

    _windowHeight = iH;
}

}   // namespace Options

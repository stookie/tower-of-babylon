/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_OPTIONS_COMMAND_LINE
#define TOB_OPTIONS_COMMAND_LINE

#include "Main/Log.hpp"

#include <string>


namespace Options
{

// Class to hold command line options
class CommandLine
{
public:
    CommandLine(int iArgc, char** iArgv);
    ~CommandLine() = default;

    CommandLine(const CommandLine&) = delete;
    CommandLine& operator=(const CommandLine&) = delete;

    CommandLine(CommandLine&&) = default;
    CommandLine& operator=(CommandLine&&) = default;

    // Print a usage message and quit, either with an error or a normal exit code
    [[noreturn]] void doPrintUsage(bool iError) const;

    // Print version information and quit normally
    [[noreturn]] void doPrintVersions() const;

    // Whether the command line was parsed successfully or not
    bool isValid() const;

    // Getters
    bool printHelp() const;
    bool printVersions() const;
    bool disableSound() const;
    bool escapeKeyQuits() const;
    Main::Log::Level minLogLevel() const;
    bool randomSeedSet() const;
    std::uint_fast32_t randomSeed() const;
    const std::string& executableName() const;

private:
    // Parse the command line options
    bool parseOptions(int iArgc, char** iArgv);

    bool _valid;
    bool _printHelp;
    bool _printVersions;
    bool _disableSound;
    bool _escapeKeyQuits;
    Main::Log::Level _minLogLevel;
    bool _randomSeedSet;
    std::uint_fast32_t _randomSeed;
    std::string _executableName;
};

// Print the CommandLine class
std::ostream& operator<<(std::ostream& ioOs, const CommandLine& iCommandLine);

}   // namespace Options

#endif   // TOB_OPTIONS_COMMAND_LINE

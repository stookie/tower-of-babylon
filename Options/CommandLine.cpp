/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "Options/CommandLine.hpp"

#include "Main/Constants.hpp"

#include <SDL2/SDL_version.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include <cstdlib>
#include <ostream>
#include <string>


namespace
{

// Access to Main::Log, Options::CommandLine
using namespace Main;
using namespace Options;

// Command line option strings
const std::string kHelpOption{"-help"};
const std::string kVersionOption{"-version"};
const std::string kDisableSoundOption{"-disablesound"};
const std::string kEscapeKeyOption{"-escapekey="};
const std::string kMinLogLevel{"-minloglevel="};
const std::string kRandomSeedOption{"-randomseed="};
//XYZ ADD ITERATION FRAME RATE

}   // unnamed namespace


namespace Options
{

CommandLine::CommandLine(int iArgc, char** iArgv) :
    _valid{false},
    _printHelp{},
    _printVersions{},
    _disableSound{},
    _escapeKeyQuits{Main::kDefaultEscapeKeyOption},
    _minLogLevel{Log::getDefaultMinimumLogLevel()},
    _randomSeedSet{},
    _randomSeed{},
    _executableName{}
{
    _valid = parseOptions(iArgc, iArgv);
}

// Print a usage message and quit, either with an error or a normal exit code
[[noreturn]]
void CommandLine::doPrintUsage(bool iError) const
{
    Log::print("Usage: ", _executableName, " [options]");
    Log::print("  ", kHelpOption, " : prints this message");
    Log::print("  ", kVersionOption, " : prints version information");
    Log::print("  ", kDisableSoundOption, " : disables the sound output unconditionally");
    Log::print("  ",
               kEscapeKeyOption,
               "[yes|no] : sets whether the escape key quits the application or not; default is ",
               (kDefaultEscapeKeyOption ? "yes" : "no"));
    Log::print("  ",
               kMinLogLevel,
               "[",
               Log::Level::kTrace, "|",
               Log::Level::kDebug, "|",
               Log::Level::kInfo, "|",
               Log::Level::kWarn, "|",
               Log::Level::kError, "|",
               Log::Level::kPlain, "]"
               " : sets the minimum logging level; default is ",
               Log::getDefaultMinimumLogLevel(), ")");
    Log::print("  ",
               kRandomSeedOption,
               "[SEED] : sets the random number seed, where SEED is an unsigned 32 bit integer\n");
    std::exit(iError ? 1 : 0);
}

// Print version information and quit normally
[[noreturn]]
void CommandLine::doPrintVersions() const
{
    Log::print("Program version (", _executableName, ") : None (in development)");
    Log::print("Network API version : None (not developed yet)");

    SDL_version aVersion{};
    SDL_VERSION(&aVersion);
    Log::print("SDL compiled version : ", int{aVersion.major}, ".", int{aVersion.minor}, ".", int{aVersion.patch});

    aVersion = SDL_version{};
    SDL_GetVersion(&aVersion);
    Log::print("SDL linked version : ", int{aVersion.major}, ".", int{aVersion.minor}, ".", int{aVersion.patch});

    aVersion = SDL_version{};
    SDL_IMAGE_VERSION(&aVersion);
    Log::print("SDL IMG compiled version : ", int{aVersion.major}, ".", int{aVersion.minor}, ".", int{aVersion.patch});

    aVersion = SDL_version{};
    aVersion = *(IMG_Linked_Version());
    Log::print("SDL IMG linked version : ", int{aVersion.major}, ".", int{aVersion.minor}, ".", int{aVersion.patch});

    aVersion = SDL_version{};
    SDL_TTF_VERSION(&aVersion);
    Log::print("SDL TTF compiled version : ", int{aVersion.major}, ".", int{aVersion.minor}, ".", int{aVersion.patch});

    aVersion = SDL_version{};
    aVersion = *(TTF_Linked_Version());
    Log::print("SDL TTF linked version : ", int{aVersion.major}, ".", int{aVersion.minor}, ".", int{aVersion.patch});

    std::exit(0);
}

// Whether the command line was parsed successfully or not
bool CommandLine::isValid() const
{
    return _valid;
}

bool CommandLine::printHelp() const
{
    return _printHelp;
}

bool CommandLine::printVersions() const
{
    return _printVersions;
}

bool CommandLine::disableSound() const
{
    return _disableSound;
}

bool CommandLine::escapeKeyQuits() const
{
    return _escapeKeyQuits;
}

Main::Log::Level CommandLine::minLogLevel() const
{
    return _minLogLevel;
}

bool CommandLine::randomSeedSet() const
{
    return _randomSeedSet;
}

std::uint_fast32_t CommandLine::randomSeed() const
{
    return _randomSeed;
}

const std::string& CommandLine::executableName() const
{
    return _executableName;
}

// Parse the command line options
bool CommandLine::parseOptions(int iArgc, char** iArgv)
{
    bool aValid{true};

    _executableName = iArgv[0];

    for (int aArg = 1; aArg < iArgc && aValid; aArg++)
    {
        if (kHelpOption.compare(iArgv[aArg]) == 0)
        {
            _printHelp = true;
        }
        else if (kVersionOption.compare(iArgv[aArg]) == 0)
        {
            _printVersions = true;
        }
        else if (kDisableSoundOption.compare(iArgv[aArg]) == 0)
        {
            Log::wrn("Disabling sound is not yet implemented");
            _disableSound = true;
        }
        else if (std::string{iArgv[aArg]}.find(kEscapeKeyOption) == std::string::size_type{0})
        {
            const std::string aOptionValue{&iArgv[aArg][kEscapeKeyOption.size()]};

            if (aOptionValue.compare("yes") == 0)
            {
                _escapeKeyQuits = true;
            }
            else if (aOptionValue.compare("no") == 0)
            {
                _escapeKeyQuits = false;
            }
            else
            {
                Log::err("Incorrect command line option: ", iArgv[aArg], '\n');
                aValid = false;
            }
        }
        else if (std::string{iArgv[aArg]}.find(kMinLogLevel) == std::string::size_type{0})
        {
            const std::string aOptionValue{&iArgv[aArg][kMinLogLevel.size()]};

            if (!Log::stringToLevel(aOptionValue, _minLogLevel))
            {
                Log::err("Incorrect command line option: ", iArgv[aArg], '\n');
                aValid = false;
            }

            if (!Log::setMinimumLogLevel(_minLogLevel))
            {
                Log::err("Cannot set the minimum log level: ", _minLogLevel, '\n');
                aValid = false;
            }
        }
        else if (std::string{iArgv[aArg]}.find(kRandomSeedOption) == std::string::size_type{0})
        {
            Log::wrn("Setting the random number seed is not yet implemented");
            const std::string aOptionValue{&iArgv[aArg][kRandomSeedOption.size()]};
            std::size_t aConvertedChars{};

            try
            {
                unsigned long aSeedTmp{std::stoul(aOptionValue, &aConvertedChars)};
                std::uint_fast32_t aSeed = static_cast<std::uint_fast32_t>(aSeedTmp);

                if (aConvertedChars != aOptionValue.size() || static_cast<unsigned long>(aSeed) != aSeedTmp)
                {
                    aValid = false;
                }
                else
                {
                    _randomSeedSet = true;
                    _randomSeed = aSeed;
                }
            }
            catch (...)
            {
                aValid = false;
            }

            if (!aValid)
            {
                Log::err("Incorrect command line option: ", iArgv[aArg], '\n');
            }
        }
        else
        {
            Log::err("Unknown command line option: ", iArgv[aArg], '\n');
            aValid = false;
        }
    }

    Log::trc(*this, '\n');
    return aValid;
}

// Print the CommandLine class
std::ostream& operator<<(std::ostream& ioOs, const CommandLine& iCommandLine)
{
    ioOs << "CommandLine {"
         << "\n  _valid: " << iCommandLine.isValid()
         << "\n  _printHelp: " << iCommandLine.printHelp()
         << "\n  _printVersions: " << iCommandLine.printVersions()
         << "\n  _disableSound: " << iCommandLine.disableSound()
         << "\n  _escapeKeyQuits: " << iCommandLine.escapeKeyQuits()
         << "\n  _minLogLevel: " << iCommandLine.minLogLevel()
         << "\n  _randomSeedSet: " << iCommandLine.randomSeedSet()
         << "\n  _randomSeed: " << iCommandLine.randomSeed()
         << "\n  _executableName: " << iCommandLine.executableName()
         << "\n}";
    return ioOs;
}

}   // namespace Options

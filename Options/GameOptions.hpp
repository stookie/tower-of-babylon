/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_OPTIONS_GAMEOPTIONS
#define TOB_OPTIONS_GAMEOPTIONS

#include <istream>
#include <ostream>


namespace Options
{

class GameOptions
{
public:
    GameOptions() = default;
    virtual ~GameOptions() = default;

    GameOptions(const GameOptions&) = default;
    GameOptions& operator=(const GameOptions&) = default;
    GameOptions(GameOptions&&) = delete;
    GameOptions& operator=(GameOptions&&) = delete;

    // Saving/loading to/from a stream
    virtual void save(std::ostream& ioStr) const = 0;
    virtual void load(std::istream& iStr) = 0;

    // Comparing two GameOptions instances
    virtual bool equal(const GameOptions& iGameOptions) const = 0;

    // Comparing the GameOptions instance to default values
    virtual bool isDefault() const = 0;

    // Copying from another GameOptions instances
    virtual void copy(const GameOptions& iGameOptions) = 0;

    // Resetting the GameOptions instance
    virtual void reset() = 0;
};

}   // namespace Options

#endif   // TOB_OPTIONS_GAMEOPTIONS

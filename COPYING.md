# COPYING

## Source Code
Authors:

* stookie (4303586-stookie@users.noreply.gitlab.com)

Copyright (C) 2021-2024 authors

All source code in this repository is available under the GNU Affero General Public License, version 3 only.

A copy of the license is provided in this repository as the file "LICENSE".

## SDL2 CMake Modules
Note that the cmake modules are not a part of the source code itself, and are not required to build the program.
Instead, they may be used as a convenience.

`cmake/FindSDL2.cmake`\
`cmake/FindSDL2_image.cmake`\
`cmake/FindSDL2_ttf.cmake`\
Original: https://github.com/Twinklebear/TwinklebearDev-Lessons; 19 January, 2015 - 11:09\
Author: Will Usher / Twinklebear; https://www.willusher.io\
License: MIT license; https://github.com/Twinklebear/TwinklebearDev-Lessons/blob/9de745276809c22681b21c9144fb613113b74e84/LICENSE.md\
Changes: None

## Binary Assets
The various binary assets (images etc) are available under difference licenses, and are listed here.

`resources/KaushanScript-Regular.otf`\
Original: https://tracker.debian.org/pkg/fonts-kaushanscript; Version 1.02-2.1; 08 January, 2021\
Secondary: https://fonts.google.com/specimen/Kaushan+Script/about\
Author: Pablo Impallari; https://www.impallari.com/\
License: SIL Open Font License, Version 1.1; https://openfontlicense.org/open-font-license-official-text\
Changes: None

`resources/diceblank_70.png`\
`resources/dice2_70.png`\
`resources/dice3_70.png`\
`resources/dice4_70.png`\
`resources/dice5_70.png`\
`resources/dice6_70.png`\
`resources/dice10_70.png`\
License: CC BY-SA 4.0; https://creativecommons.org/licenses/by-sa/4.0\
Changes: Combined one of the die images with the Babylonian numerals 2, 3, 4, 5, 6, 10;
 to create 6 dice images and a placeholder; 70x70 are the current expected dimensions.\
Original: https://opengameart.org/content/boardgame-pack; 22 May, 2014 - 06:44\
Secondary: https://www.kenney.nl/assets/boardgame-pack\
Author: Kenney; https://www.kenney.nl\
License:  CC0 1.0; http://creativecommons.org/publicdomain/zero/1.0\
Original: https://en.wikipedia.org/wiki/File:Babylonian_numerals.svg; 29 March, 2010 - 02:24\
Author: Josell7\
License: CC BY-SA 4.0; https://creativecommons.org/licenses/by-sa/4.0

`resources/felt.jpg`\
License: CC BY 3.0; https://creativecommons.org/licenses/by/3.0\
Changes: Reduced size to 400x400, shifted Hue to green, increased Chroma to make brighter.\
Original: https://opengameart.org/content/carpet-texture-red-seamless-texture-with-normalmap\
Author: Keith S / Keith333; https://opengameart.org/users/keith333\
License: CC BY 3.0; https://creativecommons.org/licenses/by/3.0

## Non-Included Binary Assets
The binary assets specified below need to be obtained separately for the moment,
until they are added to the repository and listed under Binary Assets above.


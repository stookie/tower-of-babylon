/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2023 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "Main/Dispatcher.hpp"
#include "Main/Exceptions.hpp"
#include "Main/Log.hpp"
#include "Options/CommandLine.hpp"
#include "Sdl/SdlTypes.hpp"

#include <cstdlib>


int main(int iArgc, char** iArgv)
{
    Options::CommandLine aCommandLine{iArgc, iArgv};

    if (!aCommandLine.isValid())
    {
        aCommandLine.doPrintUsage(true);
    }
    else if (aCommandLine.printHelp())
    {
        aCommandLine.doPrintUsage(false);
    }
    else if (aCommandLine.printVersions())
    {
        aCommandLine.doPrintVersions();
    }

    // The dispatcher object represents the backbone of execution
    try
    {
        // Transfer ownership of the command line to the dispatcher (and then the global context)
        Main::Dispatcher aDispatcher{std::move(aCommandLine)};

        aDispatcher.run();
    }
    catch (const SdlCtxException& e)
    {
        Main::Log::err("Unhandled SDL Context exception: ", e.what());
    }
    catch (const Main::RuntimeException& e)
    {
        Main::Log::err("Unhandled Game runtime exception: ", e.what());
    }
    catch (const Main::LogicException& e)
    {
        Main::Log::err("Unhandled Game logic exception: ", e.what());
    }
    catch (const std::runtime_error& e)
    {
        Main::Log::err("Unhandled runtime exception: ", e.what());
    }
    catch (const std::exception& e)
    {
        Main::Log::err("Unhandled std exception: ", e.what());
    }
    catch (...)
    {
        Main::Log::err("Unhandled unknown exception");
    }

    return 0;
}

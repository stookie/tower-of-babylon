/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2023 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_MAIN_LOG
#define TOB_MAIN_LOG

#include <iostream>
#include <string>
#include <type_traits>


namespace Main
{
namespace Log
{

enum class Level : unsigned char
{
    // Must use automatic numbering
    kTrace,
    kDebug,
    kInfo,
    kWarn,
    kError,
    kPlain,
    kNumLevels,   // must always be last
};

// The minimum log level at program startup
constexpr Level getDefaultMinimumLogLevel()
{
    return Level::kDebug;
}

// Internal class to provide static variable for the minimum log level
class LogLevelStorage
{
public:
    static void setMinimumLogLevel(const Level& iLevel);
    static const Level& getMinimumLogLevel();

private:
    static Level _minimumLogLevel;
};

inline const Level& LogLevelStorage::getMinimumLogLevel()
{
    return _minimumLogLevel;
}

}   // namespace Log
}   // namespace Main


// Unnamed namespace
namespace
{

using namespace Main::Log;

// Type alias of the underlying type of the enum
using Level_UT = typename std::underlying_type<Level>::type;

// Casting the number of levels to its underlying type is frequently used
constexpr Level_UT kNumLogLevels{static_cast<Level_UT>(Level::kNumLevels)};

// Logged output messages start with these titles
constexpr const char* const kTitle[kNumLogLevels]
{
    "TRACE: ",
    "DEBUG: ",
    "INFO: ",
    "WARN: ",
    "ERROR: ",
    "",
};

// The string representation of each log level
constexpr const char* const kStringRep[kNumLogLevels]
{
    "TRACE",
    "DEBUG",
    "INFO",
    "WARN",
    "ERROR",
    "PLAIN",
};

// The standard stream used to output each log level
std::ostream* const kOutStream[kNumLogLevels]
{
    &std::cout,
    &std::cout,
    &std::cout,
    &std::cerr,
    &std::cerr,
    &std::cout,
};

// The template-pack functions implementing the log output

template<typename H>
inline void LogImp(std::ostream* aStrm, const H& iH)
{
    *(aStrm) << iH << '\n';
}

template<typename H, typename... T>
inline void LogImp(std::ostream* aStrm, const H& iH, const T&... iT)
{
    *(aStrm) << iH;
    LogImp(aStrm, iT...);
}

template<typename... LST>
inline void LogImp(Level iLevel, const LST&... iLst)
{
    auto aLevel{static_cast<Level_UT>(iLevel)};

    if (aLevel >= static_cast<Level_UT>(LogLevelStorage::getMinimumLogLevel()))
    {
        *(kOutStream[aLevel]) << kTitle[aLevel];
        LogImp(kOutStream[aLevel], iLst...);
    }
}

}   // unnamed namespace


namespace Main
{
namespace Log
{

inline bool setMinimumLogLevel(Level iLevel)
{
    // Unsigned, so dont need to check its greater than zero
    if (static_cast<Level_UT>(iLevel) < kNumLogLevels)
    {
        LogLevelStorage::setMinimumLogLevel(iLevel);
        return true;
    }
    else
    {
        return false;
    }
}

template<typename... LST>
inline void trc(const LST&... iLst)
{
    LogImp(Level::kTrace, iLst...);
}

template<typename... LST>
inline void dbg(const LST&... iLst)
{
    LogImp(Level::kDebug, iLst...);
}

template<typename... LST>
inline void info(const LST&... iLst)
{
    LogImp(Level::kInfo, iLst...);
}

template<typename... LST>
inline void wrn(const LST&... iLst)
{
    LogImp(Level::kWarn, iLst...);
}

template<typename... LST>
inline void err(const LST&... iLst)
{
    LogImp(Level::kError, iLst...);
}

template<typename... LST>
inline void print(const LST&... iLst)
{
    LogImp(Level::kPlain, iLst...);
}

inline std::ostream& operator<<(std::ostream& ioOs, Level iLevel)
{
    switch (iLevel)
    {
        case Level::kTrace:
        case Level::kDebug:
        case Level::kInfo:
        case Level::kWarn:
        case Level::kError:
        case Level::kPlain:
            ioOs << kStringRep[static_cast<Level_UT>(iLevel)];
            break;
        case Level::kNumLevels:
            ioOs << "NUM_LEVELS";
            break;
    }

    return ioOs;
}

inline bool stringToLevel(const std::string& iStr, Level& oLevel)
{
    for (Level_UT aLevel = 0; aLevel < kNumLogLevels; aLevel++)
    {
        if (iStr.compare(kStringRep[aLevel]) == 0)
        {
            oLevel = static_cast<Level>(aLevel);
            return true;
        }
    }

    return false;
}

}   // namespace Log
}   // namespace Main

#endif   // TOB_MAIN_LOG

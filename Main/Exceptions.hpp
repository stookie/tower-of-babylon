/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_MAIN_EXCEPTIONS
#define TOB_MAIN_EXCEPTIONS

#include <stdexcept>


namespace Main
{

// The exception type used to indicate an error only detectable at runtime, related to more than just code.
// Used for external dependencies; like resources (images, fonts), option files, etc.
class RuntimeException : public std::runtime_error
{
    using std::runtime_error::runtime_error;
};

// The exception type used to indicate an unrecoverable error detected in the game code.
// Used when code detects null pointers, indices out of bounds for arrays and vectors, etc.
class LogicException : public std::logic_error
{
    using std::logic_error::logic_error;
};

}   // namespace Main

#endif   // TOB_MAIN_EXCEPTIONS

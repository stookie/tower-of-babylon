/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "Main/Dispatcher.hpp"

// General:
#include "Main/Constants.hpp"
#include "Main/Exceptions.hpp"
#include "Main/Log.hpp"
#include "Toolkit/GuiPage.hpp"
#include "Toolkit/GuiResources.hpp"

#include <memory>
#include <stack>
#include <vector>


namespace Main
{

Dispatcher::Dispatcher(Options::CommandLine&& iCommandLine) :
    _globalCtx{std::move(iCommandLine)}   // transfer ownership of the command line to the global context
{
}

void Dispatcher::run()
{
    _globalCtx.sdlCtx().setWindowSize(SdlDims{_globalCtx.settings().getWindowWidth(),
                                              _globalCtx.settings().getWindowHeight()});
    _globalCtx.sdlCtx().showWindow();

    // Pages are stacked on top of each other as requested
    std::stack<std::unique_ptr<Toolkit::GuiPage>> aPageStack;

    // The first page is the main page
    aPageStack.push(_globalCtx.resources().createFirstPage(_globalCtx));

    std::vector<SDL_Event> aAllEvents{};   // kept outside the loop to prevent constant memory allocation
    bool aQuit{false};

    Main::Log::info("Initialising the Vsync through SDL to ", _globalCtx._settings.getVsync());
    _globalCtx.sdlCtx().setVsync(_globalCtx._settings.getVsync());

    _globalCtx.reinitialiseTimingData();

    while (!aQuit)
    {
        if (_globalCtx._settings.hasVsyncChanged())
        {
            Main::Log::info("Updating the Vsync through SDL to ", _globalCtx._settings.getVsync(),
                            ", and re-initialising the timing data");
            _globalCtx.sdlCtx().setVsync(_globalCtx._settings.getVsync());
            _globalCtx.reinitialiseTimingData();
        }

        _globalCtx.beginIterationProcessing();

        _globalCtx.sdlCtx().setDrawColour(Main::kColourSolidBlack);
        _globalCtx.sdlCtx().renderStart();

        aAllEvents.clear();
        _globalCtx.sdlCtx().pollAllEvents(aAllEvents);

        for (const SDL_Event& aEvent : aAllEvents)
        {
            Main::Log::trc("Event of type: ", aEvent.type);

            if (aEvent.type == SDL_QUIT ||
                (aEvent.type == SDL_KEYDOWN && aEvent.key.keysym.sym == SDLK_ESCAPE &&
                 _globalCtx.commandLine().escapeKeyQuits()))
            {
                aQuit = true;
            }
        }

        aPageStack.top()->handleEvents(aAllEvents, _globalCtx);
        aPageStack.top()->draw(_globalCtx);

        if (aPageStack.top()->getPageState() == Toolkit::GuiPageState::kReturn)
        {
            aPageStack.top()->cleanUp(_globalCtx);
            aPageStack.pop();
        }
        else if (aPageStack.top()->getPageState() == Toolkit::GuiPageState::kSwapPage)
        {
            std::unique_ptr<Toolkit::GuiPage> aNewPage{aPageStack.top()->newPage(_globalCtx)};
            aPageStack.top()->cleanUp(_globalCtx);
            aNewPage.swap(aPageStack.top());

            if (aPageStack.top() == nullptr)
            {
                throw Main::LogicException{"Dispatcher::run Error: swapped-to GUI page is null"};
            }
        }
        else if (aPageStack.top()->getPageState() == Toolkit::GuiPageState::kNextPage)
        {
            aPageStack.push(aPageStack.top()->newPage(_globalCtx));

            if (aPageStack.top() == nullptr)
            {
                throw Main::LogicException{"Dispatcher::run Error: next GUI page is null"};
            }
        }

        if (aPageStack.empty())
        {
            aQuit = true;
        }

        _globalCtx.sdlCtx().renderEnd();

        _globalCtx.endIterationProcessing();
    }

    _globalCtx.logTimingData();
}

}   // namespace Main

/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2023 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "Main/Log.hpp"


namespace Main
{
namespace Log
{

Level LogLevelStorage::_minimumLogLevel = getDefaultMinimumLogLevel();

void LogLevelStorage::setMinimumLogLevel(const Level& iLevel)
{
    _minimumLogLevel = iLevel;
}

}   // namespace Log
}   // namespace Main

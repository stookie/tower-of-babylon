/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_MAIN_TIMEKEEPER
#define TOB_MAIN_TIMEKEEPER

#include "Main/Constants.hpp"

#include <chrono>
#include <utility>
#include <vector>


namespace Main
{

// This class is responsible for:
//  Controlling the delay to match the desired frame rate / iteration time
//  Keeping statistics regarding execution time, iteration time, overruns etc
//  Providing timing data access to other classes
class TimeKeeper
{
public:
    using SteadyTimePoint = std::chrono::steady_clock::time_point;
    using SteadyDuration = std::chrono::steady_clock::duration;

    // Throws Main::LogicException if frame rate is invalid
    TimeKeeper(double iFrameRate_Hz = kDefaultIterationFrameRate_Hz,
               std::size_t iStatisticsSlotsNumber = kTimingStatisticsSlotsNumber);

    ~TimeKeeper() = default;

    TimeKeeper(const TimeKeeper&) = delete;
    TimeKeeper& operator=(const TimeKeeper&) = delete;
    TimeKeeper(TimeKeeper&&) = delete;
    TimeKeeper& operator=(TimeKeeper&&) = delete;

    // Optionally re-initialise the timing data for the overall start of processing
    // Separate from the constructor to allow early object construction without commencing time handling
    // Never call between beginIterationProcessing() and endIterationProcessing()
    void reinitialiseTimingData();

    // Indicate a new iteration is about to begin
    // Call once before each endIterationProcessing()
    void beginIterationProcessing();

    // Indicate the current iteration has ended, and sleep to achieve the desired frame rate
    // Call once after each beginIterationProcessing()
    // Returns true if the iteration overran the allowed duration
    bool endIterationProcessing();

    // Log various statistics regarding the timing data
    // Call after endIterationProcessing()
    void logTimingData() const;

    // Information getters:

    // The desired duration of each iteration
    std::chrono::microseconds getIterationDuration_us() const;

    // The total number of iterations
    unsigned long int getIterations() const;

    // The total time since start of time keeping
    double getSecondsSinceStart_s() const;

    // Get the start time
    SteadyTimePoint getStartTime() const;

    // Get the current "now" time, actually that at the start of this iteration
    SteadyTimePoint getNowTime() const;

private:
    const std::size_t _statisticsSlotsNumber;   // for multiple stats, how many are retained
    std::chrono::microseconds _iterationDuration_us;   // the desired duration of each iteration
    SteadyTimePoint _startTimeKeepingTime;   // the start time of the time keeping functionality
    SteadyTimePoint _beginIterationTime;   // the time the current iteration began
    SteadyTimePoint _targetSleepTime;   // the end of iteration time (the time to sleep to)
    SteadyDuration _totalProcessingDuration;   // the total duration spent processing (not sleeping)
    SteadyDuration _totalOversleepDuration;   // the total duration spent sleeping longer than requested
    SteadyDuration _maximumOversleepDuration;   // the maximum single oversleep duration
    unsigned long int _iterations;   // the total number of iterations
    unsigned long int _sleptIterations;   // the number of iterations with a sleep (ie not overrun)
    unsigned long int _overruns;   // the number of overruns (ie an iteration without a sleep)
    std::vector<unsigned long int> _overrunIterationNumbers;   // a limited list of iterations with overruns

    // limited lists of the iterations with the shortest/longest processing durations:
    using DurationItVec = std::vector<std::pair<SteadyDuration, unsigned long int>>;
    DurationItVec _minimumProcessingDurations;
    DurationItVec _maximumProcessingDurations;
};

}   // namespace Main

#endif   // TOB_MAIN_TIMEKEEPER

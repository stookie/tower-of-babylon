/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "Main/GlobalContext.hpp"

#include "Main/Exceptions.hpp"
#include "Main/Log.hpp"

// Game type definition:
#include "GameType.hpp"

#include <fstream>


namespace Main
{

GlobalContext::GlobalContext(Options::CommandLine&& iCommandLine) :
    TimeKeeper{},   //XYZ HANDLE FRAME RATE PROPERLY
    _commandLine{std::move(iCommandLine)},
    _settings{},
    _defaultGameOptions{nullptr},
    _usedGameOptions{nullptr},
    _sdlCtx{},
    _resources{nullptr},
    _randomNumbers{}
{
    if (_commandLine.randomSeedSet())
    {
        _randomNumbers.setSeed(static_cast<Utils::RandomGenerator::SeedType>(_commandLine.randomSeed()));
    }
    else
    {
        _randomNumbers.setTimeBasedSeed();
    }

    _resources.reset(new Main::GameGuiResources{*this});

    if (_resources == nullptr)
    {
        throw Main::LogicException{"GlobalContext::GlobalContext Error: Main::GameGuiResources is null"};
    }

    // The default game options exist for the life of the program; the used game options are only created from the
    //  default game options when preparing a game.
    _defaultGameOptions = _resources->createGameOptions();

    if (_defaultGameOptions == nullptr)
    {
        throw Main::LogicException{"GlobalContext::GlobalContext Error: createGameOptions is null"};
    }

    loadOptions();
}

bool GlobalContext::saveOptions() const
{
    if (_sdlCtx.getPrefPath().empty())
    {
        Main::Log::wrn("Cannot access settings directory; cannot save options");
        return false;
    }

    const std::string aFilename = _sdlCtx.getPrefPath() + Main::kPrefDirApplicationName + "rc";
    std::ofstream aFileStr{aFilename, std::ios::out | std::ios::trunc};

    if (!aFileStr.good())
    {
        Main::Log::wrn("Cannot access settings file '", aFilename, "'; cannot save options");
        return false;
    }

    _settings.save(aFileStr);
    _defaultGameOptions->save(aFileStr);

    if (!aFileStr.good())
    {
        Main::Log::wrn("Cannot update settings file '", aFilename, "'; cannot save options");
        return false;
    }

    return true;
}

bool GlobalContext::loadOptions()
{
    if (!_sdlCtx.createPrefPath(Main::kPrefDirOrganisationName, Main::kPrefDirApplicationName))
    {
        Main::Log::wrn("Cannot access settings directory; cannot load options");
        return false;
    }

    const std::string aFilename = _sdlCtx.getPrefPath() + Main::kPrefDirApplicationName + "rc";
    std::ifstream aFileStr{aFilename, std::ios::in};

    if (!aFileStr.good())
    {
        Main::Log::wrn("Cannot access settings file '", aFilename, "'; cannot load options");
        return false;
    }

    Options::Settings aLocalSettings{};
    aLocalSettings.load(aFileStr);

    std::unique_ptr<Options::GameOptions> aLocalGameOptions = _resources->createGameOptions();
    aLocalGameOptions->load(aFileStr);

    if (!aFileStr.good())
    {
        Main::Log::wrn("Settings file '", aFilename, "' has an incorrect format; cannot load options");
        return false;
    }

    _settings.copy(aLocalSettings);
    _defaultGameOptions->copy(*aLocalGameOptions);

    return true;
}

// Copy the game options used for a game from the default game options
void GlobalContext::setUsedGameOptionsFromDefault()
{
    _usedGameOptions = _resources->createGameOptions();

    if (_usedGameOptions == nullptr)
    {
        throw Main::LogicException{"GlobalContext::setUsedGameOptionsFromDefault Error: createGameOptions is null"};
    }

    _usedGameOptions->copy(*_defaultGameOptions);
}

// Release the used game options; intended for when a game is complete, a minor memory optimisation
void GlobalContext::unsetUsedGameOptions()
{
    _usedGameOptions.reset(nullptr);
}

const Options::CommandLine& GlobalContext::commandLine() const
{
    return _commandLine;
}

const Options::Settings& GlobalContext::settings() const
{
    return _settings;
}

const Options::GameOptions& GlobalContext::defaultGameOptions() const
{
    return *_defaultGameOptions;
}

const Options::GameOptions& GlobalContext::usedGameOptions() const
{
    if (_usedGameOptions == nullptr)
    {
        throw Main::LogicException{"GlobalContext::usedGameOptions Error: used game options is null"};
    }

    return *_usedGameOptions;
}

const SdlContext& GlobalContext::sdlCtx() const
{
    return _sdlCtx;
}

const Toolkit::GuiResources& GlobalContext::resources() const
{
    return *_resources;
}

const Utils::RandomGenerator& GlobalContext::randomNumbers() const
{
    return _randomNumbers;
}

Options::Settings& GlobalContext::settings()
{
    return _settings;
}

Options::GameOptions& GlobalContext::defaultGameOptions()
{
    return *_defaultGameOptions;
}

Options::GameOptions& GlobalContext::usedGameOptions()
{
    if (_usedGameOptions == nullptr)
    {
        throw Main::LogicException{"GlobalContext::usedGameOptions Error: used game options is null"};
    }

    return *_usedGameOptions;
}

SdlContext& GlobalContext::sdlCtx()
{
    return _sdlCtx;
}

Toolkit::GuiResources& GlobalContext::resources()
{
    return *_resources;
}

Utils::RandomGenerator& GlobalContext::randomNumbers()
{
    return _randomNumbers;
}

}   // namespace Main

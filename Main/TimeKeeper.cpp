/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "Main/TimeKeeper.hpp"

#include "Main/Exceptions.hpp"
#include "Main/Log.hpp"

#include <algorithm>
#include <sstream>
#include <thread>
#include <functional>


namespace Main
{

TimeKeeper::TimeKeeper(double iFrameRate_Hz, std::size_t iStatisticsSlotsNumber) :
    _statisticsSlotsNumber{iStatisticsSlotsNumber},
    _iterationDuration_us{},
    _startTimeKeepingTime{},
    _beginIterationTime{},
    _targetSleepTime{},
    _totalProcessingDuration{SteadyDuration{0}},
    _totalOversleepDuration{SteadyDuration{0}},
    _maximumOversleepDuration{SteadyDuration{0}},
    _iterations{0},
    _sleptIterations{0},
    _overruns{0},
    _overrunIterationNumbers{},
    _minimumProcessingDurations{},
    _maximumProcessingDurations{}
{
    if (iFrameRate_Hz <= 1.0)
    {
        throw Main::LogicException{std::string{"Main::TimeKeeper Error: invalid frame rate: "} +
                                   std::to_string(iFrameRate_Hz)};
    }

    _iterationDuration_us = std::chrono::microseconds{static_cast<int>(1000000.0 / iFrameRate_Hz)};
    _overrunIterationNumbers.reserve(_statisticsSlotsNumber);
    _minimumProcessingDurations.reserve(_statisticsSlotsNumber);
    _maximumProcessingDurations.reserve(_statisticsSlotsNumber);

    reinitialiseTimingData();
}

// Optionally re-initialise the timing data for the overall start of processing
// Separate from the constructor to allow early object construction without commencing time handling
// It is up to the caller to never call between beginIterationProcessing() and endIterationProcessing()
void TimeKeeper::reinitialiseTimingData()
{
    _startTimeKeepingTime = std::chrono::steady_clock::now();
    _beginIterationTime = _startTimeKeepingTime;
    _targetSleepTime = _startTimeKeepingTime;
    _totalProcessingDuration = SteadyDuration{0};
    _totalOversleepDuration = SteadyDuration{0};
    _maximumOversleepDuration = SteadyDuration{0};
    _iterations = 0;
    _sleptIterations = 0;
    _overruns = 0;
    _overrunIterationNumbers.clear();
    _minimumProcessingDurations.clear();
    _maximumProcessingDurations.clear();
}

// Indicate a new iteration is about to begin
// Call once before each endIterationProcessing()
void TimeKeeper::beginIterationProcessing()
{
    _beginIterationTime = std::chrono::steady_clock::now();
    _iterations++;
}

// Indicate the current iteration has ended, and sleep to achieve the desired frame rate
// Call once after each beginIterationProcessing()
// Returns true if the iteration overran the allowed duration
bool TimeKeeper::endIterationProcessing()
{
    // Calculate processing duration
    SteadyTimePoint aNowTime = std::chrono::steady_clock::now();
    SteadyDuration aProcessingDuration = (aNowTime - _beginIterationTime);
    _totalProcessingDuration += aProcessingDuration;

    // Maintain the minimum/maximum processing duration lists
    DurationItVec::value_type aProcDurationIteration = std::make_pair(aProcessingDuration, _iterations);
    std::less<DurationItVec::value_type> aLT{};
    std::greater<DurationItVec::value_type> aGT{};

    if (_minimumProcessingDurations.size() < _statisticsSlotsNumber)
    {
        _minimumProcessingDurations.push_back(aProcDurationIteration);
        std::stable_sort(_minimumProcessingDurations.begin(), _minimumProcessingDurations.end(), aLT);
    }
    else if (aLT(aProcDurationIteration, _minimumProcessingDurations.back()))
    {
        _minimumProcessingDurations.pop_back();
        _minimumProcessingDurations.push_back(aProcDurationIteration);
        std::stable_sort(_minimumProcessingDurations.begin(), _minimumProcessingDurations.end(), aLT);
    }

    if (_maximumProcessingDurations.size() < _statisticsSlotsNumber)
    {
        _maximumProcessingDurations.push_back(aProcDurationIteration);
        std::stable_sort(_maximumProcessingDurations.begin(), _maximumProcessingDurations.end(), aGT);
    }
    else if (aGT(aProcDurationIteration, _maximumProcessingDurations.back()))
    {
        _maximumProcessingDurations.pop_back();
        _maximumProcessingDurations.push_back(aProcDurationIteration);
        std::stable_sort(_maximumProcessingDurations.begin(), _maximumProcessingDurations.end(), aGT);
    }

    // Either sleep or overrun
    _targetSleepTime += _iterationDuration_us;
    aNowTime = std::chrono::steady_clock::now();   // to be as accurate as possible

    if (_targetSleepTime < aNowTime)
    {
        _overruns++;
        _targetSleepTime = aNowTime;   // overrun, dont sleep, dont try to make up the lost time

        if (_overrunIterationNumbers.size() < _statisticsSlotsNumber)
        {
            _overrunIterationNumbers.push_back(_iterations);
        }

        Main::Log::info("DispatchTimer: overrun on iteration = ", _iterations, ", processing time = ",
                        aProcessingDuration.count());

        return true;
    }
    else
    {
        SteadyDuration aSleepDuration = _targetSleepTime - aNowTime;

        Main::Log::trc("DispatchTimer: processing time = ", aProcessingDuration.count(),
                       ", sleep duration = ", aSleepDuration.count());

        std::this_thread::sleep_for(aSleepDuration);

        _sleptIterations++;
        aSleepDuration = (std::chrono::steady_clock::now() - _targetSleepTime);   // actual "now" rather than planned
        _totalOversleepDuration += aSleepDuration;

        if (_maximumOversleepDuration < aSleepDuration)
        {
            _maximumOversleepDuration = aSleepDuration;
        }

        return false;
    }
}

// Log various statistics regarding the timing data
// Call after endIterationProcessing()
void TimeKeeper::logTimingData() const
{
    std::chrono::duration<double> aTimeDiff = std::chrono::steady_clock::now() - _startTimeKeepingTime;
    Main::Log::info("Timing stats:");
    Main::Log::info("Total time = ", aTimeDiff.count());
    Main::Log::info("Iterations = ", _iterations);
    Main::Log::info("Overruns = ", _overruns);

    std::stringstream aSS{};
    for (auto aIt : _overrunIterationNumbers)
    {
        aSS << aIt << ", ";
    }
    Main::Log::info("Overrun iterations (first ", _statisticsSlotsNumber, ") = ", aSS.str());

    Main::Log::info("Average frame time = ", (1000.0 * aTimeDiff.count() / _iterations), " milliseconds");
    Main::Log::info("Average frame processing time = ",
                    (1000.0 * std::chrono::duration<double>{_totalProcessingDuration}.count() / _iterations),
                    " milliseconds");

    aSS = std::stringstream{};
    for (const auto& aIt : _maximumProcessingDurations)
    {
        aSS << 1000.0 * std::chrono::duration<double>{aIt.first}.count() << "@" << aIt.second << ", ";
    }
    Main::Log::info("Highest processing time iterations (highest ", _statisticsSlotsNumber, ") = ", aSS.str());

    aSS = std::stringstream{};
    for (const auto& aIt : _minimumProcessingDurations)
    {
        aSS << 1000.0 * std::chrono::duration<double>{aIt.first}.count() << "@" << aIt.second << ", ";
    }
    Main::Log::info("Lowest processing time iterations (lowest ", _statisticsSlotsNumber, ") = ", aSS.str());

    Main::Log::info("Iterations with a sleep = ", _sleptIterations);
    Main::Log::info("Maximum over-sleep time = ",
                    (1000.0 * std::chrono::duration<double>{_maximumOversleepDuration}.count()),
                    " milliseconds");
    Main::Log::info("Total over-sleep time = ",
                    (1000.0 * std::chrono::duration<double>{_totalOversleepDuration}.count()),
                    " milliseconds");

    if (_sleptIterations > 0)
    {
        Main::Log::info("Average over-sleep time = ",
                        (1000.0 * std::chrono::duration<double>{_totalOversleepDuration}.count() / _sleptIterations),
                        " milliseconds");
    }
}

// The desired duration of each iteration
std::chrono::microseconds TimeKeeper::getIterationDuration_us() const
{
    return _iterationDuration_us;
}

// The total number of iterations
unsigned long int TimeKeeper::getIterations() const
{
    return _iterations;
}

// The total time since start of time keeping
double TimeKeeper::getSecondsSinceStart_s() const
{
    // Base the time since the start on the beginning of the iteration processing, for consistency
    auto aDelta = _beginIterationTime - _startTimeKeepingTime;
    return (std::chrono::duration_cast<std::chrono::microseconds>(aDelta)).count() / 1000000.0;
}

// Get the start time
TimeKeeper::SteadyTimePoint TimeKeeper::getStartTime() const
{
    return _startTimeKeepingTime;
}

// Get the current "now" time, actually that at the start of this iteration
TimeKeeper::SteadyTimePoint TimeKeeper::getNowTime() const
{
    // Base the "now" time on the beginning of the iteration processing, for consistency
    return _beginIterationTime;
}

}   // namespace Main

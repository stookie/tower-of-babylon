/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2023 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_MAIN_DISPATCHER
#define TOB_MAIN_DISPATCHER

#include "Main/GlobalContext.hpp"


namespace Main
{

// This class is the real main, the backbone of the process execution
class Dispatcher
{
public:
    Dispatcher(Options::CommandLine&& iCommandLine);
    ~Dispatcher() = default;

    Dispatcher(const Dispatcher&) = delete;
    Dispatcher& operator=(const Dispatcher&) = delete;
    Dispatcher(Dispatcher&&) = delete;
    Dispatcher& operator=(Dispatcher&&) = delete;

    // Run the dispatcher; throws Main::LogicException if next page or swap page is invalid (null pointer)
    void run();

private:
    GlobalContext _globalCtx;
};

}   // namespace Main

#endif   // TOB_MAIN_DISPATCHER

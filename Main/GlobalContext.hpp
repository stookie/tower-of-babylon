/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_MAIN_GLOBAL_CONTEXT
#define TOB_MAIN_GLOBAL_CONTEXT

#include "Main/TimeKeeper.hpp"
#include "Options/CommandLine.hpp"
#include "Options/GameOptions.hpp"
#include "Options/Settings.hpp"
#include "Sdl/SdlContext.hpp"
#include "Toolkit/GuiResources.hpp"
#include "Utils/RandomGenerator.hpp"

#include <memory>


namespace Main
{

class GlobalContext : private TimeKeeper
{
public:
    // Throws Main::LogicException if Main::GameGuiResources cannot be constructed, or
    //  Main::GameGuiResources::createGameOptions() returns nullptr
    GlobalContext(Options::CommandLine&& iCommandLine);
    ~GlobalContext() = default;

    GlobalContext(const GlobalContext&) = delete;
    GlobalContext& operator=(const GlobalContext&) = delete;
    GlobalContext(GlobalContext&&) = delete;
    GlobalContext& operator=(GlobalContext&&) = delete;

    // Save/load the Settings
    bool saveOptions() const;
    bool loadOptions();

    // Copy the game options used for a game from the default game options;
    //  throws Main::LogicException if the GameOptions cannot be constructed.
    void setUsedGameOptionsFromDefault();

    // Release the used game options; intended for when a game is complete, a minor memory optimisation
    void unsetUsedGameOptions();

    const Options::CommandLine& commandLine() const;
    const Options::Settings& settings() const;
    const Options::GameOptions& defaultGameOptions() const;
    const Options::GameOptions& usedGameOptions() const;   //  throws Main::LogicException if used Game Options is null
    const SdlContext& sdlCtx() const;
    const Toolkit::GuiResources& resources() const;
    const Utils::RandomGenerator& randomNumbers() const;

    //XYZ CONSIDER MAKING PRIVATE AND USING FRIENDS, FOR SETTINGS ETC. OR REVISIT NEED FOR NON_CONST VERSIONS
    Options::Settings& settings();
    Options::GameOptions& defaultGameOptions();
    Options::GameOptions& usedGameOptions();   //  throws Main::LogicException if used Game Options is null
    SdlContext& sdlCtx();
    Toolkit::GuiResources& resources();
    Utils::RandomGenerator& randomNumbers();

    // Include the TimeKeeper type aliases and safe methods to the public scope
    using TimeKeeper::SteadyTimePoint;
    using TimeKeeper::SteadyDuration;
    using TimeKeeper::getIterationDuration_us;
    using TimeKeeper::getIterations;
    using TimeKeeper::getSecondsSinceStart_s;
    using TimeKeeper::getStartTime;
    using TimeKeeper::getNowTime;

private:
    const Options::CommandLine _commandLine;
    Options::Settings _settings;
    std::unique_ptr<Options::GameOptions> _defaultGameOptions;
    std::unique_ptr<Options::GameOptions> _usedGameOptions;
    SdlContext _sdlCtx;
    std::unique_ptr<Toolkit::GuiResources> _resources;
    Utils::RandomGenerator _randomNumbers;

    // Include the non-safe TimeKeeper methods to the private scope, and provide friend access to Dispatcher
    using TimeKeeper::reinitialiseTimingData;
    using TimeKeeper::beginIterationProcessing;
    using TimeKeeper::endIterationProcessing;
    using TimeKeeper::logTimingData;
    friend class Dispatcher;
};

}   // namespace Main

#endif   // TOB_MAIN_GLOBAL_CONTEXT

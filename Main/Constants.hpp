/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_MAIN_CONSTANTS
#define TOB_MAIN_CONSTANTS

#include <cstddef>
#include <cstdint>


// Constants not related to a specific game


namespace Main
{

// The "organisation" name used in constructing the preference directory
static const char kPrefDirOrganisationName[] = "toborg";

// The default escape key behaviour
constexpr bool kDefaultEscapeKeyOption{true};

// The default iteration framerate if not specified programatically (Hz)
constexpr double kDefaultIterationFrameRate_Hz = 60.0;

// The number of timing statistics stored, for eg overrun iterations, etc
constexpr std::size_t kTimingStatisticsSlotsNumber{20};

// The initial Vsync setting and window dimensions
constexpr bool kInitialVsync{false};
constexpr int kInitialWindowWidth{800};
constexpr int kInitialWindowHeight{800};

// Some colours
constexpr uint32_t kColourSolidBlack{0x000000ff};
constexpr uint32_t kColourSolidWhite{0xffffffff};
constexpr uint32_t kColourSolidRed{0xff0000ff};
constexpr uint32_t kColourSolidGreen{0x00ff00ff};
constexpr uint32_t kColourSolidBlue{0x0000ffff};
constexpr uint8_t kOpacity25_pc{25};
constexpr uint8_t kOpacity50_pc{50};
constexpr uint8_t kOpacity75_pc{75};

}   // namespace Main

#endif   // TOB_MAIN_CONSTANTS

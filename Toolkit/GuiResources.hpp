/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2023 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_TOOLKIT_GUIRESOURCES
#define TOB_TOOLKIT_GUIRESOURCES

#include <memory>

class SdlFont;

namespace Main
{
class GlobalContext;
}


namespace Options
{
class GameOptions;
}

namespace Toolkit
{
class GuiPage;
}


namespace Toolkit
{

class GuiResources
{
public:
    GuiResources() = default;
    virtual ~GuiResources() = default;

    // Create an instance of the game options object
    virtual std::unique_ptr<Options::GameOptions> createGameOptions() const = 0;

    // Create the first page
    virtual std::unique_ptr<Toolkit::GuiPage> createFirstPage(Main::GlobalContext& iCtx) const = 0;

    // Access fonts
    virtual std::shared_ptr<SdlFont> getLargeFont() const = 0;
    virtual std::shared_ptr<SdlFont> getNormalFont() const = 0;
    virtual std::shared_ptr<SdlFont> getSmallFont() const = 0;

    //XYZ ADD TIME INFO, TO SUPPORT TIMERS, DELAYS ETC
};

}   // namespace Toolkit

#endif   // TOB_TOOLKIT_GUIRESOURCES

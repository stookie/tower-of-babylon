/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_TOOLKIT_GUIPAGE
#define TOB_TOOLKIT_GUIPAGE

#include <memory>
#include <vector>

union SDL_Event;

namespace Main
{
class GlobalContext;
}


namespace Toolkit
{

enum class GuiPageState : unsigned char
{
    kActivePage,
    kReturn,
    kNextPage,
    kSwapPage,
};

class GuiPage
{
public:
    GuiPage();
    virtual ~GuiPage() = default;

    virtual void handleEvents(const std::vector<SDL_Event>& iEvents, Main::GlobalContext& iCtx) = 0;

    virtual void draw(Main::GlobalContext& iCtx) = 0;

    // The new page when the next page or a page swap is needed; default implemention does nothing
    virtual std::unique_ptr<Toolkit::GuiPage> newPage(Main::GlobalContext& iCtx);

    virtual GuiPageState getPageState() const;

    // Allow cleaning up based on the Global Context when a page is to be destroyed
    virtual void cleanUp(Main::GlobalContext& iCtx);

protected:
    virtual bool setPageState(GuiPageState iPageState);

private:
    GuiPageState _pageState;
};


inline GuiPage::GuiPage() :
    _pageState{GuiPageState::kActivePage}
{
}

inline std::unique_ptr<Toolkit::GuiPage> GuiPage::newPage(Main::GlobalContext& iCtx)
{
    return nullptr;
}

inline GuiPageState GuiPage::getPageState() const
{
    return _pageState;
}

inline bool GuiPage::setPageState(GuiPageState iPageState)
{
    switch (iPageState)
    {
        case GuiPageState::kActivePage:
        case GuiPageState::kReturn:
        case GuiPageState::kNextPage:
        case GuiPageState::kSwapPage:
            _pageState = iPageState;
            return true;
            break;
        default:
            return false;
            break;
    }
}

inline void GuiPage::cleanUp(Main::GlobalContext& iCtx)
{
    // no default functionality, but can be overridden
}

}   // namespace Toolkit

#endif   // TOB_TOOLKIT_GUIPAGE

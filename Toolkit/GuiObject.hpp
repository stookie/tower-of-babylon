/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2022 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_TOOLKIT_GUIOBJECT
#define TOB_TOOLKIT_GUIOBJECT

#include <vector>

class SdlContext;
union SDL_Event;


namespace Toolkit
{

enum class GuiObjectState : unsigned char
{
    kHidden,
    kInactive,
    kActive,
};

class GuiObject
{
public:
    GuiObject() = default;
    virtual ~GuiObject() = default;

    // Copy and move semantics are provided
    GuiObject(const GuiObject&) = default;
    GuiObject& operator=(const GuiObject&) = default;
    GuiObject(GuiObject&&) = default;
    GuiObject& operator=(GuiObject&&) = default;

    // Use a dedicated empty-class tag to indicate cloning and sharing of resources
    class CloneShareTag
    {};

    // Clone & share construction; outcome is not self-evident regarding resource sharing, so use a separate constructor
    GuiObject(CloneShareTag, const GuiObject&)
    {
        // Nothing to clone or share for this purely abstract class
    }

    // Set/get the state of the object to one of Hidden, Inactive, Active
    virtual bool setState(GuiObjectState iState) = 0;
    virtual GuiObjectState getState() const = 0;

    // All GUI Objects must handle events, even display-only ones, for example responding to mouse motion
    virtual void handleEvents(const std::vector<SDL_Event>& iEvents) = 0;

    // Draw the GUI Object
    virtual void draw(SdlContext& iSdlCtx) = 0;
};

}   // namespace Toolkit

#endif   // TOB_TOOLKIT_GUIOBJECT

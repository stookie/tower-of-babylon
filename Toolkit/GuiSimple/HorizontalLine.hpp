/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2023 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_TOOLKIT_GUISIMPLE_HORZ_LINE
#define TOB_TOOLKIT_GUISIMPLE_HORZ_LINE

#include "Sdl/SdlTypes.hpp"
#include "Toolkit/GuiObject.hpp"

#include <vector>

class SdlContext;
union SDL_Event;


namespace Toolkit
{
namespace GuiSimple
{

class HorizontalLine : virtual public GuiObject
{
public:
    HorizontalLine();
    virtual ~HorizontalLine() override;

    // Copy and move semantics are provided
    HorizontalLine(const HorizontalLine&) = default;
    HorizontalLine& operator=(const HorizontalLine&) = default;
    HorizontalLine(HorizontalLine&&) = default;
    HorizontalLine& operator=(HorizontalLine&&) = default;

    // Set/get the state of the label to one of Hidden, Inactive, Active
    virtual bool setState(GuiObjectState iState) override;
    virtual GuiObjectState getState() const override;

    virtual void handleEvents(const std::vector<SDL_Event>& iEvents) override;

    virtual void draw(SdlContext& iSdlCtx) override;

    virtual void setPosition(SdlCoords iCoordinates);
    virtual SdlCoords getPosition() const;

    virtual void setWidth(int iWidth);
    virtual int getWidth() const;

    virtual bool setThickness(int iThickness);
    virtual int getThickness() const;

    // Note this is relative to the current drawing colour opacity
    virtual void setInactiveOpacity(uint8_t iOpacity_pc);

private:
    SdlCoords _coordinates;
    SdlDims _dimensions;   // width and thickness
    GuiObjectState _state;
    double _opacity;
};

inline void HorizontalLine::handleEvents(const std::vector<SDL_Event>& iEvents)
{
    // No response to any events
}

}   // namespace GuiSimple
}   // namespace Toolkit

#endif   // TOB_TOOLKIT_GUISIMPLE_HORZ_LINE

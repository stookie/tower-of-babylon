/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2023 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_TOOLKIT_GUISIMPLE_BUTTON
#define TOB_TOOLKIT_GUISIMPLE_BUTTON

#include "Sdl/SdlTypes.hpp"
#include "Toolkit/GuiObject.hpp"

#include <vector>

class SdlContext;
union SDL_Event;


namespace Toolkit
{
namespace GuiSimple
{

class Button : virtual public GuiObject
{
public:
    Button();
    virtual ~Button() override;

    // Copy and move semantics are provided
    Button(const Button&) = default;
    Button& operator=(const Button&) = default;
    Button(Button&&) = default;
    Button& operator=(Button&&) = default;

    // Clone & share construction; outcome is not self-evident regarding resource sharing, so use a separate constructor
    Button(CloneShareTag iCloneShareTag, const Button& iButton);

    // Copy properties & share resources; note the position is retained and interim state is not copied
    void copyAndShare(const Button& iButton);

    // Set/get the state of the button to one of Hidden, Inactive, Active
    virtual bool setState(GuiObjectState iState) override;
    virtual GuiObjectState getState() const override;

    virtual void handleEvents(const std::vector<SDL_Event>& iEvents) override;

    virtual void draw(SdlContext& iSdlCtx) override;

    virtual void setPosition(SdlCoords iCoordinates);
    virtual SdlCoords getPosition() const;

    virtual void setSize(SdlDims iDimensions);
    virtual SdlDims getSize() const;

    // Determine if the button was clicked after the events have been handled
    virtual bool wasClicked() const;

private:
    SdlCoords _coordinates;
    SdlDims _dimensions;
    GuiObjectState _state;
    bool _clickStarted;
    bool _clicked;
};

}   // namespace GuiSimple
}   // namespace Toolkit

#endif   // TOB_TOOLKIT_GUISIMPLE_BUTTON

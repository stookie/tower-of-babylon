/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "Toolkit/GuiSimple/TextButton.hpp"

#include "Main/Constants.hpp"
#include "Main/Exceptions.hpp"

#include <algorithm>


namespace Toolkit
{
namespace GuiSimple
{

TextButton::TextButton(SdlContext& iSdlCtx, const std::shared_ptr<SdlFont>& iFont, const std::string& iText) :
    Button{},
    _text{iSdlCtx.createText(iFont, iText)},
    _opacity{}
{
    if (iFont == nullptr)
    {
        throw Main::LogicException{"TextButton::TextButton Error: font is null"};
    }

    setSize(_text->getDimensions());
    setInactiveOpacity(Main::kOpacity50_pc);
}

TextButton::~TextButton()
{
}

// Clone & share construction; outcome is not self-evident regarding resource sharing, so use a separate constructor
TextButton::TextButton(CloneShareTag iCloneShareTag, const TextButton& iTextButton) :
    Button(iCloneShareTag, iTextButton),
    _text{iTextButton._text},
    _opacity{iTextButton._opacity}
{
}

// Copy properties & share resources; note the position is retained and interim state is not copied
void TextButton::copyAndShare(const TextButton& iTextButton)
{
    Button::copyAndShare(iTextButton);

    if (_text != iTextButton._text)
    {
        _text = iTextButton._text;
    }

    _opacity = iTextButton._opacity;
}

void TextButton::draw(SdlContext& iSdlCtx)
{
    if (getState() == GuiObjectState::kHidden)
    {
        // Dont draw button while hidden
        return;
    }

    // If inactive, reduce the alpha channel to indicate it
    SDL_Colour aOrigColour{};
    if (getState() == GuiObjectState::kInactive)
    {
        aOrigColour = iSdlCtx.getDrawColour();
        iSdlCtx.setDrawColour(aOrigColour.r, aOrigColour.g, aOrigColour.b, std::lround(aOrigColour.a * _opacity));
    }

    iSdlCtx.drawRectangle(getPosition(), getSize(), false);

    // The text is offset halfway between the set size of this button, and the size of the smaller(?) text texture
    SdlCoords aTextPosition = getPosition();
    aTextPosition._x += (getSize()._w - _text->getDimensions()._w) / 2;
    aTextPosition._y += (getSize()._h - _text->getDimensions()._h) / 2;

    iSdlCtx.blitText(aTextPosition, _text);

    if (getState() == GuiObjectState::kInactive)
    {
        iSdlCtx.setDrawColour(aOrigColour);
    }
}

// Set the size, but do not set the size lower than the minimum size
void TextButton::setSize(SdlDims iDimensions)
{
    SdlDims aNewDimensions = _text->getDimensions();   // initialise to the minimised size
    aNewDimensions._w = std::max(aNewDimensions._w, iDimensions._w);
    aNewDimensions._h = std::max(aNewDimensions._h, iDimensions._h);
    Button::setSize(aNewDimensions);
}

// Set new text for the button, optionally retaining the previous size if possible
// Returns true if the text actually changed; false otherwise
bool TextButton::setText(SdlContext& iSdlCtx, const std::string& iText, bool iRetainSize)
{
    SdlDims aOldDimensions = getSize();
    bool aUpdated = iSdlCtx.updateText(iText, _text);

    if (aUpdated)
    {
        if (iRetainSize)
        {
            // If too small, then the minimum size protection in setSize() applies
            setSize(aOldDimensions);
        }
        else
        {
            setSize(_text->getDimensions());
        }
    }

    return aUpdated;
}

// Set the opacity level used if the button is inactive
// Note this is relative to the current drawing colour opacity
void TextButton::setInactiveOpacity(uint8_t iOpacity_pc)
{
    _opacity = std::min(std::max(iOpacity_pc, uint8_t{0}), uint8_t{100}) / 100.0;
}

}   // namespace GuiSimple
}   // namespace Toolkit

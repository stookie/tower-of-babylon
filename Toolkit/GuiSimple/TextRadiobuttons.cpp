/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "Toolkit/GuiSimple/TextRadiobuttons.hpp"

#include "Main/Exceptions.hpp"
#include "Main/Log.hpp"

#include <string>


namespace
{
const std::string kSelectedText{"0"};
const std::string kUnselectedText{" "};
}


namespace Toolkit
{
namespace GuiSimple
{

TextRadiobuttons::TextRadiobuttons(std::size_t iCount, SdlContext& iSdlCtx, const std::shared_ptr<SdlFont>& iFont) :
    Radiobuttons{iCount},
    _selectedTextButton{iSdlCtx, iFont, kSelectedText},
    _unselectedTextButton{iSdlCtx, iFont, kUnselectedText},
    _textButtons{},
    _selectedChanged{false},
    _clicked{false}
{
    if (iFont == nullptr)
    {
        throw Main::LogicException{"TextRadiobuttons::TextRadiobuttons Error: font is null"};
    }
    else if (iCount == 0)
    {
        throw Main::LogicException{"TextRadiobuttons::TextRadiobuttons Error: count of buttons is zero"};
    }

    // iCount checked in RadioButtons()

    // Buttons are ensured square, and have a valid minimum size
    setButtonSize(SdlDims{});

    _textButtons.reserve(iCount);

    for (std::size_t aCount = 0; aCount < iCount; aCount++)
    {
        _textButtons.emplace_back(CloneShareTag{}, _unselectedTextButton);
    }
}

TextRadiobuttons::~TextRadiobuttons()
{
}

// Clone & share construction; outcome is not self-evident regarding resource sharing, so use a separate constructor
TextRadiobuttons::TextRadiobuttons(CloneShareTag iCloneShareTag, const TextRadiobuttons& iTextRadiobuttons) :
    Radiobuttons{iCloneShareTag, iTextRadiobuttons},
    _selectedTextButton{iCloneShareTag, iTextRadiobuttons._selectedTextButton},
    _unselectedTextButton{iCloneShareTag, iTextRadiobuttons._unselectedTextButton},
    _textButtons{},
    _selectedChanged{false},   // dont clone intermediate state
    _clicked{false}   // dont clone intermediate state
{
    _textButtons.reserve(iTextRadiobuttons.getCount());

    for (std::size_t aCount = 0; aCount < iTextRadiobuttons.getCount(); aCount++)
    {
        if (iTextRadiobuttons.isSelected() && (aCount == iTextRadiobuttons.getSelected()))
        {
            _textButtons.emplace_back(CloneShareTag{}, _selectedTextButton);
        }
        else
        {
            _textButtons.emplace_back(CloneShareTag{}, _unselectedTextButton);
        }
    }
}

// Copy properties & share resources; note the position is retained and interim state is not copied
void TextRadiobuttons::copyAndShare(const TextRadiobuttons& iTextRadiobuttons)
{
    const bool aIsSelected = isSelected();
    const std::size_t aSelected = getSelected();

    Radiobuttons::copyAndShare(iTextRadiobuttons);
    _selectedTextButton.copyAndShare(iTextRadiobuttons._selectedTextButton);
    _unselectedTextButton.copyAndShare(iTextRadiobuttons._unselectedTextButton);
    _selectedChanged = (aIsSelected != isSelected()) || (isSelected() && (aSelected != getSelected()));
    // _clicked is unchanged

    _textButtons.clear();
    _textButtons.reserve(iTextRadiobuttons.getCount());

    for (std::size_t aCount = 0; aCount < iTextRadiobuttons.getCount(); aCount++)
    {
        if (iTextRadiobuttons.isSelected() && (aCount == iTextRadiobuttons.getSelected()))
        {
            _textButtons.emplace_back(CloneShareTag{}, _selectedTextButton);
        }
        else
        {
            _textButtons.emplace_back(CloneShareTag{}, _unselectedTextButton);
        }
    }
}

bool TextRadiobuttons::setState(GuiObjectState iState)
{
    bool aOk{true};

    for (auto& aTextButton : _textButtons)
    {
        aOk = aTextButton.setState(iState) && aOk;
    }

    return aOk;
}

GuiObjectState TextRadiobuttons::getState() const
{
    // All text buttons have the same state, and size is guaranteed to be 1 or more
    return _textButtons.at(0).getState();
}

void TextRadiobuttons::handleEvents(const std::vector<SDL_Event>& iEvents)
{
    _clicked = false;

    for (std::size_t aCount = 0; aCount < getCount(); aCount++)
    {
        auto& aTextButton = _textButtons.at(aCount);
        aTextButton.handleEvents(iEvents);

        if (aTextButton.wasClicked() && aTextButton.getState() == GuiObjectState::kActive)
        {
            _clicked = true;
            _selectedChanged = setSelected(aCount) || _selectedChanged;
        }
    }
}

void TextRadiobuttons::draw(SdlContext& iSdlCtx)
{
    if (_selectedChanged)
    {
        _selectedChanged = false;
    }

    for (auto& aTextButton : _textButtons)
    {
        aTextButton.draw(iSdlCtx);
    }
}

// Determine if the checkbox was clicked after the events have been handled
bool TextRadiobuttons::wasClicked() const
{
    return _clicked;
}

// Change the selected state, regardless of the radio buttons state (ie even when hidden or inactive)
// Returns true if the selected state changes
bool TextRadiobuttons::setUnselected()
{
    const bool aIsSelected = isSelected();
    const std::size_t aSelected = getSelected();
    const bool aChanged = Radiobuttons::setUnselected();

    if (aChanged && aIsSelected)
    {
        _textButtons.at(aSelected).copyAndShare(_unselectedTextButton);
    }

    return aChanged;
}

bool TextRadiobuttons::setSelected(std::size_t iCount)
{
    // iCount checked in RadioButtons::setSelected()

    const bool aIsSelected = isSelected();
    const std::size_t aSelected = getSelected();
    const bool aChanged = Radiobuttons::setSelected(iCount);

    if (aChanged)
    {
        _textButtons.at(getSelected()).copyAndShare(_selectedTextButton);

        if (aIsSelected)
        {
            _textButtons.at(aSelected).copyAndShare(_unselectedTextButton);
        }
    }

    return aChanged;
}

// Set/get the individual button positions
// Setting a position may change the position and size of the bounding box containing all individual buttons
void TextRadiobuttons::setButtonPosition(std::size_t iCount, SdlCoords iCoordinates)
{
    checkCount(iCount);

    _textButtons.at(iCount).setPosition(iCoordinates);
}

SdlCoords TextRadiobuttons::getButtonPosition(std::size_t iCount) const
{
    checkCount(iCount);

    return _textButtons.at(iCount).getPosition();
}

// Set/get the individual button positions, relative to the bounding box containing all individual buttons
// Setting an offset may change the position and size of the bounding box containing all individual buttons
void TextRadiobuttons::setButtonOffset(std::size_t iCount, SdlDims iOffsets)
{
    checkCount(iCount);

    SdlCoords aCoords = getPosition();
    _textButtons.at(iCount).setPosition(SdlCoords{aCoords._x + iOffsets._w, aCoords._y + iOffsets._h});
}

SdlDims TextRadiobuttons::getButtonOffset(std::size_t iCount) const
{
    checkCount(iCount);

    SdlCoords aCoordsButton = _textButtons.at(iCount).getPosition();
    SdlCoords aCoords = getPosition();

    return SdlDims{aCoordsButton._x - aCoords._x, aCoordsButton._y - aCoords._y};
}

// Set/get the position of the bounding box containing all individual buttons
void TextRadiobuttons::setPosition(SdlCoords iCoordinates)
{
    SdlCoords aCoords = getPosition();
    SdlDims aOffset{iCoordinates._x - aCoords._x, iCoordinates._y - aCoords._y};

    for (std::size_t aCount = 0; aCount < getCount(); aCount++)
    {
        setButtonOffset(aCount, aOffset);
    }
}

SdlCoords TextRadiobuttons::getPosition() const
{
    int aX{};
    int aY{};
    bool aFirst{true};

    for (const auto& aTextButton : _textButtons)
    {
        SdlCoords aCoords = aTextButton.getPosition();

        if (aFirst)
        {
            aFirst = false;
            aX = aCoords._x;
            aY = aCoords._y;
        }
        else
        {
            if (aCoords._x < aX)
            {
                aX = aCoords._x;
            }

            if (aCoords._y < aY)
            {
                aY = aCoords._y;
            }
        }
    }

    return SdlCoords{aX, aY};
}

// Get the size of the bounding box containing all individual buttons
SdlDims TextRadiobuttons::getSize() const
{
    int aX1{};
    int aY1{};
    int aX2{};
    int aY2{};
    bool aFirst{true};

    for (const auto& aTextButton : _textButtons)
    {
        SdlCoords aCoords = aTextButton.getPosition();
        SdlDims aDims = aTextButton.getSize();

        if (aFirst)
        {
            aFirst = false;
            aX1 = aCoords._x;
            aY1 = aCoords._y;
            aX2 = aCoords._x + aDims._w;
            aY2 = aCoords._y + aDims._h;
        }
        else
        {
            if (aCoords._x < aX1)
            {
                aX1 = aCoords._x;
            }

            if (aCoords._y < aY1)
            {
                aY1 = aCoords._y;
            }

            if (aCoords._x + aDims._w > aX2)
            {
                aX2 = aCoords._x + aDims._w;
            }

            if (aCoords._y + aDims._h < aY2)
            {
                aY2 = aCoords._y + aDims._h;
            }
        }
    }

    return SdlDims{aX2 - aX1, aY2 - aY1};
}

// Get the size of any individual button; its the same whether selected or not
SdlDims TextRadiobuttons::getButtonSize() const
{
    return _selectedTextButton.getSize();
}

// Set the opacity level used if the radio buttons are inactive
// Note this is relative to the current drawing colour opacity
void TextRadiobuttons::setInactiveOpacity(uint8_t iOpacity_pc)
{
    _selectedTextButton.setInactiveOpacity(iOpacity_pc);
    _unselectedTextButton.setInactiveOpacity(iOpacity_pc);

    for (auto& aTextButton : _textButtons)
    {
        aTextButton.setInactiveOpacity(iOpacity_pc);
    }
}

// Setting the size has the same limitations as the TextButton; ie does not set the size lower than the minimum size
void TextRadiobuttons::setButtonSize(SdlDims iDimensions)
{
    _selectedTextButton.setSize(iDimensions);
    _unselectedTextButton.setSize(iDimensions);

    // Ensure the buttons are square in dimensions, the same size for both the selected and unselected text buttons
    int aSquareDim = std::max(_selectedTextButton.getSize()._w, _selectedTextButton.getSize()._h);
    aSquareDim = std::max(aSquareDim, _unselectedTextButton.getSize()._w);
    aSquareDim = std::max(aSquareDim, _unselectedTextButton.getSize()._h);

    SdlDims aDimensions{aSquareDim, aSquareDim};
    _selectedTextButton.setSize(aDimensions);
    _unselectedTextButton.setSize(aDimensions);

    for (auto& aButton : _textButtons)
    {
        aButton.setSize(aDimensions);
    }
}

}   // namespace GuiSimple
}   // namespace Toolkit

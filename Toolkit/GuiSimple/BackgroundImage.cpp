/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "Toolkit/GuiSimple/BackgroundImage.hpp"

#include "Main/Exceptions.hpp"
#include "Main/Log.hpp"
#include "Sdl/SdlContext.hpp"


namespace Toolkit
{
namespace GuiSimple
{

BackgroundImage::BackgroundImage(const std::shared_ptr<SdlTexture>& iTexture, BackgroundType iBackgroundType) :
    _texture{iTexture},
    _backgroundType{iBackgroundType},
    _origin{}
{
    if (iTexture == nullptr)
    {
        throw Main::LogicException{"BackgroundImage::BackgroundImage Error: texture is null"};
    }
}

SdlCoords BackgroundImage::getOrigin() const
{
    return _origin;
}

void BackgroundImage::setOrigin(SdlCoords iOrigin)
{
    if (_backgroundType == BackgroundType::kSingleFull)
    {
        throw Main::LogicException{"BackgroundImage::setOrigin Error: origin cant be specified for full window image"};
    }

    _origin = iOrigin;
}

void BackgroundImage::draw(SdlContext& iSdlCtx) const
{
    switch (_backgroundType)
    {
        case BackgroundType::kSingleAsIs:
            drawOnce(iSdlCtx, _origin);
            break;
        case BackgroundType::kSingleScaled:
        case BackgroundType::kSingleFull:
            throw Main::LogicException{"BackgroundImage::draw Error: this type of background image not implemented"};
            break;
        case BackgroundType::kTiled:
            drawTiled(iSdlCtx, _origin);
            break;
        default:
            throw Main::LogicException{"BackgroundImage::draw Error: unknown background image type: " +
                                       std::to_string(static_cast<int>(_backgroundType))};
            break;
    }
}

// Draw the image once, unscaled, at the specified origin
void BackgroundImage::drawOnce(SdlContext& iSdlCtx, SdlCoords iOrigin) const
{
    iSdlCtx.blit(_texture, iOrigin);
}

// Draw the image tiled, unscaled, with one of the drawings at the specified origin
void BackgroundImage::drawTiled(SdlContext& iSdlCtx, SdlCoords iOrigin) const
{
    //    pos = offset % image
    //    if pos > 0
    //        pos -= image

    iOrigin._x %= _texture->getDimensions()._w;
    int aOriginY = iOrigin._y % _texture->getDimensions()._h;

    if (iOrigin._x > 0)
    {
        iOrigin._x -= _texture->getDimensions()._w;
    }

    if (aOriginY > 0)
    {
        aOriginY -= _texture->getDimensions()._h;
    }

    while (iOrigin._x < iSdlCtx.getWindowSize()._w)
    {
        iOrigin._y = aOriginY;

        while (iOrigin._y < iSdlCtx.getWindowSize()._h)
        {
            iSdlCtx.blit(_texture, iOrigin);
            iOrigin._y += _texture->getDimensions()._h;
        }

        iOrigin._x += _texture->getDimensions()._w;
    }
}

}   // namespace GuiSimple
}   // namespace Toolkit

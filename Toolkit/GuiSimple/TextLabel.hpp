/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_TOOLKIT_GUISIMPLE_TEXTLABEL
#define TOB_TOOLKIT_GUISIMPLE_TEXTLABEL

#include "Sdl/SdlContext.hpp"
#include "Sdl/SdlFont.hpp"
#include "Toolkit/GuiSimple/Label.hpp"

#include <memory>
#include <string>


namespace Toolkit
{
namespace GuiSimple
{

class TextLabel : virtual public Label
{
public:
    // Throws Main::LogicException if font is nullptr
    TextLabel(SdlContext& iSdlCtx, const std::shared_ptr<SdlFont>& iFont, const std::string& iText);
    virtual ~TextLabel() override;

    // Copy semantics are not allowed
    TextLabel(const TextLabel&) = delete;
    TextLabel& operator=(const TextLabel&) = delete;

    // Move semantics are provided
    TextLabel(TextLabel&&) = default;
    TextLabel& operator=(TextLabel&&) = default;

    // Clone & share construction; outcome is not self-evident regarding resource sharing, so use a separate constructor
    TextLabel(CloneShareTag iCloneShareTag, const TextLabel& iTextLabel);

    // Copy properties & share resources; note the position is retained and interim state is not copied
    void copyAndShare(const TextLabel& iTextLabel);

    // setState() not overridden
    // getState() not overridden

    // All GUI Objects must handle events, even display-only ones, for example responding to mouse motion
    virtual void handleEvents(const std::vector<SDL_Event>& iEvents) override;

    // Draw the GUI Object
    virtual void draw(SdlContext& iSdlCtx) override;

    // setPosition() not overridden
    // getPosition() not overridden

    // Set the size, but do not set the size lower than the minimum size
    virtual void setSize(SdlDims iDimensions) override;
    virtual SdlDims getSize() const override;

    // Set new text for the label, optionally retaining the previous size if possible
    // Returns true if the text actually changed; false otherwise
    virtual bool setText(SdlContext& iSdlCtx, const std::string& iText, bool iRetainSize);

    // Set the opacity level used if the label is inactive
    // Note this is relative to the current drawing colour opacity
    virtual void setInactiveOpacity(uint8_t iOpacity_pc);

private:
    SdlDims _dimensions;
    std::shared_ptr<SdlText> _text;
    double _opacity;
};

inline void TextLabel::handleEvents(const std::vector<SDL_Event>& iEvents)
{
}

}   // namespace GuiSimple
}   // namespace Toolkit

#endif   // TOB_TOOLKIT_GUISIMPLE_TEXTLABEL

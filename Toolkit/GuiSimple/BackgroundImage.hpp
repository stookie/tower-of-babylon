/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_TOOLKIT_GUISIMPLE_BACKGROUNDIMAGE
#define TOB_TOOLKIT_GUISIMPLE_BACKGROUNDIMAGE

#include "Sdl/SdlTexture.hpp"
#include "Sdl/SdlTypes.hpp"

#include <memory>

class SdlContext;


namespace Toolkit
{
namespace GuiSimple
{

enum class BackgroundType : unsigned char
{
    kSingleAsIs,   // displayed once, no scaling, image origin as specified
    kSingleFull,   // displayed once, scaled to fit window
    kSingleScaled,   // displayed once, scaled as specified, image origin as specified
    kTiled,   // window filled with repeated tiling, no scaling, one image origin as specified
};

// Note: unlike other classes in this directory, BackgroundImage is not subclassed from GuiObject.
// It is also not polymorphic.
class BackgroundImage
{
public:
    // Throws Main::LogicException if texture is nullptr
    BackgroundImage(const std::shared_ptr<SdlTexture>& iTexture, BackgroundType iBackgroundType);
    ~BackgroundImage() = default;

    // Copy semantics are not allowed
    BackgroundImage(const BackgroundImage&) = delete;
    BackgroundImage& operator=(const BackgroundImage&) = delete;

    // Move semantics are not allowed
    BackgroundImage(BackgroundImage&&) = delete;
    BackgroundImage& operator=(BackgroundImage&&) = delete;

    SdlCoords getOrigin() const;
    void setOrigin(SdlCoords iOrigin);

    void draw(SdlContext& iSdlCtx) const;

private:
    // Draw the image once, unscaled, at the specified origin
    void drawOnce(SdlContext& iSdlCtx, SdlCoords iOrigin) const;

    // Draw the image tiled, unscaled, with one of the drawings at the specified origin
    void drawTiled(SdlContext& iSdlCtx, SdlCoords iOrigin) const;

    std::shared_ptr<SdlTexture> _texture;
    BackgroundType _backgroundType;
    SdlCoords _origin;
};

}   // namespace GuiSimple
}   // namespace Toolkit

#endif   // TOB_TOOLKIT_GUISIMPLE_BACKGROUNDIMAGE

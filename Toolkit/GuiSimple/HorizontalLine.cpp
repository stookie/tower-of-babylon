/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2023 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "Toolkit/GuiSimple/HorizontalLine.hpp"

#include "Main/Constants.hpp"
#include "Sdl/SdlContext.hpp"


namespace Toolkit
{
namespace GuiSimple
{

HorizontalLine::HorizontalLine() :
    _coordinates{},
    _dimensions{},
    _state{GuiObjectState::kActive},
    _opacity{}
{
    setInactiveOpacity(Main::kOpacity25_pc);
}

HorizontalLine::~HorizontalLine()
{
}

// Set/get the state of the line to one of Hidden, Inactive, Active
bool HorizontalLine::setState(GuiObjectState iState)
{
    switch (iState)
    {
        case GuiObjectState::kHidden:
        case GuiObjectState::kInactive:
        case GuiObjectState::kActive:
            _state = iState;
            return true;
            break;
        default:
            return false;
            break;
    }
}

GuiObjectState HorizontalLine::getState() const
{
    return _state;
}

void HorizontalLine::draw(SdlContext& iSdlCtx)
{
    if (_state == GuiObjectState::kHidden)
    {
        // Dont draw label while hidden
        return;
    }

    // If inactive, reduce the alpha channel to indicate it
    SDL_Colour aOrigColour{};
    if (getState() == GuiObjectState::kInactive)
    {
        aOrigColour = iSdlCtx.getDrawColour();
        iSdlCtx.setDrawColour(aOrigColour.r, aOrigColour.g, aOrigColour.b, std::lround(aOrigColour.a * _opacity));
    }

    iSdlCtx.drawRectangle(getPosition(), _dimensions);

    if (getState() == GuiObjectState::kInactive)
    {
        iSdlCtx.setDrawColour(aOrigColour);
    }
}

void HorizontalLine::setPosition(SdlCoords iCoordinates)
{
    _coordinates = iCoordinates;
}

SdlCoords HorizontalLine::getPosition() const
{
    return _coordinates;
}

void HorizontalLine::setWidth(int iWidth)
{
    _dimensions._w = iWidth;
}

int HorizontalLine::getWidth() const
{
    return _dimensions._w;
}

bool HorizontalLine::setThickness(int iThickness)
{
    if (iThickness > 0)
    {
        _dimensions._h = iThickness;
        return true;
    }
    else
    {
        return false;
    }
}

int HorizontalLine::getThickness() const
{
    return _dimensions._h;
}

// Note this is relative to the current drawing colour opacity
void HorizontalLine::setInactiveOpacity(uint8_t iOpacity_pc)
{
    _opacity = std::min(std::max(iOpacity_pc, uint8_t{0}), uint8_t{100}) / 100.0;
}

}   // namespace GuiSimple
}   // namespace Toolkit

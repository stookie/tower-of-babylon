/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2023 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "Toolkit/GuiSimple/VerticalLine.hpp"

#include "Main/Constants.hpp"
#include "Sdl/SdlContext.hpp"


namespace Toolkit
{
namespace GuiSimple
{

VerticalLine::VerticalLine() :
    _coordinates{},
    _dimensions{},
    _state{GuiObjectState::kActive},
    _opacity{}
{
    setInactiveOpacity(Main::kOpacity25_pc);
}

VerticalLine::~VerticalLine()
{
}

// Set/get the state of the line to one of Hidden, Inactive, Active
bool VerticalLine::setState(GuiObjectState iState)
{
    switch (iState)
    {
        case GuiObjectState::kHidden:
        case GuiObjectState::kInactive:
        case GuiObjectState::kActive:
            _state = iState;
            return true;
            break;
        default:
            return false;
            break;
    }
}

GuiObjectState VerticalLine::getState() const
{
    return _state;
}

void VerticalLine::draw(SdlContext& iSdlCtx)
{
    if (_state == GuiObjectState::kHidden)
    {
        // Dont draw label while hidden
        return;
    }

    // If inactive, reduce the alpha channel to indicate it
    SDL_Colour aOrigColour{};
    if (getState() == GuiObjectState::kInactive)
    {
        aOrigColour = iSdlCtx.getDrawColour();
        iSdlCtx.setDrawColour(aOrigColour.r, aOrigColour.g, aOrigColour.b, std::lround(aOrigColour.a * _opacity));
    }

    iSdlCtx.drawRectangle(getPosition(), _dimensions);

    if (getState() == GuiObjectState::kInactive)
    {
        iSdlCtx.setDrawColour(aOrigColour);
    }
}

void VerticalLine::setPosition(SdlCoords iCoordinates)
{
    _coordinates = iCoordinates;
}

SdlCoords VerticalLine::getPosition() const
{
    return _coordinates;
}

void VerticalLine::setHeight(int iHeight)
{
    _dimensions._h = iHeight;
}

int VerticalLine::getHeight() const
{
    return _dimensions._h;
}

bool VerticalLine::setThickness(int iThickness)
{
    if (iThickness > 0)
    {
        _dimensions._w = iThickness;
        return true;
    }
    else
    {
        return false;
    }
}

int VerticalLine::getThickness() const
{
    return _dimensions._w;
}

// Note this is relative to the current drawing colour opacity
void VerticalLine::setInactiveOpacity(uint8_t iOpacity_pc)
{
    _opacity = std::min(std::max(iOpacity_pc, uint8_t{0}), uint8_t{100}) / 100.0;
}

}   // namespace GuiSimple
}   // namespace Toolkit

/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_TOOLKIT_GUISIMPLE_TEXTRADIOBUTTONS
#define TOB_TOOLKIT_GUISIMPLE_TEXTRADIOBUTTONS

#include "Sdl/SdlContext.hpp"
#include "Sdl/SdlFont.hpp"
#include "Toolkit/GuiSimple/Radiobuttons.hpp"
#include "Toolkit/GuiSimple/TextButton.hpp"

#include <memory>
#include <vector>


namespace Toolkit
{
namespace GuiSimple
{

class TextRadiobuttons : virtual public Radiobuttons
{
public:
    // Throws Main::LogicException if button number is zero or font is nullptr
    TextRadiobuttons(std::size_t iCount, SdlContext& iSdlCtx, const std::shared_ptr<SdlFont>& iFont);
    virtual ~TextRadiobuttons() override;

    TextRadiobuttons(const TextRadiobuttons&) = delete;
    TextRadiobuttons& operator=(const TextRadiobuttons&) = delete;
    TextRadiobuttons(TextRadiobuttons&& iTextRadiobuttons) = delete;
    TextRadiobuttons& operator=(TextRadiobuttons&&) = delete;

    // Clone & share construction; outcome is not self-evident regarding resource sharing, so use a separate constructor
    TextRadiobuttons(CloneShareTag iCloneShareTag, const TextRadiobuttons& iTextRadiobuttons);

    // Copy properties & share resources; note the position is retained and interim state is not copied
    void copyAndShare(const TextRadiobuttons& iTextRadiobuttons);

    // Set the state of the checkbox to one of Hidden, Inactive, Active
    virtual bool setState(GuiObjectState iState) override;
    virtual GuiObjectState getState() const override;

    virtual void handleEvents(const std::vector<SDL_Event>& iEvents) override;

    virtual void draw(SdlContext& iSdlCtx) override;

    // getCount() not overridden

    // Determine if the checkbox was clicked after the events have been handled
    virtual bool wasClicked() const override;

    // Change the selected state, regardless of the radio buttons state (ie even when hidden or inactive)
    // Returns true if the selected state changes
    virtual bool setUnselected() override;
    virtual bool setSelected(std::size_t iCount) override;

    // isSelected() not overridden
    // getSelected() not overridden

    // Set/get the individual button positions
    // Setting a position may change the position and size of the bounding box containing all individual buttons
    // Throws Main::LogicException if button number is too large
    virtual void setButtonPosition(std::size_t iCount, SdlCoords iCoordinates) override;
    virtual SdlCoords getButtonPosition(std::size_t iCount) const override;

    // Set/get the individual button positions, relative to the bounding box containing all individual buttons
    // Setting an offset may change the position and size of the bounding box containing all individual buttons
    // Throws Main::LogicException if button number is too large
    virtual void setButtonOffset(std::size_t iCount, SdlDims iOffsets) override;
    virtual SdlDims getButtonOffset(std::size_t iCount) const override;

    // Set/get the position of the bounding box containing all individual buttons
    virtual void setPosition(SdlCoords iCoordinates) override;
    virtual SdlCoords getPosition() const override;

    // Get the size of the bounding box containing all individual buttons
    virtual SdlDims getSize() const override;

    // Get the size of any individual button; its the same whether selected or not
    virtual SdlDims getButtonSize() const override;

    // Set the opacity level used if the radio buttons are inactive
    // Note this is relative to the current drawing colour opacity
    virtual void setInactiveOpacity(uint8_t iOpacity_pc);

protected:
    // Setting the size has the same limitations as the TextButton; ie does not set the size lower than the minimum size
    virtual void setButtonSize(SdlDims iDimensions);

    // checkCount() not overridden

private:
    TextButton _selectedTextButton;
    TextButton _unselectedTextButton;
    std::vector<TextButton> _textButtons;
    bool _selectedChanged;   // for efficiency, set to true when the selected state has changed (reset by draw)
    bool _clicked;
};

}   // namespace GuiSimple
}   // namespace Toolkit

#endif   // TOB_TOOLKIT_GUISIMPLE_TEXTRADIOBUTTONS

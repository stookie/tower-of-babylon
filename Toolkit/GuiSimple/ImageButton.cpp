/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "Toolkit/GuiSimple/ImageButton.hpp"

#include "Main/Constants.hpp"
#include "Main/Exceptions.hpp"

#include <algorithm>


namespace Toolkit
{
namespace GuiSimple
{

ImageButton::ImageButton(SdlContext& iSdlCtx, const std::shared_ptr<SdlTexture>& iTexture, bool iDrawBorder) :
    Button{},
    _texture{iTexture},
    _opacity{},
    _originalTextureOpacity{},
    _updateTextureOpacity{},
    _drawBorder{iDrawBorder}
{
    if (iTexture == nullptr)
    {
        throw Main::LogicException{"ImageButton::ImageButton Error: texture is null"};
    }

    setSize(_texture->getDimensions());
    setInactiveOpacity(Main::kOpacity50_pc);
    _originalTextureOpacity = iSdlCtx.getTextureAlpha(iTexture);
}

ImageButton::~ImageButton()
{
}

// Clone & share construction; outcome is not self-evident regarding resource sharing, so use a separate constructor
ImageButton::ImageButton(CloneShareTag iCloneShareTag, const ImageButton& iImageButton) :
    Button(iCloneShareTag, iImageButton),
    _texture{iImageButton._texture},
    _opacity{iImageButton._opacity},
    _originalTextureOpacity{iImageButton._originalTextureOpacity},
    _updateTextureOpacity{iImageButton._updateTextureOpacity},
    _drawBorder{iImageButton._drawBorder}
{
}

// Copy properties & share resources; note the position is retained and interim state is not copied
void ImageButton::copyAndShare(const ImageButton& iImageButton)
{
    Button::copyAndShare(iImageButton);

    if (_texture != iImageButton._texture)
    {
        _texture = iImageButton._texture;
    }

    _opacity = iImageButton._opacity;
    _originalTextureOpacity = iImageButton._originalTextureOpacity;
    _updateTextureOpacity = iImageButton._updateTextureOpacity;
    _drawBorder = iImageButton._drawBorder;
}

bool ImageButton::setState(GuiObjectState iState)
{
    _updateTextureOpacity = (iState == GuiObjectState::kInactive || iState == GuiObjectState::kActive);
    return Button::setState(iState);
}

void ImageButton::draw(SdlContext& iSdlCtx)
{
    if (getState() == GuiObjectState::kHidden)
    {
        // Dont draw button while hidden
        return;
    }

    if (_updateTextureOpacity)
    {
        _updateTextureOpacity = false;

        if (getState() == GuiObjectState::kInactive)
        {
            iSdlCtx.setTextureAlpha(_texture, std::lround(_originalTextureOpacity * _opacity));
        }
        else if (getState() == GuiObjectState::kActive)
        {
            iSdlCtx.setTextureAlpha(_texture, _originalTextureOpacity);
        }
    }

    // If inactive, reduce the alpha channel to indicate it
    SDL_Colour aOrigColour{};

    if (_drawBorder)
    {
        if (getState() == GuiObjectState::kInactive)
        {
            aOrigColour = iSdlCtx.getDrawColour();
            iSdlCtx.setDrawColour(aOrigColour.r, aOrigColour.g, aOrigColour.b, std::lround(aOrigColour.a * _opacity));
        }

        iSdlCtx.drawRectangle(getPosition(), getSize(), false);
    }

    // The texture is offset halfway between the set size of this button, and the size of the smaller(?) actual texture
    SdlCoords aTexturePosition = getPosition();
    aTexturePosition._x += (getSize()._w - _texture->getDimensions()._w) / 2;
    aTexturePosition._y += (getSize()._h - _texture->getDimensions()._h) / 2;

    iSdlCtx.blit(_texture, aTexturePosition);

    if (_drawBorder && getState() == GuiObjectState::kInactive)
    {
        iSdlCtx.setDrawColour(aOrigColour);
    }
}

// Set the size, but do not set the size lower than the minimum size
void ImageButton::setSize(SdlDims iDimensions)
{
    SdlDims aNewDimensions = _texture->getDimensions();   // initialise to the minimised size

    if (_drawBorder)
    {
        // Allow 1 pixel on each side for a border
        aNewDimensions._w += 2;
        aNewDimensions._h += 2;
    }

    aNewDimensions._w = std::max(aNewDimensions._w, iDimensions._w);
    aNewDimensions._h = std::max(aNewDimensions._h, iDimensions._h);
    Button::setSize(aNewDimensions);
}

// Set the opacity level used if the button is inactive
// Note this is relative to the current drawing colour opacity
void ImageButton::setInactiveOpacity(uint8_t iOpacity_pc)
{
    _opacity = std::min(std::max(iOpacity_pc, uint8_t{0}), uint8_t{100}) / 100.0;

    if (getState() == GuiObjectState::kInactive)
    {
        _updateTextureOpacity = true;
    }
}

}   // namespace GuiSimple
}   // namespace Toolkit

/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2023 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_TOOLKIT_GUISIMPLE_RADIOBUTTONS
#define TOB_TOOLKIT_GUISIMPLE_RADIOBUTTONS

#include "Sdl/SdlTypes.hpp"
#include "Toolkit/GuiObject.hpp"


namespace Toolkit
{
namespace GuiSimple
{

class Radiobuttons : virtual public GuiObject
{
public:
    Radiobuttons(std::size_t iCount);   // throws Main::LogicException if button number is zero
    virtual ~Radiobuttons() override;

    // Copy and move semantics are provided
    Radiobuttons(const Radiobuttons&) = default;
    Radiobuttons& operator=(const Radiobuttons&) = default;
    Radiobuttons(Radiobuttons&&) = default;
    Radiobuttons& operator=(Radiobuttons&&) = default;

    // Clone & share construction; outcome is not self-evident regarding resource sharing, so use a separate constructor
    Radiobuttons(CloneShareTag iCloneShareTag, const Radiobuttons& iRadiobuttons);

    // Copy properties & share resources; note the position is retained and interim state is not copied
    void copyAndShare(const Radiobuttons& iRadiobuttons);

    // setState() not implemented
    // getState() not implemented

    // handleEvents() not implemented

    // draw() not implemented

    virtual std::size_t getCount() const;

    // Determine if the checkbox was clicked after the events have been handled
    virtual bool wasClicked() const = 0;

    // Change the selected state, regardless of the radio buttons state (ie even when hidden or inactive)
    // Returns true if the selected state changes
    virtual bool setUnselected();
    virtual bool setSelected(std::size_t iCount);   // throws Main::LogicException if button number is too large

    virtual bool isSelected() const;
    virtual std::size_t getSelected() const;

    // Set/get the individual button positions
    // Setting a position may change the position and size of the bounding box containing all individual buttons
    virtual void setButtonPosition(std::size_t iCount, SdlCoords iCoordinates) = 0;
    virtual SdlCoords getButtonPosition(std::size_t iCount) const = 0;

    // Set/get the individual button positions, relative to the bounding box containing all individual buttons
    // Setting an offset may change the position and size of the bounding box containing all individual buttons
    virtual void setButtonOffset(std::size_t iCount, SdlDims iOffsets) = 0;
    virtual SdlDims getButtonOffset(std::size_t iCount) const = 0;

    // Set/get the position of the bounding box containing all individual buttons
    virtual void setPosition(SdlCoords iCoordinates) = 0;
    virtual SdlCoords getPosition() const = 0;

    // Get the size of the bounding box containing all individual buttons
    virtual SdlDims getSize() const = 0;

    // Get the size of any individual button; its the same whether selected or not
    virtual SdlDims getButtonSize() const = 0;

protected:
    // If count is invalid, throws Main::LogicException
    void checkCount(std::size_t iCount) const;

private:
    std::size_t _count;
    std::size_t _selected;
    bool _isSelected;
};

}   // namespace GuiSimple
}   // namespace Toolkit

#endif   // TOB_TOOLKIT_GUISIMPLE_RADIOBUTTONS

/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_TOOLKIT_GUISIMPLE_TEXTCHECKBOX
#define TOB_TOOLKIT_GUISIMPLE_TEXTCHECKBOX

#include "Sdl/SdlContext.hpp"
#include "Sdl/SdlFont.hpp"
#include "Toolkit/GuiSimple/Checkbox.hpp"
#include "Toolkit/GuiSimple/TextButton.hpp"

#include <memory>
#include <vector>


namespace Toolkit
{
namespace GuiSimple
{

class TextCheckbox : virtual public Checkbox
{
public:
    // Throws Main::LogicException if font is nullptr
    TextCheckbox(SdlContext& iSdlCtx, const std::shared_ptr<SdlFont>& iFont, bool iChecked);
    virtual ~TextCheckbox() override;

    // Copy semantics are not allowed
    TextCheckbox(const TextCheckbox&) = delete;
    TextCheckbox& operator=(const TextCheckbox&) = delete;

    // Move construction is manually provided (required for vector emplacement), move assignment is not allowed
    TextCheckbox(TextCheckbox&& iTextCheckbox);
    TextCheckbox& operator=(TextCheckbox&&) = delete;

    // Clone & share construction; outcome is not self-evident regarding resource sharing, so use a separate constructor
    TextCheckbox(CloneShareTag iCloneShareTag, const TextCheckbox& iTextCheckbox);

    // Copy properties & share resources; note the position is retained and interim state is not copied
    void copyAndShare(const TextCheckbox& iTextCheckbox);

    // Set the state of the checkbox to one of Hidden, Inactive, Active
    virtual bool setState(GuiObjectState iState) override;
    virtual GuiObjectState getState() const override;

    virtual void handleEvents(const std::vector<SDL_Event>& iEvents) override;

    virtual void draw(SdlContext& iSdlCtx) override;

    virtual void setPosition(SdlCoords iCoordinates) override;
    virtual SdlCoords getPosition() const override;

    // Determine if the checkbox was clicked after the events have been handled
    virtual bool wasClicked() const override;

    // Change the checked state, regardless of the check box state (ie even when hidden or inactive)
    // Returns true if the checked state changes
    virtual bool setChecked(bool iChecked) override;
    // getChecked() not overridden

    virtual SdlDims getSize() const;

    // Set the opacity level used if the check box is inactive
    // Note this is relative to the current drawing colour opacity
    virtual void setInactiveOpacity(uint8_t iOpacity_pc);

protected:
    // Setting the size has the same limitations as the TextButton; ie does not set the size lower than the minimum size
    virtual void setSize(SdlDims iDimensions);

private:
    TextButton _checkedTextButton;
    TextButton _uncheckedTextButton;
    //XYZ REPLACE THIS WITH copyAndShare() ?
    TextButton* _activeTextButton;   // either _checkedTextButton or _uncheckedTextButton, must never be nullptr
    bool _checkChanged;   // for efficiency, set to true when the checked state has changed (reset by draw)
    bool _clicked;
};

}   // namespace GuiSimple
}   // namespace Toolkit

#endif   // TOB_TOOLKIT_GUISIMPLE_TEXTCHECKBOX

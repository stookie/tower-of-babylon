/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2023 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "Toolkit/GuiSimple/Radiobuttons.hpp"

#include "Main/Exceptions.hpp"


namespace Toolkit
{
namespace GuiSimple
{

Radiobuttons::Radiobuttons(std::size_t iCount) :
    GuiObject{},
    _count{iCount},
    _selected{0},
    _isSelected{false}
{
    if (iCount == 0)
    {
        throw Main::LogicException{"Radiobuttons::Radiobuttons Error: button number must be greater than zero"};
    }
}

Radiobuttons::~Radiobuttons()
{
}

// Clone & share construction; outcome is not self-evident regarding resource sharing, so use a separate constructor
Radiobuttons::Radiobuttons(CloneShareTag iCloneShareTag, const Radiobuttons& iRadiobuttons) :
    GuiObject{iCloneShareTag, iRadiobuttons}
{
    _count = iRadiobuttons._count;
    _selected = iRadiobuttons._selected;
    _isSelected = iRadiobuttons._isSelected;
}

// Copy properties & share resources; note the position is retained and interim state is not copied
void Radiobuttons::copyAndShare(const Radiobuttons& iRadiobuttons)
{
    _count = iRadiobuttons._count;
    _selected = iRadiobuttons._selected;
    _isSelected = iRadiobuttons._isSelected;
}

std::size_t Radiobuttons::getCount() const
{
    return _count;
}

// Change the selected state, regardless of the radio buttons state (ie even when hidden or inactive)
// Returns true if the selected state changes
bool Radiobuttons::setUnselected()
{
    const bool aIsSelected = _isSelected;
    _isSelected = false;
    return aIsSelected;
}

// Change the selected state, regardless of the radio buttons state (ie even when hidden or inactive)
// Returns true if the selected state changes
bool Radiobuttons::setSelected(std::size_t iCount)
{
    checkCount(iCount);

    const bool aIsSelected = _isSelected;
    const std::size_t aSelected = _selected;
    _isSelected = true;
    _selected = iCount;
    return (!aIsSelected || (_selected != aSelected));
}

bool Radiobuttons::isSelected() const
{
    return _isSelected;
}

std::size_t Radiobuttons::getSelected() const
{
    return _selected;
}

// If count is invalid, throws Main::LogicException
void Radiobuttons::checkCount(std::size_t iCount) const
{
    // std::size_t is unsigned, no need to check for less than zero
    if (iCount >= _count)
    {
        throw Main::LogicException{"Radiobuttons::checkCount Error: requested button number is too large"};
    }
}

}   // namespace GuiSimple
}   // namespace Toolkit

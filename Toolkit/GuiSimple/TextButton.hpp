/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_TOOLKIT_GUISIMPLE_TEXTBUTTON
#define TOB_TOOLKIT_GUISIMPLE_TEXTBUTTON

#include "Sdl/SdlContext.hpp"
#include "Sdl/SdlFont.hpp"
#include "Toolkit/GuiSimple/Button.hpp"

#include <memory>
#include <string>


namespace Toolkit
{
namespace GuiSimple
{

class TextButton : virtual public Button
{
public:
    // Throws Main::LogicException if font is nullptr
    TextButton(SdlContext& iSdlCtx, const std::shared_ptr<SdlFont>& iFont, const std::string& iText);
    virtual ~TextButton() override;

    // Copy semantics are not allowed
    TextButton(const TextButton&) = delete;
    TextButton& operator=(const TextButton&) = delete;

    // Move semantics are provided
    TextButton(TextButton&&) = default;
    TextButton& operator=(TextButton&&) = default;

    // Clone & share construction; outcome is not self-evident regarding resource sharing, so use a separate constructor
    TextButton(CloneShareTag iCloneShareTag, const TextButton& iTextButton);

    // Copy properties & share resources; note the position is retained and interim state is not copied
    void copyAndShare(const TextButton& iTextButton);

    // setState() not overridden
    // getState() not overridden
    // handleEvents() not overridden

    virtual void draw(SdlContext& iSdlCtx) override;

    // setPosition() not overridden
    // getPosition() not overridden

    // Set the size, but do not set the size lower than the minimum size
    virtual void setSize(SdlDims iDimensions) override;
    // getSize() not overridden

    // wasClicked()  not overridden

    // Set new text for the button, optionally retaining the previous size if possible
    // Returns true if the text actually changed; false otherwise
    virtual bool setText(SdlContext& iSdlCtx, const std::string& iText, bool iRetainSize);

    // Set the opacity level used if the button is inactive
    // Note this is relative to the current drawing colour opacity
    virtual void setInactiveOpacity(uint8_t iOpacity_pc);

private:
    std::shared_ptr<SdlText> _text;
    double _opacity;
};

}   // namespace GuiSimple
}   // namespace Toolkit

#endif   // TOB_TOOLKIT_GUISIMPLE_TEXTBUTTON

/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_TOOLKIT_GUISIMPLE_IMAGEBUTTON
#define TOB_TOOLKIT_GUISIMPLE_IMAGEBUTTON

#include "Sdl/SdlContext.hpp"
#include "Sdl/SdlTexture.hpp"
#include "Toolkit/GuiSimple/Button.hpp"

#include <memory>


namespace Toolkit
{
namespace GuiSimple
{

class ImageButton : virtual public Button
{
public:
    // Throws Main::LogicException if texture is nullptr
    ImageButton(SdlContext& iSdlCtx, const std::shared_ptr<SdlTexture>& iTexture, bool iDrawBorder = false);
    virtual ~ImageButton() override;

    // Copy semantics are not allowed
    ImageButton(const ImageButton&) = delete;
    ImageButton& operator=(const ImageButton&) = delete;

    // Move semantics are provided
    ImageButton(ImageButton&&) = default;
    ImageButton& operator=(ImageButton&&) = default;

    // Clone & share construction; outcome is not self-evident regarding resource sharing, so use a separate constructor
    ImageButton(CloneShareTag iCloneShareTag, const ImageButton& iImageButton);

    // Copy properties & share resources; note the position is retained and interim state is not copied
    void copyAndShare(const ImageButton& iImageButton);

    virtual bool setState(GuiObjectState iState) override;
    // getState() not overridden
    // handleEvents() not overridden

    virtual void draw(SdlContext& iSdlCtx) override;

    // setPosition() not overridden
    // getPosition() not overridden

    // Set the size, but do not set the size lower than the minimum size
    virtual void setSize(SdlDims iDimensions) override;
    // getSize() not overridden

    // wasClicked() not overridden

    // Set the opacity level used if the button is inactive
    // Note this is relative to the current drawing colour opacity
    virtual void setInactiveOpacity(uint8_t iOpacity_pc);

private:
    std::shared_ptr<SdlTexture> _texture;
    double _opacity;
    uint8_t _originalTextureOpacity;
    bool _updateTextureOpacity;
    bool _drawBorder;
};

}   // namespace GuiSimple
}   // namespace Toolkit

#endif   // TOB_TOOLKIT_GUISIMPLE_IMAGEBUTTON

/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2023 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_TOOLKIT_GUISIMPLE_VERT_LINE
#define TOB_TOOLKIT_GUISIMPLE_VERT_LINE

#include "Sdl/SdlTypes.hpp"
#include "Toolkit/GuiObject.hpp"

#include <vector>

class SdlContext;
union SDL_Event;


namespace Toolkit
{
namespace GuiSimple
{

class VerticalLine : virtual public GuiObject
{
public:
    VerticalLine();
    virtual ~VerticalLine() override;

    // Copy and move semantics are provided
    VerticalLine(const VerticalLine&) = default;
    VerticalLine& operator=(const VerticalLine&) = default;
    VerticalLine(VerticalLine&&) = default;
    VerticalLine& operator=(VerticalLine&&) = default;

    // Set/get the state of the label to one of Hidden, Inactive, Active
    virtual bool setState(GuiObjectState iState) override;
    virtual GuiObjectState getState() const override;

    virtual void handleEvents(const std::vector<SDL_Event>& iEvents) override;

    virtual void draw(SdlContext& iSdlCtx) override;

    virtual void setPosition(SdlCoords iCoordinates);
    virtual SdlCoords getPosition() const;

    virtual void setHeight(int iHeight);
    virtual int getHeight() const;

    virtual bool setThickness(int iThickness);
    virtual int getThickness() const;

    // Note this is relative to the current drawing colour opacity
    virtual void setInactiveOpacity(uint8_t iOpacity_pc);

private:
    SdlCoords _coordinates;
    SdlDims _dimensions;   // height and thickness
    GuiObjectState _state;
    double _opacity;
};

inline void VerticalLine::handleEvents(const std::vector<SDL_Event>& iEvents)
{
    // No response to any events
}

}   // namespace GuiSimple
}   // namespace Toolkit

#endif   // TOB_TOOLKIT_GUISIMPLE_VERT_LINE

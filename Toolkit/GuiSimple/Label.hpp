/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_TOOLKIT_GUISIMPLE_LABEL
#define TOB_TOOLKIT_GUISIMPLE_LABEL

#include "Sdl/SdlTypes.hpp"
#include "Toolkit/GuiObject.hpp"

class SdlContext;
union SDL_Event;


namespace Toolkit
{
namespace GuiSimple
{

class Label : virtual public GuiObject
{
public:
    Label();
    virtual ~Label() override;

    // Copy and move semantics are provided
    Label(const Label&) = default;
    Label& operator=(const Label&) = default;
    Label(Label&&) = default;
    Label& operator=(Label&&) = default;

    // Clone & share construction; outcome is not self-evident regarding resource sharing, so use a separate constructor
    Label(CloneShareTag iCloneShareTag, const Label& iLabel);

    // Copy properties & share resources; note the position is retained and interim state is not copied
    void copyAndShare(const Label& iLabel);

    // Set/get the state of the label to one of Hidden, Inactive, Active
    virtual bool setState(GuiObjectState iState) override;
    virtual GuiObjectState getState() const override;

    // handleEvents() not implemented

    // draw() not implemented

    virtual void setPosition(SdlCoords iCoordinates);
    virtual SdlCoords getPosition() const;

    // Note this may not behave as expected. For example, text and image labels will not set the
    //  size lower than the minimum size
    virtual void setSize(SdlDims iDimensions) = 0;
    virtual SdlDims getSize() const = 0;

private:
    SdlCoords _coordinates;
    GuiObjectState _state;
};

}   // namespace GuiSimple
}   // namespace Toolkit

#endif   // TOB_TOOLKIT_GUISIMPLE_LABEL

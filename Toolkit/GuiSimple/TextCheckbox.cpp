/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2024 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "Toolkit/GuiSimple/TextCheckbox.hpp"

#include "Main/Exceptions.hpp"

#include <algorithm>
#include <string>
#include <utility>


namespace
{
const std::string kCheckedText{"X"};
const std::string kUncheckedText{" "};
}


namespace Toolkit
{
namespace GuiSimple
{

TextCheckbox::TextCheckbox(SdlContext& iSdlCtx, const std::shared_ptr<SdlFont>& iFont, bool iChecked) :
    Checkbox{iChecked},
    _checkedTextButton{iSdlCtx, iFont, kCheckedText},
    _uncheckedTextButton{iSdlCtx, iFont, kUncheckedText},
    _activeTextButton{iChecked ? &_checkedTextButton : &_uncheckedTextButton},
    _checkChanged{false},
    _clicked{false}
{
    if (iFont == nullptr)
    {
        throw Main::LogicException{"TextCheckbox::TextCheckbox Error: font is null"};
    }

    // Check box buttons are ensured square, and have a valid minimum size
    setSize(SdlDims{});
}

TextCheckbox::~TextCheckbox()
{
}

// Move construction is provided (required for vector emplacement)
// Must be manually defined, due to the pointer attribute pointing to another attribute
TextCheckbox::TextCheckbox(TextCheckbox&& iTextCheckbox) :
    Checkbox{std::move(iTextCheckbox)},
    _checkedTextButton{std::move(iTextCheckbox._checkedTextButton)},
    _uncheckedTextButton{std::move(iTextCheckbox._uncheckedTextButton)},
    _activeTextButton{&_uncheckedTextButton},
    _checkChanged{iTextCheckbox._checkChanged},
    _clicked{iTextCheckbox._clicked}
{
    _activeTextButton = getChecked() ? &_checkedTextButton : &_uncheckedTextButton;
}

// Clone & share construction; outcome is not self-evident regarding resource sharing, so use a separate constructor
TextCheckbox::TextCheckbox(CloneShareTag iCloneShareTag, const TextCheckbox& iTextCheckbox) :
    Checkbox{iCloneShareTag, iTextCheckbox},
    _checkedTextButton{iCloneShareTag, iTextCheckbox._checkedTextButton},
    _uncheckedTextButton{iCloneShareTag, iTextCheckbox._uncheckedTextButton},
    _activeTextButton{iTextCheckbox.getChecked() ? &_checkedTextButton : &_uncheckedTextButton},
    _checkChanged{false},   // dont clone intermediate state
    _clicked{false}   // dont clone intermediate state
{
}

// Copy properties & share resources; note the position is retained and interim state is not copied
void TextCheckbox::copyAndShare(const TextCheckbox& iTextCheckbox)
{
    Checkbox::copyAndShare(iTextCheckbox);
    _checkedTextButton.copyAndShare(iTextCheckbox._checkedTextButton);
    _uncheckedTextButton.copyAndShare(iTextCheckbox._uncheckedTextButton);
    _activeTextButton = iTextCheckbox.getChecked() ? &_checkedTextButton : &_uncheckedTextButton;
    _checkChanged = false;
    // _clicked is unchanged
}

bool TextCheckbox::setState(GuiObjectState iState)
{
    bool aOk = _checkedTextButton.setState(iState);
    aOk = _uncheckedTextButton.setState(iState) && aOk;
    return aOk;
}

GuiObjectState TextCheckbox::getState() const
{
    return _activeTextButton->getState();
}

void TextCheckbox::handleEvents(const std::vector<SDL_Event>& iEvents)
{
    _clicked = false;
    _activeTextButton->handleEvents(iEvents);

    if (_activeTextButton->wasClicked() && getState() == GuiObjectState::kActive)
    {
        _clicked = true;
        setChecked(!getChecked());
    }
}

void TextCheckbox::draw(SdlContext& iSdlCtx)
{
    if (_checkChanged)
    {
        _checkChanged = false;
        _activeTextButton = getChecked() ? &_checkedTextButton : &_uncheckedTextButton;
    }

    _activeTextButton->draw(iSdlCtx);
}

void TextCheckbox::setPosition(SdlCoords iCoordinates)
{
    _checkedTextButton.setPosition(iCoordinates);
    _uncheckedTextButton.setPosition(iCoordinates);
}

SdlCoords TextCheckbox::getPosition() const
{
    return _activeTextButton->getPosition();
}

// Determine if the checkbox was clicked after the events have been handled
bool TextCheckbox::wasClicked() const
{
    return _clicked;
}

// Change the checked state, regardless of the check box state (ie even when hidden or inactive)
bool TextCheckbox::setChecked(bool iChecked)
{
    // Need to maintain the check changed state

    const bool aCheckChanged = Checkbox::setChecked(iChecked);
    _checkChanged = _checkChanged || aCheckChanged;
    return aCheckChanged;
}

SdlDims TextCheckbox::getSize() const
{
    return _activeTextButton->getSize();
}

// Set the opacity level used if the check box is inactive
// Note this is relative to the current drawing colour opacity
void TextCheckbox::setInactiveOpacity(uint8_t iOpacity_pc)
{
    _checkedTextButton.setInactiveOpacity(iOpacity_pc);
    _uncheckedTextButton.setInactiveOpacity(iOpacity_pc);
}

// Setting the size has the same limitations as the TextButton; ie does not set the size lower than the minimum size
void TextCheckbox::setSize(SdlDims iDimensions)
{
    _checkedTextButton.setSize(iDimensions);
    _uncheckedTextButton.setSize(iDimensions);

    // Ensure the buttons are square in dimensions, the same size for both the checked and unchecked text buttons
    int aSquareDim = std::max(_checkedTextButton.getSize()._w, _checkedTextButton.getSize()._h);
    aSquareDim = std::max(aSquareDim, _uncheckedTextButton.getSize()._w);
    aSquareDim = std::max(aSquareDim, _uncheckedTextButton.getSize()._h);

    SdlDims aDimensions{aSquareDim, aSquareDim};
    _checkedTextButton.setSize(aDimensions);
    _uncheckedTextButton.setSize(aDimensions);
}

}   // namespace GuiSimple
}   // namespace Toolkit

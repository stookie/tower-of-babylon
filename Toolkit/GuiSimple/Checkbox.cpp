/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2023 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "Toolkit/GuiSimple/Checkbox.hpp"


namespace Toolkit
{
namespace GuiSimple
{

Checkbox::Checkbox(bool iChecked) :
    GuiObject{},
    _checked{iChecked}
{
}

Checkbox::~Checkbox()
{
}

// Clone & share construction; outcome is not self-evident regarding resource sharing, so use a separate constructor
Checkbox::Checkbox(CloneShareTag iCloneShareTag, const Checkbox& iCheckbox) :
    GuiObject{iCloneShareTag, iCheckbox}
{
    _checked = iCheckbox._checked;
}

// Copy properties & share resources; note the position is retained and interim state is not copied
void Checkbox::copyAndShare(const Checkbox& iCheckbox)
{
    _checked = iCheckbox._checked;
}

// Change the checked state, regardless of the check box state (ie even when hidden or inactive)
// Returns true if the checked state changes
bool Checkbox::setChecked(bool iChecked)
{
    bool aCheckChanged = (_checked != iChecked);
    _checked = iChecked;
    return aCheckChanged;
}

bool Checkbox::getChecked() const
{
    return _checked;
}

}   // namespace GuiSimple
}   // namespace Toolkit

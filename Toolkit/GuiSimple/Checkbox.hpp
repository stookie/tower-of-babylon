/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2023 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef TOB_TOOLKIT_GUISIMPLE_CHECKBOX
#define TOB_TOOLKIT_GUISIMPLE_CHECKBOX

#include "Sdl/SdlTypes.hpp"
#include "Toolkit/GuiObject.hpp"


namespace Toolkit
{
namespace GuiSimple
{

class Checkbox : virtual public GuiObject
{
public:
    Checkbox(bool iChecked);
    virtual ~Checkbox() override;

    // Copy and move semantics are provided
    Checkbox(const Checkbox&) = default;
    Checkbox& operator=(const Checkbox&) = default;
    Checkbox(Checkbox&&) = default;
    Checkbox& operator=(Checkbox&&) = default;

    // Clone & share construction; outcome is not self-evident regarding resource sharing, so use a separate constructor
    Checkbox(CloneShareTag iCloneShareTag, const Checkbox& iCheckbox);

    // Copy properties & share resources; note the position is retained and interim state is not copied
    void copyAndShare(const Checkbox& iCheckbox);

    // setState() not implemented
    // getState() not implemented

    // handleEvents() not implemented

    // draw() not implemented

    virtual void setPosition(SdlCoords iCoordinates) = 0;
    virtual SdlCoords getPosition() const = 0;

    // Determine if the checkbox was clicked after the events have been handled
    virtual bool wasClicked() const = 0;

    // Change the checked state, regardless of the check box state (ie even when hidden or inactive)
    // Returns true if the checked state changes
    virtual bool setChecked(bool iChecked);
    virtual bool getChecked() const;

private:
    bool _checked;
};

}   // namespace GuiSimple
}   // namespace Toolkit

#endif   // TOB_TOOLKIT_GUISIMPLE_CHECKBOX

/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2023 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "Toolkit/GuiSimple/Label.hpp"

#include "Sdl/SdlContext.hpp"


namespace Toolkit
{
namespace GuiSimple
{

Label::Label() :
    GuiObject{},
    _coordinates{},
    _state{GuiObjectState::kActive}
{
}

Label::~Label()
{
}

// Clone & share construction; outcome is not self-evident regarding resource sharing, so use a separate constructor
Label::Label(CloneShareTag iCloneShareTag, const Label& iLabel) :
    GuiObject{iCloneShareTag, iLabel},
    _coordinates{iLabel._coordinates},
    _state{iLabel._state}
{
}

// Copy properties & share resources; note the position is retained and interim state is not copied
void Label::copyAndShare(const Label& iLabel)
{
    // _coordinates are unchanged
    _state = iLabel._state;
}

// Set/get the state of the label to one of Hidden, Inactive, Active
bool Label::setState(GuiObjectState iState)
{
    switch (iState)
    {
        case GuiObjectState::kHidden:
        case GuiObjectState::kInactive:
        case GuiObjectState::kActive:
            _state = iState;
            return true;
            break;
        default:
            return false;
            break;
    }
}

GuiObjectState Label::getState() const
{
    return _state;
}

void Label::setPosition(SdlCoords iCoordinates)
{
    _coordinates = iCoordinates;
}

SdlCoords Label::getPosition() const
{
    return _coordinates;
}

}   // namespace GuiSimple
}   // namespace Toolkit

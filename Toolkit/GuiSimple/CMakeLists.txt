# Authors:
# stookie <4303586-stookie@users.noreply.gitlab.com>
# Copyright (C) 2021-2024 authors
# Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.

list(APPEND tower_of_babylon_SOURCES
    ${CMAKE_CURRENT_LIST_DIR}/BackgroundImage.cpp
    ${CMAKE_CURRENT_LIST_DIR}/Button.cpp
    ${CMAKE_CURRENT_LIST_DIR}/Checkbox.cpp
    ${CMAKE_CURRENT_LIST_DIR}/HorizontalLine.cpp
    ${CMAKE_CURRENT_LIST_DIR}/ImageButton.cpp
    ${CMAKE_CURRENT_LIST_DIR}/Label.cpp
    ${CMAKE_CURRENT_LIST_DIR}/Radiobuttons.cpp
    ${CMAKE_CURRENT_LIST_DIR}/TextButton.cpp
    ${CMAKE_CURRENT_LIST_DIR}/TextCheckbox.cpp
    ${CMAKE_CURRENT_LIST_DIR}/TextLabel.cpp
    ${CMAKE_CURRENT_LIST_DIR}/TextRadiobuttons.cpp
    ${CMAKE_CURRENT_LIST_DIR}/VerticalLine.cpp
)

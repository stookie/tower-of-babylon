/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2021-2023 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "Toolkit/GuiSimple/Button.hpp"

#include "Sdl/SdlContext.hpp"

#include <SDL2/SDL_events.h>


namespace Toolkit
{
namespace GuiSimple
{

Button::Button() :
    GuiObject{},
    _coordinates{},
    _dimensions{},
    _state{GuiObjectState::kActive},
    _clickStarted{false},
    _clicked{false}
{
}

Button::~Button()
{
}

// Clone & share construction; outcome is not self-evident regarding resource sharing, so use a separate constructor
Button::Button(CloneShareTag iCloneShareTag, const Button& iButton) :
    GuiObject(iCloneShareTag, iButton)
{
    _coordinates = iButton._coordinates;
    _dimensions = iButton._dimensions;
    _state = iButton._state;
    _clickStarted = false;   // dont clone intermediate state
    _clicked = false;   // dont clone intermediate state
}

// Copy properties & share resources; note the position is retained and interim state is not copied
void Button::copyAndShare(const Button& iButton)
{
    // _coordinates are unchanged
    _dimensions = iButton._dimensions;
    _state = iButton._state;
    // _clickStarted is unchanged
    // _clicked is unchanged
}

// Set/get the state of the button to one of Hidden, Inactive, Active
bool Button::setState(GuiObjectState iState)
{
    switch (iState)
    {
        case GuiObjectState::kHidden:
        case GuiObjectState::kInactive:
        case GuiObjectState::kActive:
            _state = iState;
            return true;
            break;
        default:
            return false;
            break;
    }
}

GuiObjectState Button::getState() const
{
    return _state;
}

void Button::handleEvents(const std::vector<SDL_Event>& iEvents)
{
    _clicked = false;   // one-shot

    if (_state == GuiObjectState::kHidden || _state == GuiObjectState::kInactive)
    {
        // Dont handle any events while hidden or inactive
        _clickStarted = false;
        return;
    }

    for (const SDL_Event& aEvent : iEvents)
    {
        if (!_clickStarted && aEvent.type == SDL_MOUSEBUTTONDOWN && aEvent.button.button == SDL_BUTTON_LEFT)
        {
            if (aEvent.button.x >= _coordinates._x && aEvent.button.x <= (_coordinates._x + _dimensions._w) &&
                aEvent.button.y >= _coordinates._y && aEvent.button.y <= (_coordinates._y + _dimensions._h))
            {
                _clickStarted = true;
            }
        }
        else if (_clickStarted && aEvent.type == SDL_MOUSEBUTTONUP && aEvent.button.button == SDL_BUTTON_LEFT)
        {
            _clickStarted = false;

            if (aEvent.button.x >= _coordinates._x && aEvent.button.x <= (_coordinates._x + _dimensions._w) &&
                aEvent.button.y >= _coordinates._y && aEvent.button.y <= (_coordinates._y + _dimensions._h))
            {
                _clicked = true;
            }
        }
    }
}

void Button::draw(SdlContext& iSdlCtx)
{
    if (_state == GuiObjectState::kHidden)
    {
        // Dont draw button while hidden
        return;
    }

    iSdlCtx.drawRectangle(_coordinates, _dimensions, false);
}

void Button::setPosition(SdlCoords iCoordinates)
{
    _coordinates = iCoordinates;
}

SdlCoords Button::getPosition() const
{
    return _coordinates;
}

// Note this may not behave as expected. For example, text and image buttons will not set the
//  size lower than the minimum size
void Button::setSize(SdlDims iDimensions)
{
    _dimensions = iDimensions;
}

SdlDims Button::getSize() const
{
    return _dimensions;
}

// Determine if the button was clicked after the events have been handled
bool Button::wasClicked() const
{
    return _clicked;
}

}   // namespace GuiSimple
}   // namespace Toolkit

# Tower Of Babylon

A free/libre open source simple dice game (under development)

## Summary

This is an attempt to create a free, open source simple dice game.

It is currently at an early stage. Contributions will be welcome, but at a later stage of development.

It is written in C++, and uses the SDL2 libraries. It is developed in Linux (Debian, specifically), but should be fully portable to all platforms supporting SDL2.

The source code is released under the GNU Affero General Public License, version 3 only. For details of the authors and source code, as well as the licenses of other included files, see "COPYING.md".

![GNU AGPL 3](https://www.gnu.org/graphics/agplv3-155x51.png "GNU AGPL 3")

## Build Instructions

Unix makefile build:

```
> mkdir build && cd build
> cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ../
> make
```

Eclipse project:

```
> cmake -G "Eclipse CDT4 - Unix Makefiles" -D CMAKE_BUILD_TYPE=Debug ./
```

